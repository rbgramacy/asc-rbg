\documentclass[12pt,xcolor=svgnames]{beamer}
\usepackage{dsfont,natbib,setspace}
\mode<presentation>

% replaces beamer foot with simple page number
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}{
  \vspace{-1.5cm}
  \raisebox{10pt}{\makebox[\paperwidth]{\hfill\makebox[20pt]{\color{gray}\scriptsize\insertpagenumber}}}}

% colors
\newcommand{\bk}{\color{black}}
\newcommand{\rd}{\color{red}}
\newcommand{\fg}{\color{forestgreen}}
\newcommand{\bl}{\color{blue}}
\newcommand{\gr}{\color{gray}}
\newcommand{\theme}{\color{Maroon}}

% common math markups
\newcommand{\bs}[1]{\boldsymbol{#1}}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mr}[1]{\mathrm{#1}}
\newcommand{\bm}[1]{\mbox{\boldmath $#1$}}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\ds}[1]{\mathds{#1}}

% spacing and style shorthand
\newcommand{\sk}{\vspace{.4cm}}
\newcommand{\nochap}{\vspace{0.6cm}}
\newcommand{\nsk}{\vspace{-.4cm}}
\newcommand{\chap}[1]{{\theme \Large \bf #1} \sk}
\newcommand{\R}[1]{{\bl\tt #1}}
\newcommand{\til}{{\footnotesize$\bs{\stackrel{\sim}{}}$ }}

% specific stats markups for this doc
\newcommand{\E}{\ds{E}}
\newcommand{\Reals}{\ds{R}}
\newcommand{\var}{\text{var}}
\newcommand{\cov}{\text{cov}}
\newcommand{\mT}{\mc{T}}
\newcommand{\GP}{\mc{GP}}
\newcommand{\iidsim}{\stackrel{\mathrm{iid}}{\sim}}
\newcommand{\indsim}{\stackrel{\mathrm{ind}}{\sim}}
\newcommand{\mN}{\mc{N}}

\begin{document}
{ %\usebackgroundtemplate{\includegraphics[height=\paperheight]{phoenix}}
\thispagestyle{empty}
\setcounter{page}{0}

\title{\Large \theme 
\includegraphics[scale=0.1]{../Rlogo}\\
\bf {\sf Plotting and Visualization}}
\author{\vskip .5cm {\bf Robert B.~Gramacy}\\{\color{Maroon}
    Virginia Tech}
  Department of Statistics\\
  \vskip .2cm \texttt{\rd bobby.gramacy.com}
  \vskip .25cm}
\date{}
\maketitle }

% doc spacing
\setstretch{1.1}

\begin{frame}[fragile]
\chap{Graphics}

{\sf R} has tools for drawing most common types of charts, including
\begin{itemize}
\item bar charts, pie charts, line charts, and scatter plots.
\end{itemize}

\sk And some less familiar ones too:
\begin{itemize}
\item quantile-quantile (Q-Q) plots, mosaic plots and contour plots.
\end{itemize}

You can show {\sf R} graphics on the screen, or save them in many
different formats.

\sk {\rd This lecture scratches the surface.}

\end{frame}

\begin{frame}
\nochap

\vspace{-0.25cm}
{\sf R} gives you an enormous amount of control over graphics.
\begin{itemize}
\item You can control almost every aspect of a chart.
\end{itemize}

\sk A criticism of {\sf R}'s graphics capability is that the plots
aren't very interactive.
\begin{itemize}
\item That's fair for the most part, especially compared to {\sf MATLAB}.
\end{itemize}

\sk {\sf R's} plotting {\rd philosophy} is that generating graphics is best
scripted, which makes them
\begin{itemize}
\item better looking,
\item reproducible,
\item and more transparently linked to the underlying data.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\chap{Scatter plots}

We've used these before, but its a good place to start.

\vspace{0.25cm}
\begin{quote}
\R{plot(}x, y=NULL, \dots\R{)}
\end{quote}

It is a generic function (you can ``plot'' many types of objects).
\begin{itemize}
\item For simple scatter plots with one (or two) vector(s),
  the function that is called is \R{plot.default()}.
\item when \R{y=NULL}, the \R{x=} argument actually specifies the
  ``$y$-axis'' values, and the $x$-values are \R{x=1:length(y)}.
\end{itemize}

{\bl
\begin{verbatim}
> y <- (1:10)^2
> plot(y)
> plot(1:10, y)  ## gives the same thing
\end{verbatim}
}
\vspace{-0.5cm}
\end{frame}

\begin{frame}[fragile]
\nochap
... a simple way to visualize functions in the $x$-$y$
plane

{\bl \footnotesize
\begin{verbatim}
> lines(1:10, y, col="red")  
> curve(x^2, 1, 10, add=TRUE, lty=2)
> legend("topleft", c("lines", "curve"), 
+     lty=1:2, col=1:2)
\end{verbatim}
}

\begin{center}
\includegraphics[scale=0.5,trim=10 0 0 30]{pointslines}
\end{center}

\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.25cm}
Consider cases of cancer in 2008, and toxic waste releases in
2006, each by state; available at
\begin{center}
\url{http://www.census.gov/compendia/statab}
\end{center}

\vspace{-0.25cm}
{\bl
\begin{verbatim}
> tc <- read.csv("toxins_and_cancer.csv")
> attach(tc)
\end{verbatim}
}

\sk Lets make a scatter plot comparing 
\begin{itemize}
\item the overall cancer rate (number of cancer deaths divided by
  state population)
\item to the presence of toxins (total toxic chemicals release
  divided by state area)
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\nochap

{\bl
\begin{verbatim}
> toxpres <- total_toxic_chemicals/Surface_Area
> crate <- deaths_total/Population
> plot(toxpres, crate)
\end{verbatim}

\begin{center}
\includegraphics[scale=0.55,trim=10 0 0 30]{toxrate_crate}
\end{center}
}
\end{frame}

\begin{frame}[fragile]
\nochap

Suppose you wanted to know which states were associated with which
points.
\begin{itemize}
\item {\sf R} provides some interactive tools.
\end{itemize}

\sk You can use \R{locator()} to tell you the coordinates of a
specific point (or set of points).  After plotting ...
{\bl
\begin{verbatim}
> locator(1)
$x
[1] 0.0037301

$y
[1] 0.002238106
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap

Another interactive way of identifying points is \R{identify()}.
{\bl
\begin{verbatim}
> identify(toxpres, crate, State_Abbrev)
[1]  3  4 10 12 35
\end{verbatim}
}
\begin{itemize}
\item While the command is running, you can click on individual points
  on the chart, and {\sf R} will label those with state names.
\item Press {\tt Esc} when you're done.
\end{itemize}
\end{frame}

\begin{frame}
\nochap

\begin{center}
\includegraphics[scale=0.6,trim=10 0 0 40]{identify}
\end{center}
\end{frame}

\begin{frame}[fragile]
\nochap

If you want to label all points at once, use \R{text()}.

{\bl
\begin{verbatim}
> xl <- "toxic chem / unit area"
> yl <- "deaths per capita"
> plot(toxpres, crate, xlab=xl, ylab=yl)
> text(toxpres, crate, labels=State_Abbrev,
+      cex=0.75, adj=c(0,-1))
\end{verbatim}
}
\begin{itemize}
\item \R{xlab=} and \R{ylab=} add labels to the axes
\item \R{cex=} adjusts the size of the points or text
\item \R{adj=} specifies an $x$-$y$ adjustment to the
coordinates of the labels.  Centered is \R{adj=c(0.5,0.5)}.
\end{itemize}
\end{frame}

\begin{frame}
\nochap 
\begin{center}
\includegraphics[scale=0.6,trim=10 0 0 40]{toxrate_crate_text}
\end{center}
\end{frame}

\begin{frame}[fragile]
\nochap
Another option is to forgo the points altogether.
{\bl \small
\begin{verbatim}
> plot(toxpres, crate, xlab=xl, ylab=yl, type="n")
> text(toxpres, crate, labels=State_Abbrev)
\end{verbatim}
}

\begin{center}
\includegraphics[scale=0.55,trim=10 0 0 30]{toxrate_crate_textonly}
\end{center}

\end{frame}

\begin{frame}[fragile]
\nochap
{\rd Example (ctd.):} Recall the \R{cars} data from the first
lecture.

\begin{center}
\includegraphics[scale=0.65, trim=20 20 0 60]{cars2pred}
\end{center}
\vspace{-0.25cm}
\hfill (code in \R{.R} file)
\vspace{1cm}
\end{frame}

\begin{frame}[fragile]
\chap{Large Data}

If plotting a very large number of points, then a
\R{smoothScatter()} might look cleaner.
\begin{itemize}
\item It plots the density of points by shading different regions of
  the space.
\end{itemize}

\sk Consider data on a random, but very {\em large}, 10\% subsample of
births in the United States during 2006.
{\bl
\begin{verbatim}
> b6 <- read.csv("births2006.csv")
\end{verbatim}
}

We can use the data to check, e.g., whether the mother's weight gain
correlates to the baby's weight.
\begin{itemize}
\item Something we will revisit more more technical tools later.
\end{itemize}

\end{frame}

\begin{frame}[fragile]
\nochap 

First, clean the data so that
\begin{itemize}
\item  we're only looking at valid
(non-missing) birth weights and weight gain values
\item excluding premature births, and twins (or more)
\end{itemize}

{\bl
\begin{verbatim}> b6 <- read.csv("births2006.csv")
> b6c <- b6[!is.na(b6$WTGAIN) &
+           !is.na(b6$DBWT) &
+           b6$DPLURAL == "1 Single" &
+           b6$ESTGEST>35,]
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap

{\bl \footnotesize
\begin{verbatim}
> smoothScatter(b6c$WTGAIN, b6c$DBWT,
+    xlab="mom's weight gain", ylab="baby's weight")
KernSmooth 2.23 loaded
Copyright M. P. Wand 1997-2009
\end{verbatim}
}

\begin{center}
\includegraphics[scale=0.57,trim=10 0 0 50]{birthwt}
\end{center}

\end{frame}

\begin{frame}[fragile]
\nochap

If you have a data frame with $n$ different variables and you would
like to generate a scatter plot for each pair, use \R{pairs()}.

\sk For example, we can plot hits, runs, strikeouts, walks, and home
runs for each MLB player with $> 100$ at bats.

\sk
{\bl
\begin{verbatim}
> bat08 <- read.csv("bat08.csv")
> pairs(bat08[bat08$AB > 100,
+       c("H", "R", "SO", "BB", "HR")])
\end{verbatim}
}
\end{frame}

\begin{frame}
\nochap

\begin{center}
\includegraphics[scale=0.6,trim=0 0 0 25]{batpairs}
\end{center}

\end{frame}

\begin{frame}[fragile]
\nochap

That's not a surprising result.
\begin{itemize}
\item Of course people with more hits tend to have more of everything else
  because they've been to the plate more often.
\item We need to adjust for number of at bats, or plate appearances.
\end{itemize}

\sk
{\bl
\begin{verbatim}
> b08ab <- bat08[bat08$AB > 100,]
> pairs(b08ab[,c("H", "R", "SO", "BB", "HR")]/
+     b08ab$AB)
\end{verbatim}
}
\end{frame}

\begin{frame}
\nochap

\begin{center}
\includegraphics[scale=0.6,trim=0 0 0 25]{batpairs_ab}
\end{center}

\end{frame}

\begin{frame}[fragile]
\chap{Time Series}

{\sf R} includes a suite of functions for visualizing time series
data.
\begin{itemize}
\item \R{plot} has a method for {\bl \verb!"ts"!}-class
objects: \R{plot.ts()}
\end{itemize}

\vspace{-0.2cm}
{\bl
\begin{verbatim}
> turkey <- ts(scan("turkey.txt"), start=2001, 
+     frequency=12)
> plot(turkey)
\end{verbatim}
}

\begin{center}
\includegraphics[scale=0.5,trim=10 30 0 60]{turkey2}
\end{center}
\vspace{-0.6cm}
\end{frame}

\begin{frame}
\nochap

As you can see, turkey prices are very seasonal.
\begin{itemize}
\item Huge sales in November and December (for Thanksgiving and
  Christmas)
\item Minor sales in the Spring (probably Easter).
\end{itemize}

\sk
Another way to look at seasonal effects is with an {\rd
  autocorrelation plot}, which is also called a {\rd correlogram}.
\begin{itemize}
\item This plot shows how correlated points are with others that spaced
  apart by different gaps, called {\em lag}s.
\[
r(\ell) = \mathrm{cor}(Y_t, Y_{t-\ell}), \;\;\; t\in \{\ell+1, \dots, T\},
\]
for lags $\ell = 1, 2, 3, \dots$.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\nochap
\R{acf()} computes the autcorrelation function (ACF) and plots the result
(by default).

{\bl
\begin{verbatim}
> acf(turkey)
\end{verbatim}
}

\begin{center}
\includegraphics[scale=0.55,trim=10 10 0 30]{turkey2_acf}
\end{center}
\end{frame}

\begin{frame}[fragile]
\chap{Bar Charts}

To draw bar (or column) charts in {\sf R}, use \R{barplot()}.

\sk Consider data on doctoral degrees in USA from 2001--2006.
{\bl
\begin{verbatim}
> docs <- read.csv("doctorates.csv")
\end{verbatim}
}

\sk \R{barplot()} can't work with data frames, so make a matrix.
{\bl 
\begin{verbatim}
> docs.m <- as.matrix(docs[,2:7])
> rownames(docs.m) <- docs[,1]
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap
Start by showing a bar plot of the 2001 numbers.
{\bl
\begin{verbatim}
> barplot(docs.m[1,])
\end{verbatim}
}

\begin{center}
\includegraphics[scale=0.55,trim=5 30 0 30]{docs2001}
\end{center}
\vspace{-0.25cm}
\end{frame}

\begin{frame}[fragile]
\nochap

All years as bars stacked side-by-side, horizontally.
{\bl
\begin{verbatim}
> barplot(docs.m, beside=TRUE, horiz=TRUE,
+     legend=TRUE, cex.names=0.65)
\end{verbatim}
}

\begin{center}
\includegraphics[scale=0.55,trim=5 5 0 40]{docs}
\end{center}
\end{frame}

\begin{frame}[fragile]
\nochap
Finally, stacked by year on top of one another.

{\bl \small
\begin{verbatim}
> barplot(t(docs.m), legend=TRUE,
+     ylim=c(0, 55000), args.legend=list(ncol=3))
\end{verbatim}
}

\begin{center}
\includegraphics[scale=0.55,trim=5 20 0 40]{docsyear}
\end{center}
\vspace{-0.25cm} 
\end{frame}

\begin{frame}
\chap{Pie Charts}

Pie charts can be an effective way to compare different parts of a
quantity, though there are lots of good reasons {\rd not} to use them.
They
\begin{itemize}
\item take up lots of space, and
\item are bad at showing subtle differences between the size of different
  slices.
\item The eye is bad at judging relative measures.
\end{itemize}

\sk Bar charts might be better. 

\sk Nonetheless, {\sf R} can make a pretty pie chart.

\end{frame}

\begin{frame}[fragile]
\nochap
\vspace{-0.25cm}
Fish caught in the USA in 2006.

{\bl
\begin{verbatim}
> catch <- c(7752, 1166, 463, 108)
> names(catch) <- c("Fresh and Frozen",
+      "Reduced to meal, oil, etc.", "Canned",
+      "Cured")
> pie(catch)
\end{verbatim}
}

\begin{center}
\hspace{1cm} \includegraphics[scale=0.45,trim=150 120 150 150]{fishpie}
\end{center}
\end{frame}

\begin{frame}
\chap{Conditional Density Plots}

Suppose you want to look at the {\em conditional density} of a set of
categories dependent on a numeric value.
\begin{itemize}
\item Combining elements of bar/pie charts and real-valued data.
\end{itemize}

\sk We can use \R{cdplot()} which has two interfaces:
\begin{itemize}
\item $x$-$y$ via \R{cdplot(x, y, ...)} where \R{y=} contains the
  categories.
\item via formula: \R{cdplot(formula, data=, ...)} handy for data frames.
\end{itemize}
\vspace{-0.25cm}
\end{frame}

\begin{frame}[fragile]
\nochap

E.g. suppose we wanted to look at how the distribution of batting
hand varies by batting average among MLB players in 2008.

\sk
{\bl
\begin{verbatim}
> bat08 <- transform(bat08, AVG=H/AB)
> cdplot(bats~AVG, data=bat08, 
+     subset=bat08$AB>100)
\end{verbatim}
}

\sk We'll see that the proportion of switch hitters
({\bl\verb!bats == "B"!})  increases with batting average.

\end{frame}

\begin{frame}
\nochap

\begin{center}
\includegraphics[scale=0.5,trim=30 0 0 40]{batcd}
\end{center}
\end{frame}

\begin{frame}[fragile]
\nochap

You can do something similar for two categorical variables
via \R{mosaicplot()} or \R{spineplot()}.
\begin{itemize}
\item Their interfaces are very similar to \R{cdplot()}
\item accepting either matrix or formula arguments.
\end{itemize}

{\bl
\begin{verbatim}
mosaicplot(bats~throws, data=bat08, color=TRUE)
spineplot(bats~throws, data=bat08)
\end{verbatim}
}
\end{frame}

\begin{frame}
\nochap

\vspace{-0.5cm}
\hspace{1.8cm} Mosaic \hspace{4.2cm} Spine\\

\vspace{0.25cm}
\includegraphics[scale=0.4,trim=60 0 20 40]{batsmosaic}
\includegraphics[scale=0.4,trim=10 0 40 10]{batsspine}

\sk Also see \R{assocplot()}, \R{stars()}, and \R{fourfoldplot()}.
\end{frame}

\begin{frame}
\chap{Three-dimensional Data}

{\sf R} includes several functions for visualizing 3d numerical data.  

\sk At their most basic, they take in a matrix of $z$-values:
\begin{itemize}
\item The $x$-coordinates are \R{x=1:nrow(z)};
\item the $y$-coordinates are \R{y=1:ncol(x)}.
\item Then each $z$-coordinate is interpreted as\\ \R{z[i,j] = z(x[i], y[j])}.
\end{itemize}

\sk As an example, consider elevation data at Yosemite National Park,
which can be downloaded from the USGS:
\begin{center}
\url{seamless.usgs.gov/website/seamless/viewer.htm}
\end{center}
\vspace{-0.5cm}
\end{frame}

\begin{frame}[fragile]
\nochap

Read in the data.
{\bl
\begin{verbatim}
> yos <- as.matrix(
+     read.csv("yosemite.csv", header=FALSE))
\end{verbatim}
}

\sk And for now lets zoom in on the square of rightmost columns
containing Halfdome.
{\bl
\begin{verbatim}
> halfdome <- (nrow(yos) - ncol(yos) + 1):562
> dim(yos[halfdome,])
[1] 253 253
\end{verbatim}
}
\end{frame}

\begin{frame}
\nochap

To view a 3d surface, use \R{persp()} which 
\begin{itemize}
\item draws the surface from a particular perspective (in 2d of course).
\end{itemize}

\sk In addition to providing the \R{z} matrix, you'll need to consider
\begin{itemize}
\item the angle (in degrees) of the perspective \R{(theta=, phi=)};
\item the angle (in degrees) and intensity of light for shading
  \R{(ltheta=, lphi=, shade=)}.
\end{itemize}

\sk
It take a little tinkering to make it pretty.
\end{frame}

\begin{frame}[fragile]
\nochap

{\bl \footnotesize
\begin{verbatim}
> persp(yos[halfdome,], col=grey(0.25), border=NA,
+       expand=0.15, theta=225, phi=20,ltheta=45,
+       lphi=20, shade=0.75, xlab="East-West",
+       ylab="North-South", zlab="Altitude")
\end{verbatim}

\begin{center}
\includegraphics[scale=0.75,trim=50 100 40 140]{halfdome_persp}
\end{center}
}

\end{frame}

\begin{frame}
\nochap

Another useful function for plotting 3d data is \R{image()}.
\begin{itemize}
\item It takes similar arguments as \R{persp()}, 
\item plotting a matrix of
  data points as a grid of boxes,
\item color coding them based on the intensity ($z$-value) at each location.
\end{itemize}

\sk \R{image()} has a \R{col=} argument where you need to specify the
{\rd palette} of colors for the $z$ intensities.  
\begin{itemize}
\item The default \R{col=heat.colors(12)} is low resolution.
\item \R{col=terrain.colors(128)} or \R{col=topo.colors(128)} are
  appropriate for Yosemite valley.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\nochap 

{\bl
\begin{verbatim}
> image(yos, asp=253/562, col=terrain.colors(128),
+       xlab="West-East", ylab="South-North", 
+       bty="n")
\end{verbatim}
}

\begin{center}
\includegraphics[scale=0.75,trim=50 0 40 0]{yosimage}
\end{center}
\end{frame}

\begin{frame}[fragile]
\nochap

Finally, contour plots work similarly.

{\bl
\begin{verbatim}
> contour(yos, asp=253/562, bty="n",
+         xlab="West-East", ylab="South-North")
\end{verbatim}
}

\begin{center}
\includegraphics[scale=0.75,trim=50 0 40 0]{yoscontour}
\end{center}
\end{frame}

\begin{frame}[fragile]
\nochap

You can even do both.
{\bl
\begin{verbatim}
> image(yos, asp=253/562, col=terrain.colors(128),
+       xlab="West-East", ylab="South-North",
+       bty="n")
> contour(yos, add=TRUE)
\end{verbatim}
}

\begin{center}
\includegraphics[scale=0.75,trim=50 0 40 0]{yosboth}
\end{center}

\end{frame}

\begin{frame}[fragile]
\nochap

For interactive 3d visualization, it is hard to beat \R{rgl}.
\begin{itemize}
\item It can be quite some work, but worth it.
\end{itemize}

{\bl
\begin{verbatim}
> library(rgl)
> x <- 10*(1:nrow(yos))
> y <- 10*(1:ncol(yos))
> zlim <- range(yos)
> zlen <- zlim[2] - zlim[1] + 1
> colorlut <- terrain.colors(zlen) 
> col <- colorlut[ yos-zlim[1]+1 ] 
> surface3d(x, y, yos, color=col, back="lines")
\end{verbatim}
}

\end{frame}

\begin{frame}
\nochap

\begin{center}
\includegraphics[scale=0.38,trim=0 0 0 60]{yosrgl}
\end{center}
\end{frame}

\begin{frame}[fragile]
\nochap 
How about when you don't have a nice matrix of $z$-values.
\begin{itemize}
\item Instead, you have a scatter of $(x,y,z)$.
\end{itemize}

\sk E.g., 
{\bl
\begin{verbatim}
> f <- function(x, y) {
+   g <- function(z) 
+     exp(-(z-1)^2) + 
+     exp(-0.8*(z+1)^2) - 0.05*sin(8*(z+0.1))
+   z <- -g(x)*g(y)
+ }
> x <- runif(1000, -2, 2)
> y <- runif(1000, -2, 2)
> z <- f(x,y)
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap

One (not great) option is \R{scatterplot3d()}.
\begin{itemize}
\item Might be a good option for noisy data.
\end{itemize}

\vspace{-0.25cm}
{\bl
\begin{verbatim}
> library(scatterplot3d)
> scatterplot3d(x,y,z)
\end{verbatim}
}

\hfill\includegraphics[scale=0.45,trim=10 0 0 100]{f2dscatter}

\vspace{-0.25cm}
\end{frame}

\begin{frame}[fragile]
\nochap

It would be nice to make a perspective or image plot. 
\begin{itemize}
\item First {\rd interpolate} the scatter of dots onto a grid.
\end{itemize}

{\bl
\begin{verbatim}
> library(akima)
> g <- interp(x, y, z)
\end{verbatim}
}

Then, make whatever 3d plot you fancy.
{\bl 
\begin{verbatim}
> persp(g, phi=45, theta=45, xlab="x",
+       ylab="y", zlab="f(x,y)")
> image(g, xlab="x", ylab="y",
+       col=heat.colors(128))
> contour(g, add=TRUE)
\end{verbatim}
}
\end{frame}

\begin{frame}
\nochap

\includegraphics[scale=0.47,trim=100 40 60 90]{f2dpersp}
\includegraphics[scale=0.4]{f2dimage}

\begin{itemize}
\item Much better.
\end{itemize}

\end{frame}

\begin{frame}[fragile]
\nochap

Of course, if what you really want to do is evaluate a function over a
surface of $(x,y)$ input pairs, you can evaluate over an appropriate
grid (in the first place) and avoid interpolation.

\sk The \R{outer()} function is helpful here.
\begin{itemize}
\item It works
 a little like \R{apply()}.
\end{itemize}

{\bl
\begin{verbatim}
> x <- seq(-2, 2, length=100)
> y <- x
> z <- outer(x, y, f)
> dim(z)
[1] 100 100
\end{verbatim}
}

\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.35cm}
{\bl
\begin{verbatim}
> persp3d(x, y, z, col="lightblue")
\end{verbatim}
}

\begin{center}
\includegraphics[scale=0.27,trim=0 0 0 0]{f2dpersp3d}
\end{center}

\end{frame}

\begin{frame}
\chap{Plotting Distributions}

When performing data analysis, it is often important to understand the
shape of a data distribution.
\begin{itemize}
\item Check for outliers.
\item Help decide on appropriate models.
\item Exploratory data analysis.
\end{itemize}

\sk The best-known technique for visualizing a distribution is the
{\rd histogram}.
\begin{itemize}
\item \R{hist()} automates the drawing of histograms by choosing an
  appropriate number of bins.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\nochap 
For example, consider the number of plate appearances (PAs)
for batters during the 2008 MLB season.

{\bl
\begin{verbatim}
> bat08 <- transform(bat08, PA=AB+BB+HBP+SF+SH)
> hist(bat08$PA, xlab="PA", main="")
\end{verbatim}
}

\begin{center}
  \includegraphics[scale=0.5,trim=10 10 0 30]{PAhist}
\end{center}
\end{frame}

\begin{frame}[fragile]
\nochap So there were many players with $<50$ plate appearances.
\begin{itemize}
\item In further analysis it may be sensible to exclude these players.
\end{itemize} 

\sk
A closely related type of chart is the {\rd density} plot.  
{\bl 
\begin{verbatim}
> plot(density(bat08[bat08$PA > 25, "PA"]),
+      main="", xlab="PA > 25")
\end{verbatim}
}
\begin{itemize}
\item \R{density()} constructs a {\rd kernel density} estimator with
  an optimally chosen {\rd bandwidth}.
\item It does not create a plot, but it does have a plot method.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\nochap

{\bl
\begin{verbatim}
> rug(bat08[bat08$PA > 25, "PA"])
\end{verbatim}
}
\begin{itemize}
\item adds a strip-plot along the $x$-axis
\end{itemize}

\nsk
\begin{center}
  \includegraphics[scale=0.6,trim=10 10 0 30]{PAdens}
\end{center}

\end{frame}

\begin{frame}
\chap{Box Plots}

A box plot (or {\rd box and whiskers plot}) is a compact summary of
the distribution of a variable  amenable to showing several
similarly measured variables along side for comparison.

\sk It shows the
\begin{itemize}
\item {\rd Interquartile range (IQR)} as a box containing the 25th and 75th percentile
\item {\rd median} as a line through the box 
\item {\rd whiskers} showing {\em adjacent values}: the most extreme
  values less than or equal to $\pm 1.5 \times$ the length of the IQR
\item {\rd outside values} falling outside the whiskers are plotted
  individually as points.
\end{itemize}

\end{frame}

\begin{frame}
\nochap

\begin{center}
\includegraphics[scale=0.3,trim=0 0 0 30]{boxplot}
\end{center}

\end{frame}

\begin{frame}[fragile]
\nochap

E.g.,  2008 AL on-base percentage (OBP) by team.

{\bl \footnotesize
\begin{verbatim}
> bat08 <- transform(bat08, OBP=(H+BB+HBP)/PA,
+     teamID=as.character(teamID))
> boxplot(OBP~teamID, las=3,
+     data=bat08[bat08$PA>100 & bat08$lgID=="AL",])
\end{verbatim}
}

\begin{center}
  \includegraphics[scale=0.55,trim=10 10 0 40]{obpbox}
\end{center}

\end{frame}

\begin{frame}[fragile]
\chap{Saving Plots}

The best way to script the export of a plot is with one of the built
in {\rd graphics devices} like \R{pdf()}, \R{png()}, ...

\sk Here is how I exported the plot on the previous slide.
{\bl \footnotesize
\begin{verbatim}
> pdf("objplot.pdf", width=7, height=4)
> boxplot(OBP~teamID, las=3,
+    data=bat08[bat08$PA > 100 & bat08$lgID=="AL",])
> dev.off()
\end{verbatim}
}

The \R{dev.off()} bit is essential.  
\begin{itemize}
\item Without it, all future plots will added to the PDF.
\item \R{png()} is a nice way to save larger images.
\end{itemize}

\sk The Windows and RStudio GIUs have save buttons.
\end{frame}

\begin{frame}
\nochap

\begin{center}
  \includegraphics[scale=0.3, trim=200 200 200 160]{savepdf}
\end{center}

\end{frame}

\begin{frame}
\chap{Customizing}

Almost every aspect of a plot can be customized.
\begin{itemize}
\item Plotting commands often have dozens of optional arguments that
  control how things look.
\item They also almost always have ellipses ({\R ...})  which,
  eventually, change low-level graphical parameters through \R{par()}.
\end{itemize}

\sk Through \R{par()} you can control the number of plotting widows,
their margins, background color, and more.
\begin{itemize}
\item What \R{par()} doesn't handle is passed (again through \R{...})
  to high-level functions like \R{axis()}, \R{title()}, \R{points()}, etc.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\nochap

Probably the two most common uses of \R{par()} are

\sk
\begin{enumerate}
\item Turning off the axes with {\bl \verb!xaxt="n"!} or {\bl \verb!yaxt="n"!}, or
  \R{ann=FALSE}.
\begin{itemize}
\item Then they can be customized with \R{axis()}.
\end{itemize}
\sk 
\item To create multiple plotting windows within one graphical device.
\begin{itemize}
\item E.g., using \R{par(mfrow=c(nrow, ncol))}.
\item It can help to adjust the margins to reduce the amount of
  whitespace.
\end{itemize}
\end{enumerate}
\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.25cm}
{\bl \footnotesize
\begin{verbatim}
par(mfrow=c(2,2), mar=c(4,2,1,1)+0.1)
pie(c(5,4,3)); plot(x=1:5, y=c(1.1, 1.9, 3, 3.9, 6))
barplot(1:5); acf(turkey, main="")
\end{verbatim}
}

\begin{center}
  \includegraphics[scale=0.43, trim=0 0 0 0]{fourplots}
\end{center}
\end{frame}

\begin{frame}
\chap{Add-on Packages}

There are lots of add-on plotting packages.  

\sk The two most popular are
\begin{itemize}
\item \R{lattice}, which is well-suited to splitting data by a
  conditioning variable. 
\item \R{ggplot2}, which provides stunning charts, but it has a very
  steep learning curve. 
\end{itemize}

\sk Both occupy whole chapters in books.
\end{frame}

% \begin{frame}
% \chap{``Homework''}

% Use charts to explore the 1970s UK teacher's pay survey data in the
% file \R{teach.csv} available on the course web page.  
% \begin{itemize}
% \item Info on the data columns is available in the CSV file.
% \end{itemize}

% Some things to consider include. 
% \begin{enumerate}
% \item Plot \R{salary} v.~the number of \R{months} in service, while
%   also indicating \R{sex}.  What do you find?
% \item Ignore \R{months} and use \R{boxplot()}s to explore the
%   distribution of each of the levels of \R{sex}, \R{marry}, \R{degree},
%   \R{type}, \R{train}, and \R{break}.
% \item Consider the part of the data for teachers whose school
%   offers a \R{degree} of type ``0'', and revisit 1.  Plot and try a
%   augmenting with the best fitting regression line/intervals.
% \end{enumerate}

% \vspace{-0.25cm}
% \end{frame}

\end{document}
