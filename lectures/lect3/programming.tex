\documentclass[12pt,xcolor=svgnames]{beamer}
\usepackage{dsfont,natbib,setspace}
\mode<presentation>

% replaces beamer foot with simple page number
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}{
  \vspace{-1.5cm}
  \raisebox{10pt}{\makebox[\paperwidth]{\hfill\makebox[20pt]{\color{gray}\scriptsize\insertpagenumber}}}}

% colors
\newcommand{\bk}{\color{black}}
\newcommand{\rd}{\color{red}}
\newcommand{\fg}{\color{forestgreen}}
\newcommand{\bl}{\color{blue}}
\newcommand{\gr}{\color{gray}}
\newcommand{\theme}{\color{Maroon}}

% common math markups
\newcommand{\bs}[1]{\boldsymbol{#1}}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mr}[1]{\mathrm{#1}}
\newcommand{\bm}[1]{\mbox{\boldmath $#1$}}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\ds}[1]{\mathds{#1}}

% spacing and style shorthand
\newcommand{\sk}{\vspace{.4cm}}
\newcommand{\nochap}{\vspace{0.6cm}}
\newcommand{\nsk}{\vspace{-.4cm}}
\newcommand{\chap}[1]{{\theme \Large \bf #1} \sk}
\newcommand{\R}[1]{{\bl\tt #1}}
\newcommand{\til}{{\footnotesize$\bs{\stackrel{\sim}{}}$ }}

% specific stats markups for this doc
\newcommand{\E}{\ds{E}}
\newcommand{\Reals}{\ds{R}}
\newcommand{\var}{\text{var}}
\newcommand{\cov}{\text{cov}}
\newcommand{\mT}{\mc{T}}
\newcommand{\GP}{\mc{GP}}
\newcommand{\iidsim}{\stackrel{\mathrm{iid}}{\sim}}
\newcommand{\indsim}{\stackrel{\mathrm{ind}}{\sim}}
\newcommand{\mN}{\mc{N}}

\begin{document}

{ %\usebackgroundtemplate{\includegraphics[height=\paperheight]{phoenix}}
\thispagestyle{empty}
\setcounter{page}{0}

\title{\Large \theme 
\includegraphics[scale=0.1]{../Rlogo}\\
\bf {\sf Programming}}
\author{\vskip .5cm {\bf Robert B.~Gramacy}\\{\color{Maroon}
    Virginia Tech}
  Department of Statistics\\
  \vskip .2cm \texttt{\rd bobby.gramacy.com}
  \vskip .25cm}
\date{}
\maketitle }

% doc spacing
\setstretch{1.1}

\begin{frame}[fragile]
\chap{Functions}

Function objects are defined with this syntax
\vspace{0.25cm}
\begin{quote}
\R{function(}arguments\R{)} body
\end{quote}

\nsk
where 

\begin{itemize}
\item {\em arguments} is a set of symbol names (and, optionally,
default values) that will be defined within the body of the function,
\item and {\em body} is an {\sf R} expression---usually enclosed in
  curly braces, but that's not necessary for single expressions.
\end{itemize}

{\bl
\begin{verbatim}
> f <- function(x,y) x + y
> f <- function(x,y) { x + y }
\end{verbatim}
}
\vspace{-0.5cm}
\end{frame}

\begin{frame}[fragile]
\chap{Arguments and defaults}

If you specify a default value for an argument, then it is considered
optional.

{\bl
\begin{verbatim}
> f(1,2)
[1] 3
> g <- function(x, y=10) { x + y }
> g(1)
[1] 11
> f(1)
Error in x + y : 'y' is missing
> g(1,2)
[1] 3
\end{verbatim}
}
\vspace{-0.5cm}
\end{frame}

\begin{frame}[fragile]
\nochap

\nsk
\begin{itemize}
\item You will only get an error if you try to use the uninitialized
  argument within the function body.
\item So functions can have {\rd optional} arguments, if you
  provide extra checks and conditional execution.

{\bl
\begin{verbatim}
> h <- function(x, y) {
+   args <- as.list(match.call())
+   if(is.null(args$y)) {
+     y <- 10
+   }
+   x + y
+ }
> h(1)
[1] 11
\end{verbatim}
}
\end{itemize}

\end{frame}

\begin{frame}
\nochap

It is often convenient to specify a {\rd variable-length argument list}.
\begin{itemize}
\item You may want to pass extra arguments to another function,
\item or you may want the function to accept a variable number of arguments.
\end{itemize}

\sk You can do this in {\sf R} with {\rd ellipsis} (\R{...}) in the
argument list.
\end{frame}

\begin{frame}[fragile]
\nochap 

\vspace{-0.25cm}
Here is a function that prints the first argument and then passes the
other arguments to \R{summary()}.

{\bl
\begin{verbatim}
> f <- function(x, ...) {
+   print(x)
+   summary(...)
+ }
> v <- sqrt(1:100)
> f("Summarizing v:", v, digits=2)
[1] "Summarizing v:"
   Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
    1.0     5.1     7.1     6.7     8.7    10.0 
\end{verbatim}
}
\begin{itemize}
\item All of the arguments after \R{x} were passed to \R{summary()}.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.25cm}
You can read arguments from the variable-length list.

{\bl
\begin{verbatim}
> addemup <- function(x, ...) {
+   args <- list(...)
+   for(a in args) x <- x + a
+   x
+ }
> addemup(1, 2, 3, 4, 5)
[1] 15
\end{verbatim}
}

\begin{itemize}
\item You can refer to items within \R{...} with \R{..1}, \R{..2},
  etc.,
\item and named arguments become valid symbols within the body of the
  function.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\chap{Return values}

Like with many other languages there is a \R{return()} function to
explicitly specify the value being returned.  
\begin{itemize}
\item It is most useful when the object you wish to return is not the
  last expression in the function.
\end{itemize}
{\bl
\begin{verbatim}
> f <- function(a, b) {
+   if(a < b) return(a)
+   b
+ }
> f(1,2)
[1] 1
\end{verbatim}
}

Explicit \R{return()}s may lead to cleaner code.
\vspace{-0.25cm}
\end{frame}

\begin{frame}[fragile]
\chap{Functions as arguments}

Many functions in {\sf R} take other functions as arguments.

\begin{itemize}
\item \R{sapply()} is a good example; it iterates through each element
  of a vector, applying a function to each and returning the results.
\end{itemize}

{\bl
\begin{verbatim}
> a <- 1:7
> sapply(a, sqrt)
[1] 1.000000 1.414214 1.732051 2.000000
[5] 2.236068 2.449490 2.645751
\end{verbatim}
}
\begin{itemize}
\item This is a little silly since \R{sqrt} is already {\rd
    vectorized}, as are many built-in functions in {\sf R}.
  \hfill (see \R{.R} file)
\end{itemize}
\vspace{-0.25cm}
\end{frame}

\begin{frame}[fragile]
\nochap 

It is sometimes expedient to create functions without names, i.e.,
{\rd anonymous functions},
\begin{itemize}
\item usually to pass them as arguments to other functions.
\end{itemize}

\sk
Here is a simple example.
{\bl
\begin{verbatim}
> apply.to.three <- function(f) { f(3) }
> apply.to.three(function(x) { x * 7 })
[1] 21
\end{verbatim}
}
\begin{itemize}
\item Essentially, anonymous functions bypass assigning a function
  object to a symbol.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\nochap

This is most common with the ``apply'' family of functions.  E.g.,

\sk
Instead of
{\bl
\begin{verbatim}
> v <- 1:20
> w <- NULL
> for(i in 1:length(v)) { w[i] <- v[i]^2 }
\end{verbatim}
}
the following is a lot clearer, but may not be faster:
{\bl
\begin{verbatim}
> w <- sapply(v, function(i) { i^2 })
\end{verbatim}
}
\hfill (see \R{.R} file)
\end{frame}

\begin{frame}[fragile]
\nochap

Functions exist to manipulate the behavior of other functions.  E.g.,
you can separately access, and change, the arguments and body of a
function.

{\bl
\begin{verbatim}
> f <- function(x, y=1, z=2) { x + y + z }
> ff <- formals(f)
> ff$y <- 3
> formals(f) <- ff
> body(f) <- expression({ x * y * z })
> f
function (x, y = 3, z = 2) 
{
    x * y * z
}
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\chap{Argument Order and Names}

When you specify a function in {\sf R}
\begin{itemize}
\item you assign a name to each argument
\item arguments passed into the function are {\rd copied} from the
  calling environment
\item and inside the function body you can access them by their
  argument name.
\end{itemize}

\sk Consider the following simple function.
{\bl
\begin{verbatim}
> addTheLog <- function(first, second) {
+   first + log(second)
+ }
\end{verbatim}
}
\vspace{-0.5cm}
\end{frame}

\begin{frame}[fragile]
\nochap
When calling, you can specify arguments in three ways.
\begin{enumerate}
\item Argument order
{\bl
\begin{verbatim}
> addTheLog(1, exp(4))
[1] 5
\end{verbatim}
}
\item Exact names (in any order).
{\bl
\begin{verbatim}
> addTheLog(second=exp(4), first=1)
[1] 5
\end{verbatim}
}
\item Partial names (in any order).
{\bl
\begin{verbatim}
> addTheLog(s=exp(4), f=1)
[1] 5
\end{verbatim}
}
\end{enumerate}
Combinations are allowed ...
\end{frame}

\begin{frame}
\nochap

\begin{itemize}
\item It is common to specify the first few arguments in order, and
  the latter few (optional) arguments by name, leaving others to
  defaults.
\item Other mixes are confusing.
\end{itemize}

\sk When calling generic functions, you cannot specify the first
argument's name, whose class defines the method.
\begin{itemize}
\item E.g., for an \R{lm} object called \R{lmo}, do \R{plot(lmo)} not
  \R{plot(x=lmo)}.
\item The rest of the arguments can be specified by name.
\end{itemize}
\end{frame}

\begin{frame}
\chap{Side Effects}

All functions in {\sf R} return a value.

\sk Some functions also do other things:
\begin{itemize}
\item change variables in the current environment (or in other
  environments)
\item plot graphics,
\item load or save files,
\item access the network.
\end{itemize}

\sk Some we've seen already (using \R{parent.frame()}), and some are the
topic of future lectures.
\end{frame}

\begin{frame}[fragile]
\nochap

An important function that causes side effects is {\bl \verb!<<-!}.  E.g.,

\vspace{0.25cm}
\begin{quote}
var \R{<<-} value
\end{quote}

It causes the interpreter  to first search through the
current environment to find \R{var}.  If its not defined then it will
look in the parent environment, etc.
\begin{itemize}
\item Ultimately creating a new variable called \R{var} in the global
  environment if such a symbol is not found.
\item Otherwise (re)-assigning to the first \R{var} symbol it finds
  the object \R{value}.
\end{itemize}

\end{frame}

\begin{frame}[fragile]
\nochap

{\bl
\begin{verbatim}
> x
Error: object 'x' not found
> doesnt.assign.x <- function(i) x <- i
> doesnt.assign.x(4)
> x
Error: object 'x' not found
> assigns.x <- function(i) x <<- i
> assigns.x(4)
> x
[1] 4
\end{verbatim}
}

\end{frame}

\begin{frame}
\chap{Recursion}

Recursive programming is a powerful programming technique, made
possible by functions.  
\begin{itemize}
\item A {\rd recursive} program is simply one that calls itself.
\item This is useful because many algorithms and mathematical formulas
  are recursive in nature.
\end{itemize}

\sk Consider the Fibonacci sequence:
\begin{align*}
F[1] &= F[2] = 1\\
F[n] &= F[n-1] + F[n-2], \;\;\; n=3,4,5,\dots
\end{align*}

\end{frame}

\begin{frame}[fragile]
\nochap

Here is one recursive implementation.
{\bl
\begin{verbatim}
> fib1 <- function(n, verb=0) {
+   if(verb > 0)
+     cat("called fact1(", n, ")\n", sep="")
+   if(n == 1 || n == 2) return(1)
+   else return(fib1(n-1, verb) + fib1(n-2, verb))
+ }
> fib1(5)
[1] 5
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap
But this is inefficient since it duplicates effort.
{\bl
\begin{verbatim}
> fib1(5, verb=1)
called fact1(5)
called fact1(4)
called fact1(3)
called fact1(2)
called fact1(1)
called fact1(2)
called fact1(3)
called fact1(2)
called fact1(1)
[1] 5
\end{verbatim}
}
\nsk\nsk
\hfill Is there a better way?
\end{frame}

\begin{frame}
\nochap

\vspace{-0.25cm}
How about a less trivial example, where recursion is essential.

\sk The {\em Sieve of Eratosthenes} is an algorithm for finding all
primes less than or equal to a given number $n$.

\sk Here are the steps.
\begin{enumerate}
\item Start with the list $2, 3, \dots, n$ and the largest known prime
  $p=2$.
\item Remove from the list all elements that are multiples of $p$ (but
  keep $p$ itself).
\item Increase $p$ to the smallest element of the remaining list that
  is larger than the current $p$.
\item If $p$ is larger than $\sqrt{n}$ then stop, otherwise go to 2.
\end{enumerate}
\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.5cm}
{\bl
\begin{verbatim}
> primesieve <- function(siev, unsiev, v=0) {
+     if(v > 0) {
+       cat("sieved", siev, "\n")
+       cat("unsieved", unsiev, "\n");
+     }
+     p <- unsiev[1]
+     n <- unsiev[length(unsiev)]
+     if(p^2 > n) return(c(siev, unsiev))
+     else {
+       unsiev <- unsiev[unsiev %% p != 0]
+       siev <- c(siev, p)
+       return(primesieve(siev, unsiev, v))
+     }
+   }
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap

{\bl 
\begin{verbatim}
> primesieve(c(), 2:200)
 [1]   2   3   5   7  11  13  17  19  23  29
[11]  31  37  41  43  47  53  59  61  67  71
[21]  73  79  83  89  97 101 103 107 109 113
[31] 127 131 137 139 149 151 157 163 167 173
[41] 179 181 191 193 197 199
\end{verbatim}

\begin{itemize}
\item It is a terse but clever program that is faster than many alternatives.
\end{itemize}
}

\end{frame}

\begin{frame}
\nochap

\vspace{-0.5cm}
Just to keep the example going ...

\sk
Let $\rho(n)$ be the number of primes less than or equal to $n$.  Both
Legendre and Gauss famously asserted that
\[
\lim_{n\rightarrow\infty} \frac{\rho(n) \log(n)}{n} \rightarrow 1.
\]

\sk 
The result was eventually proved some time later by Hadamard and
de la Vall\'ee Poussin in 1896.

\begin{itemize}
\item The proof is hard, but we can easily check the result
  numerically.
\end{itemize}

\end{frame}

\begin{frame}[fragile]
\nochap
\begin{itemize}
\item We'll use our new \R{primesieve()} and the built-in \R{cumsum()}
  function for cumulative sums of a vector.
\end{itemize}

{\bl
\begin{verbatim}
> n <- 10000
> ps <- primesieve(c(), 2:n)
> primes <- rep(0, n-1)
> primes[ps-1] <- 1
> density <- cumsum(primes)/(2:n)

> plot(2:n, density, type="l", 
+      main="prime density",
+      xlab="n", ylab="density", lwd=2)
> lines(2:n, 1/log(2:n), col="red")
\end{verbatim}
}

\end{frame}

\begin{frame}[fragile]
\nochap

\nsk\nsk

\begin{center}
\includegraphics[scale=0.35,trim=20 30 0 20]{primedens}
\end{center}

\nsk
{\bl
\begin{verbatim}
> plot(2:n, density*log(2:n), type="l", xlab="n",
+  ylab="", main="prime density * log(n)", lwd=2)
\end{verbatim} 

}
\end{frame}

\begin{frame}[fragile]
\chap{Preallocation and Vectorization}

{\sf R} code will be faster if you can reduce the number of times new
memory is allocated.  E.g.,

{\bl
\begin{verbatim}
> seq1 <- function(n) {
+     F <- c(1,1)
+     for(i in 3:n) F[i] <- F[i-1]/F[i-2] + F[i-2]
+     F
+   }
> system.time(s1 <- seq1(100000))
   user  system elapsed 
 25.732   5.489  33.091 
\end{verbatim}
\vspace{-0.5cm}
}
\end{frame}

\begin{frame}[fragile]
\nochap

Its way faster if to first create a right-sized \R{F} so it doesn't
have to be re-allocated (automatically) each iteration.

{\bl
\begin{verbatim}
> seq2 <- function(n) {
+   F <- rep(NA, n)
+   F[1:2] <- 1
+   for(i in 3:n) F[i] <- F[i-1]/F[i-2] + F[i-2]
+   F
+ }
> system.time(s2 <- seq2(100000))
   user  system elapsed 
  0.498   0.000   0.501 
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.25cm}
Its also faster if you can avoid \R{for}
loops altogether. Consider a function summing the first $n$ squares.
{\bl
\begin{verbatim}
> s2 <- function(n) {
+   S <- 0
+   for(i in 1:n) S <- S + i^2
+   S
+ }
> system.time(s2(1000000))
   user  system elapsed 
  1.028   0.005   1.089 
> system.time(sum((1:1000000)^2))
   user  system elapsed 
  0.035   0.000   0.036 
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.25cm}
Many of {\sf R}'s functions are {\rd vectorized}
\begin{itemize}
\item which means that if the first argument is a vector, then the
  output will be a vector of the same length,
\item computed by applying the function element-wise.
\item Vectorization (usually) allows for faster executing code that is
  easier to read.
\item User-defined functions can also be vectorised if they comprise
  of functions that are innately vectorised, or are invoked using one of
  the \R{apply} family of functions.
\end{itemize}


\sk Here are a collection of different approaches to solving the
problem of summing across the columns of a matrix.
{\bl
\begin{verbatim}
> bigM <- matrix(runif(1e+07), nrow=1000)
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap

Using a double-\R{for} loop:

{\bl
\begin{verbatim}
> csfor <- function(M) {
+   cs <- rep(NA, ncol(M))
+   for(i in 1:ncol(M)) {
+     s <- 0
+     for(j in 1:nrow(M)) s <- s + M[j, i]
+     cs[i] <- s
+   }
+ }
> system.time(cs1 <- csfor(bigM))
   user  system elapsed 
 10.096   0.020  10.166 
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.3cm}
Using \R{apply()}, which works like \R{sapply()}, except iterates over
rows or columns of a matrix.

{\bl
\begin{verbatim}
> system.time(cs2 <- apply(bigM, 2, sum))
   user  system elapsed 
  0.276   0.087   0.367 
\end{verbatim}
}

Using a single loop of \R{sum()}s.
{\bl
\begin{verbatim}
> system.time({
+  cs3 <- rep(NA, ncol(bigM))
+  for(i in 1:ncol(bigM)) cs3[i] <- sum(bigM[,i])
+ })
   user  system elapsed 
  0.218   0.021   0.239 
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap

Using the built-in \R{colSums()} function.
{\bl
\begin{verbatim}
> system.time(cs4 <- colSums(bigM))
   user  system elapsed 
  0.016   0.000   0.016 
\end{verbatim}
}

\sk Why is this fastest?
\begin{itemize}
\item Because its implemented in \sf{C}.  Most built-in functions are.
\item Interpreting {\sf R} code is much slower than executing compiled
  {\sf C} code.
\end{itemize}
\end{frame}

\begin{frame}
\chap{Matrix operations}

{\sf R}'s matrix/vector linear algebra routines are state-of-the-art.  
\begin{itemize}
\item They are linked against to {\sf BLAS/Lapack} libraries like {\sf MATLAB}.
\item But only the product is in operator form.
\end{itemize}

\sk Suppose you wish to calculate 
\[
\hat{\beta} = (X^\top X)^{-1} X^\top y,
\]
i.e., the OLS estimator for a linear model.
\end{frame}

\begin{frame}[fragile]
\nochap

Here is how we'd do that in {\sf R} commands.
{\bl
\begin{verbatim}
> X <- cbind(1, as.matrix(trees[,1:2]))
> y <- trees[,3]
> XtX <- t(X) %*% X
> XtXi <- solve(XtX)
> XtXiXt <- XtXi %*% t(X)
> XtXiXt %*% y
              [,1]
       -57.9876589
Girth    4.7081605
Height   0.3392512
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap 

Or, in one expression:
{\bl
\begin{verbatim}
> beta.hat <- solve(t(X) %*% X) %*% t(X) %*% y
> beta.hat
              [,1]
       -57.9876589
Girth    4.7081605
Height   0.3392512
\end{verbatim}
}

\sk And just to double-check
{\bl 
\begin{verbatim}
> coef(lm(y~X[,1]+X[,2]))
(Intercept)      X[, 1]      X[, 2] 
-57.9876589   4.7081605   0.3392512 
\end{verbatim}
}

\end{frame}

\begin{frame}
\nochap

Matrix/vector multiplication and inversion are perfect examples
of vectorized operations you wouldn't want to re-code.
\begin{itemize}
\item An {\sf R} implementation would be cumbersome.
\item Even a bespoke {\sf C} implementation of specific operations
  would be folly.
\end{itemize}

\sk The libraries doing the work are heavily optimized.
\begin{itemize}
\item Only a bespoke {\em compiled} implementation of a long chain of
  matrix/vector operations has a chance of being faster,
\item and only one that also used the same underlying libraries.
\item Why? Because {\sf R} is uses {\rd immutable objects}.
\end{itemize}


\end{frame}

\begin{frame}
\chap{Object Oriented Programming}

At its heart, {\sf R} is a {\em functional} programming language.

\sk But it includes some support for {\rd OOP}, which has become a popular
paradigm for organizing computer software.
\begin{itemize}
\item In a nutshell, OOP means that objects/data structures are
  grouped together under a \R{class} label
\item and associated with {\rd methods} which are functions that operate
  on objects in the \R{class}.
\item Usually the names of the {\rd methods} are shared across classes,
  but do something particular depending on the \R{class} of the object
  supplied.
\end{itemize}
\end{frame}

\begin{frame}
\nochap
Confusingly, {\sf R} includes two different mechanisms for OOP, which
has to do with the {\sf S/S-plus} legacy.
\begin{itemize}
\item {\rd S3}-classes, which were introduced in {\sf S}, version 3.
\item {\rd S4}-classes, from {\sf S}, version 4.
\end{itemize}

\sk 
{\rd S4} classes are more sophisticated, complicated, and more
{\em truly} OO.

\sk But {\rd S3} are simpler and more common and leverage many of the
built-in {\rd generic} methods we are getting familiar with:
\begin{itemize}
\item \R{plot}, \R{print}, \R{summary}, etc.
\item So we'll focus on {\rd S3}.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.25cm}
Establishing a new {\rd S3} class is as simple as setting a \R{class}
attribute.

{\bl
\begin{verbatim}
> myobj <- list(v=sample((1:20)^2), m="squares")
> class(myobj) <- "myclass"
> myobj
$v
 [1]   1  16 400 289 169 361 324 100  64   9
[11] 225 256  36 196 121  49  25   4 144  81

$m
[1] "squares"

attr(,"class")
[1] "myclass"
\end{verbatim}
}

\end{frame}

\begin{frame}[fragile]
\nochap

\begin{itemize}
\item Now we have the bare bones needed to start defining methods
  associated with the class {\bl \verb!"myclass"!}.
\end{itemize}

\sk Its easiest to extend existing {\rd generic} methods. E.g.,

{\bl
\begin{verbatim}
> print.myclass <- function(x, ...) {
+   cat("This is a myclass object containing\n")
+   cat("a", class(x$v), "vector ")
+   cat("with", length(x$v), x$m, "\n")
+ }

> myobj
This is a myclass object containing
a numeric vector with 20 squares 
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap

How does it work?  

\begin{itemize}
\item Lets look at how \R{print()} is
defined.
\end{itemize}
{\bl
\begin{verbatim}
> print
function (x, ...) 
UseMethod("print")
<bytecode: 0x7fcf4476c1c8>
<environment: namespace:base>
\end{verbatim}
}
So when you call the \R{print()} function, it calls \R{UseMethod()},
which looks at the \R{class} attribute of \R{x} and then (attempts to)
call a function named \em{print.class(x, ...)}.
\end{frame}

\begin{frame}[fragile]
\nochap

\begin{itemize}
\item The new {\rd method}'s arguments must, at minimum, match those
  of the {\rd generic} method.
{\bl
\begin{verbatim}
> names(formals(print))
[1] "x"   "..."
\end{verbatim}
}
\item You can still inspect the contents of the object.
{\bl
\begin{verbatim}
> names(myobj)
[1] "v" "m"
> myobj$v
 [1]   1   4   9  16  25  36  49  64  81 100
[11] 121 144 169 196 225 256 289 324 361 400
\end{verbatim}
}
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\nochap
\R{plot} and \R{summary} are other popular {\rd generic} methods.
{\bl
\begin{verbatim}
> plot.myclass <- function(x, ...) {
+   plot(x$v, ylab=x$m, ...)
+ }
> plot(myobj, main="Parabola!")
> summary.myclass <- function(x, ...)
+   {
+     cat("Your", x$m, "are summarized as\n")
+     summary(x$v)
+   }
> summary(myobj)
\end{verbatim}
}
\hfill (see \R{.R} session for output)
\end{frame}

\begin{frame}[fragile]
\nochap 

\vspace{-0.5cm}
\begin{itemize}
\item Use the \R{methods()} function see the methods defined in the
  current environment for certain generic functions
{\bl
\begin{verbatim}
> methods(plot)
...
[13] plot.HoltWinters*   plot.isoreg*       
[15] plot.lm             plot.medpolish*    
[17] plot.mlm            plot.myclass       
[19] plot.ppr*           plot.prcomp*       
...
\end{verbatim}
}
\item or the methods defined for a particular class
{\bl
\begin{verbatim}
> methods(class="myclass")
[1] plot.myclass    print.myclass  
[3] summary.myclass
\end{verbatim}
}
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\nochap
\vspace{-0.25cm}
You can make your own generic methods too.
{\bl
\begin{verbatim}
> penultimate <- function(x) 
+     UseMethod("penultimate")
> penultimate.myclass <- function(x) {
+   sort(x$v)[length(x$v)-1]
+ }
> penultimate(myobj)
[1] 361
> penultimate(myobj$v)
Error in UseMethod("penultimate") : 
  no applicable method for 'penultimate' applied 
  to an object of class "c('double', 'numeric')"
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap
\vspace{-0.25cm}
\begin{itemize}
\item We could fix that!
\end{itemize}
{\bl
\begin{verbatim}
> penultimate.numeric <- function(x) {
+   sort(x)[length(x)-1]
+ }
> penultimate(myobj$v)
[1] 361
> M <- matrix(1:10, nrow=2)
> penultimate(M)
[1] 9
\end{verbatim}
}
\begin{itemize}
\item Notice how {\rd coercion} allows the generic method for numeric
  classes to be applied to matrix objects.
\end{itemize}
\end{frame}

\begin{frame}
\chap{An example: root finding by bisection}

Suppose that $f: \mathbb{R} \rightarrow \mathbb{R}$ is a continuous
function.  
\begin{itemize}
\item A {\rd root} of $f$ is a solution to the equation $f(x) = 0$.
\item That is, a root is a number $a\in \mathbb{R}$ such that $f(a) =
  0$.
\item If we draw the graph of our function, say $y=f(x)$, which is a
  curve in the plane, a solution of $f(x) = 0$ is the $x$-coordinate
  of a point at which the curve crosses the $x$-axis.
\end{itemize}

\sk Roots are important for factorizing polynomials and minimizing
functions, and this is not always easy to do analytically.
\end{frame}

\begin{frame}
\nochap

\nsk\nsk
\begin{center}
\includegraphics[scale=0.32,trim=27 0 0 0]{root}
\end{center}
These are some of the roots of $\cos(x)$.
\end{frame}

\begin{frame}
\nochap

One of the best {\em numerical} methods for root finding of 1-d
functions $f(x)$ is the {\rd bisection method}.

\sk
If $f$ is a continuous function, then $f$ has a root in the interval
$(x_l, x_r)$ if either 
\begin{itemize}
\item $f(x_l) < 0$ and $f(x_r) > 0$, or
\item $f(x_l) > 0$ and $f(x_r) < 0$.
\end{itemize}

\sk The bisection method works by 
\begin{itemize}
\item taking an interval $(x_l, x_r)$ that
contains a root, 
\item then successively refining $x_l$ and $x_r$ until $x_r - x_l <
  \varepsilon$, where $\varepsilon$ is some pre-defined tolerance.
\end{itemize}
\end{frame}

\begin{frame}
\nochap

Here is the algorithm in pseudocode.

\sk Start with inputs $x_l < x_r$ such that $f(x_l) f(x_r) < 0$, and a
desired tolerance $\varepsilon$.
\begin{enumerate}
\item If $x_l - x_r < \varepsilon$ then stop.
\item Let $x_m \leftarrow (x_l + x_r) / 2$; if $f(x_m) = 0$ stop.
\item If $f(x_l) f(x_m) < 0$ then let $x_r \leftarrow x_m$; otherwise
  let $x_l \leftarrow x_m$.
\item Goto 1.
\end{enumerate}

\end{frame}

\begin{frame}
\nochap 

Note that at every iteration of the algorithm we know there is a
root in the interval $(x_l, x_r)$, so
\begin{itemize}
\item it is guaranteed to converge
\item with the approximation error
  reducing by $1/2$ at each iteration.
\item If it stops when $x_r - x_l < \varepsilon$, then we know that
  both $x_l$ and $x_r$ are within distance $\varepsilon$ of a root.
\end{itemize}

\sk (The code, which is included in the accompanying \R{.R} file, is
too long to paste here.)
\end{frame}

\begin{frame}[fragile]
\nochap 

\vspace{-0.25cm}
We'll illustrate the method on
\[
f(x) = \log(x) - \exp(-x), \;\;\; x \in (1, 2).
\]

{\bl
\begin{verbatim}
> f <- function(x) log(x) - exp(-x)
> fr <- bisection(f, 1, 2)
> fr
Root of:
function(x) log(x) - exp(-x)
in (1, 2) found after 27 iterations: 1.3098
to a tolerance of 1.490116e-08
\end{verbatim}
\begin{itemize}
\item ... we have augmented the generic \R{print()} method
\end{itemize}
}
\end{frame}

\begin{frame}[fragile]
\nochap

... and the \R{summary()} and \R{plot()} methods too.

{\bl
\begin{verbatim}
> plot(fr, ...)
> plot(fr, ..., after=20)
\end{verbatim}
}

\nsk
\begin{center}
\includegraphics[scale=0.65,trim=15 5 0 25]{bisection}
\end{center}
\end{frame}

% \begin{frame}
% \chap{``Homework''}

% The bisection method can be generalized to deal with the case $f(x_l)
% f(x_r) > 0$, by {\em broadening} the bracket.  
% \begin{itemize}
% \item That is, we reduce $x_l$ and/or increase $x_r$, and try again.
% \item A reasonable choice is to double the width of the interval,
%   i.e., 
% \begin{align*}
% m &\leftarrow (x_l + x_r)/2, & w &\leftarrow x_r - x_l \\
% x_l&\leftarrow m - w, & x_r &\leftarrow m + w.
% \end{align*}
% \end{itemize}

% \begin{enumerate}
% \item Incorporate bracketing into the \R{bisection()} function we coded.
% \end{enumerate}
% \end{frame}

% \begin{frame}
% \nochap

% Note that broadening is {\em not guaranteed} to find $x_l$ and $x_r$
% such that $f(x_l) f(x_r) \leq 0$, 
% \begin{itemize}
% \item so you should include a limit on the number of times it can be tried.
% \end{itemize}

% \sk Use your modified function to 
% \begin{enumerate}
% \item[2.] find a root of $f(x)$, given above,
% but with a different starting interval not containing the root we
% found;
% \item[3.] and to find the root of
% \[
% h(x) = (x - 1)^3 - 2x^2 + 10 - sin(x),
% \]
% starting with $x_l = 1$ and $x_r = 2$.
% \end{enumerate}
% \end{frame}

\end{document}
