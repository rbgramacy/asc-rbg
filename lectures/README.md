# Lectures

Subdirectories herin contain the source files for lectures delivered in-class, including R examples and data sets, for **Advanced Statistical Computing**.

## Breakdown

- **lect1**: R computing basics (`basics.tex`)
- **lect2**: R fundamentals (`fundamentals.tex`)
- **lect3**: R programming (`programming.tex`)
- **lect4**: R debugging and profiling (`debug.tex`)
- **lect5**: Monte Carlo and parallelization (`mc.tex`)
- **lect6**: Working with compiled code including Rcpp (`compiled.tex` and `Rcpp.Rmd`)
- **lect7**: Working with data (`data.tex`)
- **lect8**: Plotting and visualization (`plots.tex`), including `ggplot` and `plotly`
- **lect9**: Regular expressions, `sed` and `awk`

## Notes

[Tim Warburton](https://www.paranumal.com/) teaches a similar class to CMDA undergraduates, and [this slide](cmda3646slide.png) offers a nice snap-shot of the toolchain computational modelers (and statisticians) need to be effective researchers and collaborators.  If you find it helpful, think of our class as catching you up with what undergraduates in other quantitative fields know about scientific computing, with a slight emphasis on statistics and data analytics.

The "home base" language for this course is R, which can be obtained from [http://cran.r-project.org](CRAN). [http://rstudio.org](R Studio) is an excellent multi-platform graphical interface to R which you will likely prefer to the default Windows/OSX GUI(s).

Throughout the course we will encounter several other helpful tools, platforms and languages. The (incomplete) list of resources below, blending tutorials and best-practice guides, may be helpful.

- A [guide to Unix](https://secure.hosting.vt.edu/www.arc.vt.edu/userguide/unix/) from [VT ARC (Advanced Research Computing](http://arc.vt.edu)
- A guide to the [bash shell](http://www.tldp.org/LDP/Bash-Beginners-Guide/html/)
- Ubuntu's guide to the [Windows subsystem for Linux](https://ubuntu.com/wsl). 
- R style guides from [Google](https://google.github.io/styleguide/Rguide.xml) and [Hadley Wickham](http://r-pkgs.had.co.nz/style.html)
- Some [instructions](http://cinf401.artifice.cc/notes/rstudio-workflow.html) on setting up bitbucket/git and integrating with Rstudio.
- A PeerJ issue on [Practical Data Science for Stats](https://peerj.com/collections/50-practicaldatascistats/)
- [Code Academy](https://www.codecademy.com/learn/all) offers a nice suite of tutorials on many computing tools.  You may find the ones on the [Unix command line](https://www.codecademy.com/learn/learn-the-command-line) and [Git](https://www.codecademy.com/learn/learn-git) to be of interest.
- Free access to [Intel MKL on Ubuntu](https://software.intel.com/en-us/articles/installing-intel-free-libs-and-python-apt-repo) for optimized and threaded linear algebra, and instructions for [quick linking to R](https://software.intel.com/en-us/articles/quick-linking-intel-mkl-blas-lapack-to-r).
- Follow [these instructions](http://statistics.berkeley.edu/computing/blas) to use the Accelerate framework on the Mac.  (Ignore the OpenBLAS suggestions.  OpenBLAS is not thread safe.  E.g., it doesn't work with OpenMP.)
- You may need to [download gcc and gfortran](http://hpc.sourceforge.net/) to compile C code on your Mac. OSX's default compiler, Clang, is great but it doesn't support OpenMP.
- [Microsoft R Open](https://mran.microsoft.com/open/) is basically R compiled with Intel MKL for Windows and Linux platforms.

## Other

- The file [bash_aliases](bash_aliases) should be renamed to `.bash_aliases` and reside in your home directory.
