\documentclass[12pt,xcolor=svgnames]{beamer}
\usepackage{dsfont,natbib,setspace}
\mode<presentation>

% replaces beamer foot with simple page number
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}{
  \vspace{-1.5cm}
  \raisebox{10pt}{\makebox[\paperwidth]{\hfill\makebox[20pt]{\color{gray}\scriptsize\insertpagenumber}}}}

% colors
\newcommand{\bk}{\color{black}}
\newcommand{\rd}{\color{red}}
\newcommand{\fg}{\color{forestgreen}}
\newcommand{\bl}{\color{blue}}
\newcommand{\gr}{\color{gray}}
\newcommand{\theme}{\color{Maroon}}

% common math markups
\newcommand{\bs}[1]{\boldsymbol{#1}}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mr}[1]{\mathrm{#1}}
\newcommand{\bm}[1]{\mbox{\boldmath $#1$}}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\ds}[1]{\mathds{#1}}

% spacing and style shorthand
\newcommand{\sk}{\vspace{.4cm}}
\newcommand{\nochap}{\vspace{0.6cm}}
\newcommand{\nsk}{\vspace{-.4cm}}
\newcommand{\chap}[1]{{\theme \Large \bf #1} \sk}
\newcommand{\R}[1]{{\bl\tt #1}}
\newcommand{\til}{{\footnotesize$\bs{\stackrel{\sim}{}}$ }}

% specific stats markups for this doc
\newcommand{\E}{\ds{E}}
\newcommand{\Reals}{\ds{R}}
\newcommand{\var}{\text{var}}
\newcommand{\cov}{\text{cov}}
\newcommand{\mT}{\mc{T}}
\newcommand{\GP}{\mc{GP}}
\newcommand{\iidsim}{\stackrel{\mathrm{iid}}{\sim}}
\newcommand{\indsim}{\stackrel{\mathrm{ind}}{\sim}}
\newcommand{\mN}{\mc{N}}

\begin{document}

{ %\usebackgroundtemplate{\includegraphics[height=\paperheight]{phoenix}}
\thispagestyle{empty}
\setcounter{page}{0}

\title{\Large \theme 
\includegraphics[scale=0.1]{../Rlogo}\\
\bf {\sf Fundamentals}}
\author{\vskip .5cm {\bf Robert B.~Gramacy}\\{\color{Maroon}
    Virginia Tech}
  Department of Statistics\\
  \vskip .2cm \texttt{\rd bobby.gramacy.com}
  \vskip .25cm}
\date{}
\maketitle }

% doc spacing
\setstretch{1.1}

\begin{frame}[fragile]
\chap{Expressions}

{\sf R} code is composed of a series of {\rd expressions}.
\begin{itemize}
\item assignments
\item conditional statements
\item arithmetic
\end{itemize}

\sk Here are a few example expressions.
{\bl
\begin{verbatim}
> x <- 1
> if(1 > 2) "yes" else "no"
[1] "no"
> 127 %% 10
[1] 7
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
  \nochap 
{\bl Expressions} are composed of {\rd objects} and {\bl
    functions}, which may be separated by newlines or semicolons.

{\bl
\begin{verbatim}
> "an expression"; 7+13; exp(0+1i*pi)
[1] "an expression"
[1] 20
[1] -1+0i
\end{verbatim}
}

\sk
And, as we've seen, functions are themselves objects.
\begin{itemize}
\item All {\sf R} code manipulates objects.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\chap{Symbols}

Formally, variable names in {\sf R} are called {\rd symbols}.

\sk When you assign an object a variable name you are actually
assigning the object to a symbol in a certain {\rd environment} or
{\rd context}.

\sk
For example \R{x <- 1} assigns the symbol ``x'' to the object ``1''.

{\bl
\begin{verbatim}
> x <- 1
\end{verbatim}
}

The environment, like the sequence of expressions, is defined by
the programmer.
\begin{itemize}
\item more on this later
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.25cm}
\begin{itemize}
\item {\em Almost} every symbol you can imagine creating can be assigned to
any object, 
\item and can later be re-assigned to another, possibly different,
  object.  {\em In fact, a symbol is technically an object.}
\end{itemize}

\sk Some symbols are off-limits for assignment
\begin{itemize}
\item \R{NA}, \R{Inf}, \R{NaN}, \R{NULL}, \R{if}, \R{else}, \R{while},
  \R{function}, \R{for}, ...
\end{itemize}

Others, which come pre-assigned with values, are not.
{\bl
\begin{verbatim}
> pi
[1] 3.141593
> pi <- 1
> pi
[1] 1
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap

\begin{itemize}
\item When the {\sf R} interpreter evaluates an expression, it
  evaluates all symbols
\item If you compose an object from a set of symbols, {\sf R} will
  resolve the symbols at the time the object is constructed.
\end{itemize}

{\bl
\begin{verbatim}
> x <- 1; y <- 2; z <- 3
> v <- c(x, y, z)
> v
[1] 1 2 3
> x <- 10
> v
[1] 1 2 3
\end{verbatim}
}

\end{frame}

\begin{frame}[fragile]
\nochap

It is possible to delay the evaluation of an expression so that
symbols are not evaluated immediately.

{\bl
\begin{verbatim}
> x <- 1
> v <- quote(c(x, y, z))
> eval(v)
[1] 1 2 3
> x <- 5
> eval(v)
[1] 5 2 3
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap

Finally, you can create a {\em promise object} in {\sf R} to delay the
evaluation of a variable until it is (first) needed.  

{\bl
\begin{verbatim}
> x <- 1
> delayedAssign("v", c(x, y, z))
> x <- 5
> v
[1] 5 2 3
\end{verbatim}
}

\begin{itemize}
\item Promise objects are used within packages to make objects
  available to users without loading them into memory.
\end{itemize}

\end{frame}

\begin{frame}
\chap{Functions}

A {\rd function} is an object that takes some input objects
({\rd arguments}) and returns an output object.
\begin{itemize}
\item Unlike {\sf MATLAB}, functions cannot return multiple objects.
\item They can, however, return a {\rd list} of objects.
\end{itemize}

\sk {\em Technically}, all work in {\sf R} is done in functions.

\sk {\em Every} statement in {\sf R}---setting variables, doing
arithmetic, repeating code in a loop---can be written as a function.
\end{frame}

\begin{frame}[fragile]
\nochap

For example, suppose that you had defined a variable \R{animals}
    pointing to a character vector with four elements
{\bl
\begin{verbatim}
> animals <- c("cow", "chicken", "pig", "tuba")
\end{verbatim}
}
... and you wanted to change the fourth element
{\bl
\begin{verbatim}
> animals[4] <- "duck"
\end{verbatim}
}
That statement is {\em parsed} into a call to the \R{[<-} function.
{\bl
\begin{verbatim}
> `[<-`(animals, 4, "duck")
[1] "cow"     "chicken" "pig"     "duck"   
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap

In practice you would probably never write this statement as a
function call;
\begin{itemize}
\item the bracket notation is more intuitive and easier to read.
\end{itemize}
However it is helpful to know that every operation in {\sf R} is a
function.  

\sk Here is another example
{\bl
\begin{verbatim}
> if(1 < 2) "this" else "that"
[1] "this"
> `if`(1 < 2, "this", "that")
[1] "this"
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\chap{Immutable objects}

In assignment statements, most objects are {\bl immutable}.
\begin{itemize} 
\item {\sf R} will {\rd copy} the object, not just
  the reference.
\end{itemize}

\vspace{-0.25cm}
{\bl
\begin{verbatim}
> u <- list(1)
> v <- u
> u[[1]] <- "hat"
> u
[[1]]
[1] "hat"

> v
[[1]]
[1] 1
\end{verbatim}
}

\vspace{-0.5cm}
\end{frame}

\begin{frame}[fragile]
\nochap

This applies to vectors, lists, and most other primitive objects.

\sk Including functions.  E.g.,

{\bl
\begin{verbatim}
> f <- function(x, i) { x[i] <- 4 }
> w <- c(10, 11, 12, 13)
> f(w, 1)
> w
[1] 10 11 12 13
\end{verbatim}

\begin{itemize}
\item The vector \R{w} is copied when it is passed to the function.
\item The value \R{x} is modified inside the {\rd context} of the
  function.
\end{itemize}
}
\end{frame}

\begin{frame}[fragile]
\chap{Special Values}

\begin{itemize}
\item \R{NA}: reserved for missing values
{\bl
\begin{verbatim}
> v <- c(1,2,3)
> length(v) <- 4
> v
[1]  1  2  3 NA
\end{verbatim}
}
\item \R{Inf} and \R{-Inf}: for numbers too large for machine
  representation
{\bl
\begin{verbatim}
> 2^1024
[1] Inf
> -2^1024
[1] -Inf
\end{verbatim}
}
\end{itemize}

\vspace{-0.5cm}
\end{frame}

\begin{frame}[fragile]
\begin{itemize}
\item \R{NaN}: for numerical results that make no sense (not a number)
{\bl
\begin{verbatim}
> Inf - Inf
[1] NaN
> 0 / 0
[1] NaN
\end{verbatim}
}
\item \R{NULL} : a symbol representing no value.
\begin{itemize}
\item Usually \R{NULL} is used as argument to a function to indicate
that no value is being provided for that argument.  
\item Sometimes function return-values are \R{NULL}.
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\chap{Coercion}

When you call a function with an argument of the wrong type, {\sf R}
will try to {\rd coerce} the values to a different type so that the
function will work.  

\sk  With {\rd generic} functions, {\sf R} will look for a suitable
method.
\begin{itemize}
\item If no suitable match exists, {\sf R} will search for a {\rd
    coersion method} that converts the object to a type for which a
  suitable method does exist.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\chap

For example ...

{\bl
\begin{verbatim}
> x <- 1:5
> x
[1] 1 2 3 4 5
> class(x)
[1] "integer"
> x[4] <- "hat"
> x
[1] "1"   "2"   "3"   "hat" "5"  
> class(x)
[1] "character"
\end{verbatim}
}
\end{frame}

\begin{frame}
\nochap

\vspace{-0.25cm}
An overview of conversion rules:
\begin{itemize}
\item Logical values become numbers: \R{TRUE} becomes \R{1}
 and \R{FALSE} becomes \R{0}.
\item Values are converted to the simplest type required to represent
  all information.
\item The ordering is roughly
\end{itemize}
\[
\mbox{logical} < \mbox{integer} < \mbox{numeric} < \mbox{complex} <
\mbox{character} < \mbox{list}
\]
\vspace{-0.6cm}
\begin{itemize}
\item Objects of type \R{raw} are not converted to other types.
\item Object attributes are dropped when an object is coerced from one
  type to another.
\end{itemize}

You can inhibit coersion when passing arguments to functions by using
the \R{I()} function.
\end{frame}

\begin{frame}[fragile]
\chap{Operators}

We already discussed how operators (\R{+},  {\bl \verb!^!}, \R{*}, ...) are
really just functions.

\begin{itemize}
\item These are some of the the built-in {\em binary} operators.
\end{itemize}

\sk  You can define your own operators.  E.g.,

{\bl
\begin{verbatim}
> `%myop%` <- function(a, b) { 2*a + 2*b }
> 1 %myop% 1
[1] 4
> 1 %myop% 2
[1] 6
\end{verbatim}
}

There are {\em unary} operators too, e.g., \R{-}.
\vspace{-0.25cm}
\end{frame}

\begin{frame}
\nochap

Order of operations:

\begin{enumerate}
\item function calls and grouping expressions: \R{\{\}}, \R{()}
\item index and lookup operators: \R{[]}
\item arithmetic: \R{+/-*}, with the usual precedence
\item comparison: \R{<=>}
\item formulas: \R{$\sim$}
\item assignment: \R{<-}
\item help: \R{?}
\end{enumerate}
\end{frame}

\begin{frame}[fragile]
\chap{Special assignments}

Most assignments simply assign an object to a symbol.
{\bl
\begin{verbatim}
> y <- list(shoes="loafers", hat="ball cap", 
+      shirt="white")
> z <- function(a, b, c) { a^b/c }
> v <- 1:8
\end{verbatim}
}

\sk {\sf R} also allows assignments with a function on the left-hand
side of the assignment operator:
{\bl
\begin{verbatim}
> dim(v) <- c(2,4)
> v[2,2] <- 10
> formals(z) <- alist(a=1, b=2, c=3)
\end{verbatim}
}

\vspace{-0.5cm}
\end{frame}

\begin{frame}[fragile]
\chap{Grouping expressions}

We already saw that you can place multiple expressions on the same
line with separating semicolons.

\sk {\rd Parentheses} elevate the precedence of an operation to that
of a function call.
\begin{itemize}
\item In fact, a parenthetical grouping is equivalent to evaluating an
  identity function.
\end{itemize}

{\bl
\begin{verbatim}
> 2 * (5 + 1)
[1] 12
> f <- function(x) x
> 2 * f(5 + 1)
[1] 12
\end{verbatim}
}

\vspace{-0.5cm}
\end{frame}

\begin{frame}[fragile]
\nochap
{\rd Curly braces} are used to evaluate a series of expressions
(separated by newlines or semicolons) and return only the last
expression.

\sk A common use is to group operations in the body of a function.

{\bl
\begin{verbatim}
> f <- function() { x <- 1; y <- 2; x + y }
> f()
[1] 3
\end{verbatim}
}

But they can also be used in other contexts.
{\bl
\begin{verbatim}
> { x <- 1; y <- 2; x + y }
[1] 3
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap

Code in curly braces are evaluated in the {\rd current
  environment}.
\begin{itemize}
\item A {\rd new environment} is created by a function call but {\em
    not} by the use of curly braces.
\end{itemize}

{\bl
\begin{verbatim}
> f <- function() { u <- 1; v <- 2; u + v }
> f()
[1] 3
> u
Error: object 'u' not found
> { u <- 1; v <-2; u + v }
[1] 3
> u
[1] 1
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\chap{Control Structures}

Conditional statements take the form

\vspace{0.25cm}
\begin{quote}
\R{if(}condition\R{)} true\_expression \R{else} false\_expression
\end{quote}
or, alternatively

\vspace{0.25cm}
\begin{quote}
\R{if(}condition\R{)} true\_expression 
\end{quote}

Some examples:
{\bl
\begin{verbatim}
> if(FALSE) "not printed"
> if(FALSE) "not printed" else "printed"
[1] "printed"
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap

Conditional statements are not treated as vector operations.

\sk If the {\em condition} statement is a vector of more than one
\R{logical} value, then only the first term will be used.  E.g.,
{\bl
\begin{verbatim}
> x <- 10
> y <- c(8, 10, 12, 3, 17)
> if(x < y) x else y
[1]  8 10 12  3 17
Warning message:
In if (x < y) x else y :
  the condition has length > 1 and only 
  the first element will be used
\end{verbatim}
}
\end{frame}


\begin{frame}[fragile]
\nochap

\R{ifelse} is vectorized if you need it:
{\bl
\begin{verbatim}
> a <- rep("a", 5)
> b <- rep("b", 5)
> tf <- (1:5) %% 2 > 0
> ifelse(tf, a, b)
[1] "a" "b" "a" "b" "a"
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\chap

Often, it is convenient to return different values (or call different
functions) depending on a single input value.
{\bl
\begin{verbatim}
> switcheroo.if.then <- function(x) {
+   if(x == "a") "alligator"
+   else if(x == "b") "bear"
+   else if(x == "c") "camel"
+   else "moose"
+ }

> switcheroo.if.then("b")
[1] "bear"
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\chap

Its cleaner to use the \R{switch} function.

{\bl
\begin{verbatim}
> switcheroo.switch <- function(x) {
+   switch(x,
+          a="alligator",
+          b="bear",
+          c="camel",
+          "moose")
+ }

> switcheroo.switch("b")
[1] "bear"
\end{verbatim}
}
\end{frame}

\begin{frame}
\nochap
There are three different {\rd loop}ing constructs in R.

\sk The simplest is \R{repeat},

\vspace{0.25cm}
\begin{quote}
\R{repeat}  expression
\end{quote}
which repeats the same {\em expression} until a \R{break} keyword is
executed therein.

\sk
\begin{itemize}
\item If you don't include a \R{break} command, the {\sf R} code will be
in an infinite loop.
\item A \R{next} command allows you skip the rest of the loop
  iteration without evaluating the remaining {\em expressions} in the
  loop body.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\nochap

As an example, the following code prints out multiples of 5 up to 25.

{\bl
\begin{verbatim}
> i <- 5
> repeat {
+   if(i > 25) break
+   else { print(i); i <- i + 5 }
+ }
[1] 5
[1] 10
[1] 15
[1] 20
[1] 25
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap

Another useful construction is \R{while} loops, which repeat an
expression while a condition is true:

\vspace{0.25cm}
\begin{quote}
\R{while(}condition\R{)} expression
\end{quote}

\nsk
%% E.g.,
{\bl
\begin{verbatim}
> i <- 5
> while(i <= 25) { print(i); i <- i + 5 }
[1] 5
[1] 10
[1] 15
[1] 20
[1] 25
\end{verbatim}
}

\vspace{-0.25cm}
\begin{itemize}
\item You can use \R{break} and \R{next} inside
  \R{while} loops.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\nochap

Finally, {\sf R} provides \R{for} loops, which iterate through each
item in a vector (or a list):

\vspace{0.25cm}
\begin{quote}
\R{for(}var \R{in} list\R{)} expression
\end{quote}

{\bl 
\begin{verbatim}
> for(i in seq(5, 25, by=5)) print(i)
[1] 5
[1] 10
[1] 15
[1] 20
[1] 25
\end{verbatim}
}

\begin{itemize}
\item Again, \R{break} and \R{next} work with \R{for} loops.
\end{itemize}

\end{frame}

\begin{frame}[fragile]
\nochap

There are two important properties of (all) looping statements to keep
in mind.

\begin{enumerate}
\item Results are not printed unless you explicitly call \R{print}.
{\bl
\begin{verbatim}
> for(i in seq(5, 25, by=5)) i
\end{verbatim}
}
\item The variable {\em var} that is set in a \R{for} loop is changed
  in the calling environment.
{\bl
\begin{verbatim}
> i <- 1
> for(i in seq(5, 25, by=5)) i
> i
[1] 25
\end{verbatim}
}
\end{enumerate}

\end{frame}

\begin{frame}
\chap{Accessing Data Structures}

We've seen three ways of accessing information from vectors, lists,
arrays, matrices, and data frames.
\begin{center}
\begin{tabular}{c c c}
\R{x[i]} & \R{x[[i]]} & \R{x\$n}
\end{tabular}
\end{center}
About brackets:
\begin{itemize}
\item \R{[[]]} always returns a single element, whereas\R{[]} \!may
  return multiple elements.
\item When an element is referred to by name, as opposed to by
  element, \R{[]} requires exact matches whereas \R{[[]]} allows
  partial ones.
\item With lists, \R{[]} gives a list whereas \R{[[]]} gives a vector.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\nochap
In the last lecture we saw lots of indexing examples; here are some
more advanced features/details.

\sk You can use {\rd negative indices} to return a vector consisting of
all elements {\em except} those indexed.

{\bl
\begin{verbatim}
> v <- 100:119
> v
 [1] 100 101 102 103 104 105 106 107 108 109
[11] 110 111 112 113 114 115 116 117 118 119
> v[-15:-1]
[1] 115 116 117 118 119
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap

The same applies to lists.
{\bl
\begin{verbatim}
> l <- list(a=1, b=2, c=3, d=4, e=5,
+           f=6, g=7, h=8, i=9, j=10)
> l[-7:-1]
$h
[1] 8

$i
[1] 9

$j
[1] 10
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap

When selecting a subset of a larger dimensional object, {\sf R} will
automatically coerce the results to an object appropriate for the new
dimensions.

{\bl
\begin{verbatim}
> class(a)
[1] "array"
> class(a[1,,])
[1] "matrix"
> class(a[1,1,])
[1] "integer"
> class(a[1:2,1:2,1:2])
[1] "array"
> class(a[1,1,1, drop=FALSE])
[1] "array"
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap

You can replace elements of a vector, matrix, or array using brackets.

{\bl
\begin{verbatim}
> a[1,,]
     [,1] [,2] [,3] [,4]
[1,]  101  107  113  119
[2,]  103  109  115  121
[3,]  105  111  117  123
> a[1,1:2,1:2] <- 1:4
> a[1,,]
     [,1] [,2] [,3] [,4]
[1,]    1    3  113  119
[2,]    2    4  115  121
[3,]  105  111  117  123
\end{verbatim}
}

\end{frame}

\begin{frame}[fragile]
\nochap

Lists can be indexed by name using \R{\$}, but you can also use
\R{[]}.

{\bl
\begin{verbatim}
> l[c("a", "b", "c")]
$a
[1] 1

$b
[1] 2

$c
[1] 3
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\chap

You can index by name with \R{[[]]} for single elements.
{\bl
\begin{verbatim}
> dairy <- list(milk="1 gallon", butter="1 pound",
+               eggs=12)
> dairy$milk
[1] "1 gallon"
> dairy[["milk"]]
[1] "1 gallon"
\end{verbatim}
}

You can also use partial names with \R{exact=FALSE}.
{\bl
\begin{verbatim}
> diary[["mil"]]
NULL
> dairy[["mil", exact=FALSE]]
[1] "1 gallon"
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap

Sometimes an object is a list of lists, in which case you can use
\R{[[]]} with a vector index of names or numbers.

{\bl
\begin{verbatim}
> fruit <- list(apples=6, oranges=3, bananas=10)
> shopping.list <- list(dairy=dairy, fruit=fruit)
> shopping.list[[c("dairy", "milk")]]
[1] "1 gallon"
> shopping.list[[c(1,2)]]
[1] "1 pound"
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\chap{Factors}

When analyzing data, it is quite common to encounter categorical
values.  

\sk For example, suppose you have a set of observations about people
that includes eye color.

\sk You could represent eye colors as a character array.

{\bl
\begin{verbatim}
> ecc <- c("brown", "blue", "blue", "green",
+          "brown", "brown", "brown")
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap

This is a perfectly valid way to represent information, but it can
become inefficient if you are working with large names or a large
number of observations.

\begin{itemize}
\item {\sf R} provides a better way to represent categorical values, by
using factors.
\end{itemize}

\sk A {\rd factor} is an ordered (potentially) collection of items.
{\bl
\begin{verbatim}
> ecf <- factor(ecc)
> ecf 
[1] brown blue  blue  green brown brown brown
Levels: blue brown green
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap
In this example, {\em order} probably does not matter.  But you can
use factors to represent ordered categories.  E.g.,
{\bl
\begin{verbatim}
> ecf <- factor(ecc, levels=c("green", "brown",
+                      "blue"), ordered=TRUE)
> ecf
[1] brown blue  blue  green brown brown brown
Levels: green < brown < blue
\end{verbatim}
}
\begin{itemize}
\item By default, the order is alphanumeric.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\nochap

\begin{itemize}
\item Factors are implemented internally using integers.
\item The levels attribute maps integer to factor level.
\item Integers take up a small, fixed amount of storage space.
\end{itemize}
{\bl
\begin{verbatim}
> unclass(ecf)
[1] 2 3 3 1 2 2 2
attr(,"levels")
[1] "green" "brown" "blue" 
> as.integer(ecf)
[1] 2 3 3 1 2 2 2
\end{verbatim}
}
\end{frame}

\begin{frame}
\chap{Data Frames}

{\em It is worth repeating that} {\rd data frames} are a useful way to
represent data in {\sf R}.
\begin{itemize}
\item Many experiments consist of individual observations, each of
  which involves several different measurements.
\item Often, those measurements have different dimensions and
  sometimes there are qualitative, not quantitative.
\end{itemize}

A data frame provides a structure for representing such data in a
tabular (row/column) format.
\begin{itemize}
\item Each column may be a different type, but each row must have the
  same length.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\nochap

The row and column names of a data frame (and a matrix) can be
accessed as follows.
{\bl
\begin{verbatim}
> colnames(trees)
[1] "Girth"  "Height" "Volume"
> names(trees)
[1] "Girth"  "Height" "Volume"
> rownames(trees)[1:6]
[1] "1" "2" "3" "4" "5" "6"
\end{verbatim}
}

\begin{itemize}
\item Assignment can be used to make changes.
\end{itemize}

\end{frame}

\begin{frame}[fragile]
\nochap

Consider data on Cherry trees, which is stored in a data frame,
measuring trees girth, height, and volume.

{\bl
\begin{verbatim}
> trees
   Girth Height Volume
1    8.3     70   10.3
2    8.6     65   10.3
3    8.8     63   10.2
4   10.5     72   16.4
...
30  18.0     80   51.0
31  20.6     87   77.0
\end{verbatim}
}

\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.25cm}
A {\rd pairs} plot offers a nice visual inspection of the
relationships between variables in a data frame.
{\bl
\begin{verbatim}
> pairs(trees)
\end{verbatim}
}

\begin{center}
\includegraphics[scale=0.42,trim=10 5 10 30]{trees}
\end{center}
\end{frame}

\begin{frame}[fragile]
\chap{Formulas}

Modeling such data requires the use of {\rd formulas} to express a
relationship between variables.

\sk For example, we may wish to fit the linear model
\[
V_i = \beta_0 + H_i\beta_1 + G_i \beta_2 + G_i^2 \beta_3 + \varepsilon_i.
\]

\sk The formula describing this is
{\bl
\begin{verbatim}
> formula(Volume~Height + Girth + I(Girth^2))
Volume ~ Height + Girth + I(Girth^2)
\end{verbatim}
}

\begin{itemize}
\item The \R{I()} ``properly'' express the squared term.
\end{itemize}

\vspace{-0.5cm}
\end{frame}

\begin{frame}[fragile]
\nochap

Formulas are required for most fitting commands, e.g., \R{lm}.
{\bl
\begin{verbatim}
> lm(Volume~Height+Girth+I(Girth^2),
+    data=trees)

Call:
lm(formula = Volume~Height+Girth+I(Girth^2), 
   data=trees)

Coefficients:
(Intercept)       Height        Girth  
    -9.9204       0.3764      -2.8851  
 I(Girth^2)  
     0.2686 
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap

They offer handy abbreviations for compactly expressing longer
formulas.

{\bl 
\begin{verbatim}
> coef(lm(Volume ~ . + I(Girth^2), data=trees))
(Intercept)       Girth      Height 
 -9.9204060  -2.8850787   0.3763873 
 I(Girth^2) 
  0.2686224 
\end{verbatim}
}

\begin{itemize}
\item (More shorthand examples in the code.)
\item also see \R{?~formula}
\end{itemize}
\end{frame}

\begin{frame}
\chap{Time Series}

Many important problems look at how a variable changes over time, and
{\sf R} includes a class to represent this data: {\rd time series}
objects.

\begin{itemize}
\item Regression functions for time series (like \R{ar} or \R{arima})
  use time series objects.
\item Many plotting functions have special methods for time series.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.5cm} The \R{turkey} data, below, contains quarterly total
sales (in thousands) of one-day-old turkey chicks from hatcheries in
Eire over a period of years starting in 1974.

{\bl
\begin{verbatim}
> turkey
 [1] 131.7 322.6 285.6 105.7  80.4 285.1
 [7] 347.8  68.9 293.3 375.9 415.9  65.8
[13] 177.0 483.3 463.2 136.0 192.2 442.8
[19] 509.6 201.2 196.0 478.6 688.6 259.8
[25] 352.5 508.1 701.5 325.6 305.9 422.2
[31] 771.0 329.3 384.0 472.0 852.0
\end{verbatim}
}

Converting to a time series object allows extra attributes in the data
representation.
\end{frame}

\begin{frame}[fragile]
\nochap

{\bl
\begin{verbatim}
> turkey <- ts(turkey, start=1974, freq=4)
> turkey
      Qtr1  Qtr2  Qtr3  Qtr4
1979 131.7 322.6 285.6 105.7
1980  80.4 285.1 347.8  68.9
1981 293.3 375.9 415.9  65.8
1982 177.0 483.3 463.2 136.0
1983 192.2 442.8 509.6 201.2
1984 196.0 478.6 688.6 259.8
1985 352.5 508.1 701.5 325.6
1986 305.9 422.2 771.0 329.3
1987 384.0 472.0 852.0      
\end{verbatim}
}

\end{frame}

\begin{frame}[fragile]
\chap

Time series \R{plot()} methods show the extra information, and
automatically connect the ``dots''.  {\bl
\begin{verbatim}
> plot(turkey)
\end{verbatim}
}

\begin{center}
\includegraphics[scale=0.5,trim=10 30 10 50]{turkey}
\end{center}
\end{frame}

\begin{frame}[fragile]
\chap{Environments}

Like everything in {\sf R}, environments (which define a scope for
symbols) are objects.
\begin{itemize}
\item Internally, {\sf R} stores symbol mappings in hash tables.
\end{itemize}

You can list the objects/symbols in the current
environment.
{\bl
\begin{verbatim}
> objects()
[1] "turkey" "v"      "x"      "y"     
[5] "z"     
\end{verbatim}
}

\vspace{-0.1cm}
You can remove/delete objects from the current environment.
{\bl
\begin{verbatim}
> rm(x)
> objects()
[1] "turkey" "v"      "y"      "z"
\end{verbatim}
}
\vspace{-0.5cm}
\end{frame}

\begin{frame}
\nochap

When a user starts a new session in {\sf R}, the system creates a new
{\rd global environment} for objects.

\begin{itemize}
\item The global environment is not actually the {\em root} of the
  tree of environments.
\item Rather, it is the last environment in the chain/search path of environments
  initialized when {\sf R} started up.
\item Every environment has a parent environment except one, the {\em
    empty environment}.
\item All environments chain back to the empty environment.
\end{itemize}

\begin{center}
\R{(see accompanying~.R file)}
\end{center}

\end{frame}

\begin{frame}[fragile]
\chap{Environments and Functions}

When a function is called in {\sf R}, 
\begin{itemize}
\item a new environment is created
within the body of the function
\item  and the arguments of the function are
assigned symbols in the local environment.
\end{itemize}
E.g.,

{\bl
\begin{verbatim}
> env.demo <- function(a, b, c, d) {
+   print(objects())
+ }
> env.demo(1, "truck", 1:5, pi)
[1] "a" "b" "c" "d"
\end{verbatim}
}
\vspace{-0.5cm}
\end{frame}

\begin{frame}
\nochap

The parent environment of a function is the environment from which the
function was {\em created}.

\begin{itemize}
\item I.e., not necessarily the one from which it was called.
\item If the function was created in another environment, such as a
  package, then the parent environment will not be the same as the
  calling environment.
\end{itemize}

However, it is possible to access the environment from which a
function was called.
\end{frame}

\begin{frame}
\nochap

{\sf R} maintains a {\rd stack} of calling environments.
\begin{itemize}
\item Each time a new function is called, a new environment is {\em
    pushed} onto the stack.
\item When the function is finished evaluating, the environment is
  {\em popped} off.
\end{itemize}

\sk
\sk There are many functions for inspecting/manipulating the
stack.
\begin{itemize}
\item The most useful is \R{parent.frame()} which allows you to
  access the calling environment.
\item The \R{eval()} function can evaluate an expression in any environment.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.25cm}
The best example of when this would be useful comes from how formulas
are used within modeling functions like \R{lm} and \R{glm} when the
programmer omits the \R{data} argument.

{\bl
\begin{verbatim}
> x <- 1:10
> y <- 5 + x + rnorm(10)
> fit <- lm(y~x)
\end{verbatim}
}

\begin{itemize}
\item The formula {\bl \verb!y~x!} {\em represents} a relationship between
variables.  
\item It does not copy/paste those variables to \R{lm}.
\item Rather, \R{lm} reads the formula and pulls \R{x} and \R{y} from
  the calling environment via \R{parent.frame()}
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\nochap

Sometimes it is convenient to treat a data frame or a list as an
environment.
\begin{itemize}
\item This lets you refer to each item in the data frame or list by
  name as if you were using symbols.
\end{itemize}

{\bl
\begin{verbatim}
> df <- data.frame(a=1, b=2, c=3)
> d <- with(df, a+b+c)
> print(d)
[1] 6
> df2 <- within(df, d <- a+b+c)
> df2
  a b c d
1 1 2 3 6
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\chap{Attach}

R provides a shorthand for adding objects to the current environment
\begin{itemize}
\item e.g., the columns of a data frame.
\end{itemize}

{\bl
\begin{verbatim}
> df
  a b c
1 1 2 3
> attach(df)
> print(c(a,b,c))
[1] 1 2 3
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap

\begin{itemize}
\item \R{attach()} works with data frames, lists, and data saved with
  \R{save()}.
\item You can undo an attachment.
{\bl
\begin{verbatim}
> detach(df)
\end{verbatim}
}
\item \R{attach()} will warn if an attaching column name clashes with a symbol
  in the current environment (and it will not attach that column).

\sk
In particular, be careful with \R{attach()} when working with multiple
data sets with variables of the same or similar names.
\end{itemize}
\end{frame}


% \begin{frame}
% \chap{``Homework''}

% \begin{itemize}
% \item Use \R{eval()} to evaluate a function (of your design) in the
%   calling environment.
% \item E.g., the function could swap two elements of a (long) vector
%   without supplying the vector or swapping indices as arguments.
%   Instead, get them from the \R{parent.frame()}.
% \item In what situation would doing this be advantageous?
% \item \R{do.call()} offers similar functionality.  How is it different
%   and when might it be useful?
% \end{itemize}

% \end{frame}

\end{document}
