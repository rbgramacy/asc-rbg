\documentclass[12pt,xcolor=svgnames]{beamer}
\usepackage{dsfont,natbib,setspace}
\mode<presentation>

% replaces beamer foot with simple page number
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}{
  \vspace{-1.5cm}
  \raisebox{10pt}{\makebox[\paperwidth]{\hfill\makebox[20pt]{\color{gray}\scriptsize\insertpagenumber}}}}

% colors
\newcommand{\bk}{\color{black}}
\newcommand{\rd}{\color{red}}
\newcommand{\fg}{\color{forestgreen}}
\newcommand{\bl}{\color{blue}}
\newcommand{\gr}{\color{gray}}
\newcommand{\theme}{\color{Maroon}}

% common math markups
\newcommand{\bs}[1]{\boldsymbol{#1}}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mr}[1]{\mathrm{#1}}
\newcommand{\bm}[1]{\mbox{\boldmath $#1$}}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\ds}[1]{\mathds{#1}}

% spacing and style shorthand
\newcommand{\sk}{\vspace{.4cm}}
\newcommand{\nochap}{\vspace{0.6cm}}
\newcommand{\nsk}{\vspace{-.4cm}}
\newcommand{\chap}[1]{{\theme \Large \bf #1} \sk}
\newcommand{\R}[1]{{\bl\tt #1}}
\newcommand{\til}{{\footnotesize$\bs{\stackrel{\sim}{}}$ }}

% specific stats markups for this doc
\newcommand{\E}{\ds{E}}
\newcommand{\Reals}{\ds{R}}
\newcommand{\var}{\text{var}}
\newcommand{\cov}{\text{cov}}
\newcommand{\mT}{\mc{T}}
\newcommand{\GP}{\mc{GP}}
\newcommand{\iidsim}{\stackrel{\mathrm{iid}}{\sim}}
\newcommand{\indsim}{\stackrel{\mathrm{ind}}{\sim}}
\newcommand{\mN}{\mc{N}}

\begin{document}

{ %\usebackgroundtemplate{\includegraphics[height=\paperheight]{phoenix}}
\thispagestyle{empty}
\setcounter{page}{0}

\title{\Large \theme 
\includegraphics[scale=0.1]{../Rlogo}\\
\bf {\sf Monte Carlo \& Parallelization}}
\author{\vskip .5cm {\bf Robert B.~Gramacy}\\{\color{Maroon}
    Virginia Tech}
  Department of Statistics\\
  \vskip .2cm \texttt{\rd bobby.gramacy.com}
  \vskip .25cm}
\date{}
\maketitle }

% doc spacing
\setstretch{1.1}

\begin{frame}
\chap{Monte Carlo Integration}

The term {\rd Monte Carlo (MC)} is used to refer to techniques involving
computer simulation,
\begin{itemize}
\item alluding to the games of chance played in casinos.
\item So MC integration is numerical integration using simulation.
\end{itemize}

\sk 
Although MC is used calculate/estimate many quantities, most can be
framed as integrals.
\begin{itemize}
\item So these are one in the same topic in the end.
\end{itemize}
\end{frame}

\begin{frame}
\nochap

MC essentially replaces analytics with approximate computation,
\begin{itemize}
\item where accuracy is tightly coupled to time and other
  computing resources.
\end{itemize}

\sk Therefore, a discussion of MC techniques is not complete without
discussing
\begin{itemize}
\item computational techniques that allow you to make the most of the
  horsepower available.
\end{itemize}

\sk Choosing the best MC method matters too, so as not to waste
resources, but that is a topic for another time.

\end{frame}

\begin{frame}
\chap

Most generally, suppose we wish to calculate
\[
I = \int_a^b f(x) \, dx
\]
and that the anti-derivative of $f(x)$ is not known.


\sk Let 
\begin{itemize}
\item $c$ and $d$ be such that $f(x) \in [c,d]$ for all $x \in [a,b]$
\item $A$ be the set bounded above by the curve and by the box $[a,b]
  \times [c,d]$.
\end{itemize}

Then, $I=|A| + c(b-a)$.
\begin{itemize}
\item Thus, if we can estimate $|A|$ we can estimate $I$,
\end{itemize}
\end{frame}

\begin{frame}
\nochap

\begin{center}
\includegraphics[scale=0.4,trim=50 0 0 30]{hitandmiss}
\end{center}
\end{frame}

\begin{frame}
\nochap

To estimate $|A|$, imagine throwing darts at the box $[a,b] \times
[c,d]$.

\sk On average
\begin{itemize}
\item the proportion that land under the curve will be given by the
  area of $A$ over the area of the box.  That is
\[
\frac{|A|}{(b-a)(d-c)}
\]
\item giving us a means of estimating $|A|$.
\end{itemize}

\end{frame}

\begin{frame}
\nochap

Take $X \sim \mathrm{Unif}(a,b)$ and $Y \sim \mathrm{Unif}(c,d)$,
then 
\begin{itemize}
\item $(X,Y)$ is uniformly distributed over the box $[a,b] \times
  [c,d]$, and
\item $P((X,Y) \in A) = P(Y \leq f(X)) = \frac{|A|}{(b-a)(d-c)}$.
\end{itemize}

\sk Let $Z = 1$ if $Y \leq f(X)$ and 0 otherwise, then 
\[
\mathbb{E}\{Z\} = P((X,Y) \in A)
\]
and we have 
\[
I = \mathbb{E}\{Z\} (b-a)(d-c) + c(b-a).
\]
\end{frame}

\begin{frame}[fragile]
\nochap

So by simulating $X$ and $Y$ we can simulate $Z$, and estimate
$\mathbb{E}\{Z\}$, and thus $I$.

\sk Here is a vectorized implementation of this so-called {\rd
  hit-and-miss} method.

{\bl \footnotesize
\begin{verbatim}
> hitmiss <- function(f, a, b, c, d, n, plotit=FALSE)
+   {
+     X <- runif(n, a, b)
+     Y <- runif(n, c, d)
+     Z <- Y <= f(X)
+     if(plotit) {
+       I <- (b-a)*c + (cumsum(Z)/(1:n))*(b-a)*(d-c)
+       plot(2:n, I[-1], type="l")
+       return(I[n])
+     } else return((b-a)*c + (sum(Z)/n)*(b-a)*(d-c))
+   }
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.25cm}
{\bl \footnotesize
\begin{verbatim}
> f <- function(x) x^3 - 7*x^2 + 1
> hitmiss(f, 0, 1, -6, 2, 1000000, plotit=TRUE)
[1] -1.08628
> hitmiss(f, 0, 1, -6, 2, 1000000) 
[1] -1.075784
\end{verbatim}
}

\begin{center}
\includegraphics[scale=0.45,trim=10 10 0 40]{hitandmiss_progress}
\end{center}
\end{frame}

\begin{frame}
\nochap 
Convergence looks pretty good, 
\begin{itemize}
\item although perhaps we don't have enough
MC samples (\R{n}) to get an accurate estimate to the second decimal
place.
\end{itemize}

\sk Other numerical methods are more accurate in some cases,
particularly in lower dimension, $d$.
\begin{center}
\begin{tabular}{lr}
Method & Error\\
\hline
Trapezoid & $O(n^{-2/d})$ \\
Simpson's Rule & $O(n^{-4/d})$ \\
Hit-and-miss MC & $O(n^{-1/2})$
\end{tabular}
\end{center}

\begin{itemize}
\item Our MC method is best for $d>8$.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.25cm}
Actually, there is a simpler method that requires a bit more mathematics to
justify.

\sk For
\[
\hat{I}_n = \frac{1}{n} \sum_{i=1}^n f(u_i)(b-a), \;\;\;\;\;
u_i\stackrel{\mathrm{iid}}{\sim} \mathrm{Unif}(a,b),
\]
then $\hat{I}_n \rightarrow I$ as $n\rightarrow \infty$.

\sk This suggests
{\bl
\begin{verbatim}
> mci <- function(g, a, b, n)
+   return(mean(f(runif(n, a, b)))*(b-a))
\end{verbatim}
}
\vspace{-0.25cm}
\begin{itemize}
\item It has the same efficiency as the hit-and-miss method,
\item but does not require bounding $f$.
\end{itemize}

\end{frame}

\begin{frame}
\nochap

\vspace{-0.1cm}
In statistics, the integrals that typically need evaluating/approximating
are expectations.
\[
\mathbb{E}\{h(X)\} = \int h(x) f(x) \, dx,
\]
where $f(x)$ is a density function.
\begin{itemize}
\item If $h(x) = x$, this is the mean $\mu = \mathbb{E}(X)$.
\item If $h(x) = (x-\mu)^2$ it is the variance if $X$.
\item If $h(x) = \mathbb{I}_{\{x \in A\}}$, then
  $\mathbb{E}\{h(X)\} = P(A)$.
\end{itemize}

\sk Our MC integration method, \R{mci()}, is easily generalized.
\[
\bar{h}_n = \frac{1}{n} \sum_{i=1}^n h(x_i), \;\;\;\;\; \mbox{where }
x_i \stackrel{\mathrm{iid}}{\sim} f.
\]

\end{frame}

\begin{frame}
\nochap 

 As before, $\bar{h}_n \rightarrow \mathbb{E}\{h(X)\}$ as
$n\rightarrow\infty$.

The rate of convergence is easy to derive:
\[
\mathrm{Var}(\bar{h}_n) = \frac{\mathrm{Var}[h(X)]}{n}.
\]
\begin{itemize}
\item For any particular approximation, we can use the draws of $x_i$
  to estimate the variance, and thereby estimate the accuracy.
\[
\widehat{\mathrm{Var}[h(X)]} = \frac{1}{n-1} \sum_{i=1}^n (h(x_i) - \bar{h}_n)^2.
\]
\end{itemize}
\end{frame}

\begin{frame}
\nochap

\vspace{-0.25cm}
An example: where the simulation is trivial but the analytic version
is hard(er).

\sk Suppose Annie ($A$) and Sam ($S$) plan a rendezvous at the Empire
State Building on Valentine's day.
\begin{itemize}
\item Sam arrives at a random time between 10 and 11:30pm,
\item Annie arrives at a random time between 10:30 and 12am,
\end{itemize}
independently.

\sk
What is?
\begin{itemize}
\item The probability that Annie arrives before Sam: $P(A < S)$;
\item The expected difference in the two arrival times:
  $\mathbb{E}\{A-S\}$, or $\mathbb{E}\{|A-S|\}$, depending on your
  interpretation.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.5cm}
{\bl
\begin{verbatim}
> n <- 1000
> S <- runif(n, 10, 11.5)
> A <- runif(n, 10.5, 12)
> prob <- mean(A < S)
> prob
[1] 0.24
\end{verbatim}
}

Since the MC estimate is a sample proportion, 
\begin{itemize}
\item we can use the formula for the variance of a proportion 
\item to calculate the standard error of the estimate.
\end{itemize}

\vspace{-0.25cm}
{\bl
\begin{verbatim}
> se <- sqrt(prob*(1-prob)/n)
> prob + c(-1,1)*1.96*se
[1] 0.2135291 0.2664709
\end{verbatim}
}

\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.4cm}
{\bl \small
\begin{verbatim}
> plot(S[1:1000], A[1:1000])
> polygon(c(10.5, 11.5, 11.5, 10.5),
+ c(10.5, 10.5, 11.5, 10.5), density=10, angle=135)
\end{verbatim}
}

\begin{center}
\includegraphics[scale=0.45,trim=10 10 0 40]{samalice}
\end{center}

\end{frame}

\begin{frame}[fragile]
\nochap

Calculating the expected difference in arrival times is similarly
straightforward.  We have
\begin{align*}
\mathbb{E}\{A-S\} &= \int (a-s) f_A(a) f_S(s) \, da\,ds \\
\mbox{or } \ \mathbb{E}\{|A-S|\} &= \int |a-s| f_A(a) f_S(s) \, da\,ds.
\end{align*}

\vspace{-0.5cm}
{\bl
\begin{verbatim}
> diff <- A - S
> d <- c(mean(diff), mean(abs(diff)))
> se <- c(sd(diff), sd(abs(diff)))/sqrt(n)
> d[1] + c(-1,1)*1.96*se[1]
[1] 0.4532109 0.5288760
> d[2] + c(-1,1)*1.96*se[2]
[1] 0.6057472 0.6627269
\end{verbatim}
}

\end{frame}

\begin{frame}
\chap{Bootstrap Resampling}

In {\em statistics}, we don't have the luxury of {\em generating} our
``data'', but we want to make similar calculations.

\begin{itemize}
\item What is the sampling distribution of our estimator?
\item Is it biased?
\item What is the probability that one parameter is bigger than other?
\item Are our estimates sensitive to a few outlying values?
\end{itemize}

Sometimes these questions can be answered analytically, but not in the
most interesting/compelling modeling scenarios.

\sk
{\rd Bootstrapping} is an attractive option in such cases.
\end{frame}

\begin{frame}
\nochap

\vspace{-0.25cm}
The {\bl bootstrap} is a general tool for assessing statistical
accuracy.

\sk At first glance it seems like voodoo, or like you get something
for nothing.  Hence the name ...

\sk The phrase ``pull oneself over a fence by one's bootstraps'' goes
back to the early 19th century United States. 
\begin{itemize}
\item It means that one has initiated a self-sustaining process
  without external help---something absurdly impossible.
\item Its most common use is in the business world.
\end{itemize}

\sk In statistics, its all about (re-) sampling with replacement.
\end{frame}


\begin{frame}
\nochap 

\vspace{-0.25cm}
Suppose you have a model/fitting method for particular data $Z = (z_1, z_2,
 \dots, z_n)$, where $z_i = (x_i, y_i)$ and you are
 interested in some statistic $S(Z)$ about an {\em aspect} of the fit
 $\hat{f}|Z$.
\begin{itemize}
\item $S$ may be the variance of some parameter $\theta$ used by $f$,
\item or an aspect of the predictive equations $\hat{f}(x)$, etc.
\end{itemize}

\sk The {\bl bootstrap} involves randomly drawing datasets, with replacement,
from the original data ($Z$).
\begin{itemize}
\item Each bootstrapped data set is typically of size $n$.
\item Repeat $B$ times ($B = 100$, say), for $B$ {\bl
  bootstrap datasets}
\item obtaining $B$ samples of $S$ are from its
  (nonparametric) distribution.
\end{itemize}
\end{frame}

\begin{frame}
\nochap

\begin{center}
\includegraphics[scale=0.4]{htf_7_10}
\end{center}

\end{frame}

\begin{frame}
\nochap

The mechanics of obtaining each bootstrap sample are trivial. 

\sk Just sample with replacement.

\sk {\sf R} pseudocode:

\sk
\R{indices <- sample(1:n, n, replace=TRUE)}\\
\R{Zbstar <- Z[indices,]}\\
\R{S[b] <- S(Zbstar)}\\

\sk Based on the bootstrap sample we can estimate any aspect of the
distribution of $S(Z)$.  

\end{frame}

\begin{frame}
\nochap 

\vspace{-0.5cm}
For example, its variance is

\[
\widehat{\var}(S(Z)) = \frac{1}{B-1} \sum_{b=1}^B (S(Z^{*b}) - \bar{S}^*)^2,
\]
where $\bar{S}^* = \sum_b S(Z^{*b}) /B$.  

\sk
\begin{itemize}
\item $\widehat{\var}(S(Z))$ is a Monte Carlo
  estimate of the variance of $S(Z)$
\item obtained with reference to the empirical distribution
  function $\hat{F}_n$ of the data $(z_1,\dots, z_n)$.
\end{itemize}

\sk CIs can be approximated with the
  $(1-\frac{\alpha}{2})(B+1)$ and $\frac{\alpha}{2}(B+1)$ order
  statistics of $S(Z^{*1}), \dots, S(Z^{*B})$.

%%% Verb: in fact, people sometimes write \hat{Var}_{\hat{F}_n} to
%%% indicate the bootstrap estimator for the variance.
\end{frame}

\begin{frame}[fragile]
\nochap

As a simple example, consider estimating uncertainty in the mean of a
population with unknown distribution.

{\bl \footnotesize
\begin{verbatim}
> alpha <- 4; ibeta <- 1/4
> n <- 30
> x <- rgamma(n, alpha, ibeta)
> hist(x, freq=FALSE, xlim=range(xt),
+      ylim=c(0,1.5*max(f)), main="")
> xt <- seq(0,qgamma(0.999, alpha, ibeta), length=1000)
> f <- dgamma(xt, alpha, ibeta)
> lines(xt, f, col=2, lwd=2, lty=2)
> legend("topright", "true pdf", lty=2, col=2,
+        lwd=2, cex=1.5, bty="n")
\end{verbatim}
}
\end{frame}

\begin{frame}
\nochap

\begin{center}
\includegraphics[scale=0.6,trim=0 0 0 40]{gammahist}
\end{center}
\end{frame}

\begin{frame}[fragile]
\nochap

\hfill \begin{minipage}{7cm}
Via normal approximation.

\nsk
{\bl \footnotesize
\begin{verbatim}
> xbar <- mean(x); s2.norm <- var(x)
> za <- qnorm(0.975, 0, sqrt(s2.norm))
> q.norm <- xbar + c(-1,1)*za/sqrt(n)
> points(xbar, 0, col=3, cex=1.5)
> text(q.norm, c(0,0), c("[", "]"), 
+      col=3, cex=1.5)
\end{verbatim}
}
\end{minipage}

\vspace{-3cm}
\includegraphics[scale=0.57,trim=10 20 0 20]{gammahist_norm}\hfill
\end{frame}

\begin{frame}[fragile]
\nochap

A better estimate via bootstrap.

{\bl
\begin{verbatim}
> B <- 199
> X.star <- matrix(NA, nrow=B, ncol=n)
> for(b in 1:B) {
+   X.star[b,] <- sample(x, n, replace=TRUE)
+ }
> 
> ## calculate the bootstrap mean-interval
> xbar.boot <- apply(X.star, 1, mean)
> q.boot <- quantile(xbar.boot, c(0.025, 0.975))
> c(mean(xbar.boot), q.boot)
             2.5%    97.5% 
16.09991 13.27447 19.20387 
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap

\hfill \begin{minipage}{6.5cm}
\nsk
{\bl \footnotesize
\begin{verbatim}
> text(q.boot, c(0,0), c("[", "]"), 
+     col=4, cex=1.5)
> lines(density(xbar.boot), col=4, 
+     lty=2, lwd=2)
> legend("right", "boot mean distn", 
+     cex=1.5, col=4, lty=3, lwd=2, 
+     bty="n")
\end{verbatim}
}
\end{minipage}

\vspace{-3cm}
\includegraphics[scale=0.57,trim=15 45 0 20]{gammahist_boot}\hfill
\end{frame}

\begin{frame}[fragile]
\chap{Bootstrapped regression}

Consider a typical LM $Y \sim \mN_n(X\beta, \sigma^2I_n)$,
\begin{itemize}
\item for which the sampling distribution
$\hat{\beta}$ is readily available.
\end{itemize}

{\bl \footnotesize
\begin{verbatim}
> n <- 200
> X <- 1/cbind(rt(n, df=1), rt(n, df=1), rt(n,df=1))
> beta <- c(1,2,3,0)
> Y <- beta[1] + X %*% beta[-1] + rnorm(100, sd=3)
> coef(fit)
 0.935245814  1.974775625  3.035927924 -0.009531117 
> summary(fit)$cov.unscaled
  5.032666e-03  3.781279e-05  3.456918e-05  1.951668e-06
  3.781279e-05  2.552126e-04 -2.153433e-06 -2.274621e-06
  3.456918e-05 -2.153433e-06  4.515081e-05  5.939866e-07
  1.951668e-06 -2.274621e-06  5.939866e-07  3.598318e-05
\end{verbatim}
\vspace{-0.5cm}
}

\end{frame}

\begin{frame}[fragile]

\sk How does this sampling distribution differ from the empirical
distribution obtained by the following bootstrap procedure?

\sk For $b \in \{1,\dots,B\}$

\begin{enumerate}
\item Sample $Z^{*b} = (X,Y)^{(*b)}$.
\item Calculate $\hat{\beta}^{*b} = (X^{*b\top} X^{*b})^{-1}
  X^{*b\top} Y^{*b}$.
\end{enumerate}

{\bl \footnotesize
\begin{verbatim}
> B <- 10000
> beta.hat.boot <- matrix(NA, nrow=B, ncol=length(beta))
> for(b in 1:B) {
+   indices <- sample(1:nrow(X), nrow(X), replace=TRUE)
+   beta.hat.boot[b,] <- coef(lm(Y[indices]~X[indices,]))
+ }
\end{verbatim}
}
\end{frame}


\begin{frame}[fragile]
\nochap

One option is to compare covariances.

{\bl \footnotesize
\begin{verbatim}
> cov(beta.hat.boot)
            [,1]         [,2]          [,3]          [,4]
[1,]  0.05700913 1.367853e-04  4.558475e-04 -6.616389e-04
[2,]  0.00013678 7.365028e-04  6.926400e-06  3.959462e-05
[3,]  0.00045584 6.926400e-06  1.299969e-03 -5.295993e-05
[4,] -0.00066163 3.959462e-05 -5.295993e-05  4.786919e-04
\end{verbatim}
}
\begin{itemize}
\item more similar in some entries than in others.
\end{itemize}

\end{frame}


\begin{frame}
\nochap
A perhaps clearer comparison is available through the marginals of the
sampling distributions. \hfill [see \R{.R} file]

\begin{center}
\includegraphics[scale=0.45,trim=10 30 5 40]{boot_regress_1}
\includegraphics[scale=0.45,trim=10 30 5 40]{boot_regress_2}\\
\includegraphics[scale=0.45,trim=10 10 5 40]{boot_regress_3}
\includegraphics[scale=0.45,trim=10 10 5 40]{boot_regress_4}
\end{center}
\end{frame}


\begin{frame}
\nochap

They are different!  {\rd Why?}

\sk The usual sampling distribution for $\hat{\beta}$ is conditional
on $X$ (through the model for $Y|X$).

\sk Whereas the bootstrap empirical distribution of $\hat{\beta}$ is
constructed from the joint distribution $(X,Y)$.  
\begin{itemize}
\item We resampled {\em both}!
\end{itemize}

\sk 
If the marginal distribution for $X$ is not uniform, the bootstrap
distribution for $\hat{\beta}$ can be quite different from the typical
one.
\begin{itemize}
\item The bootstrap version will more accurately account for any
  unusually high leverage predictors (outliers).
\end{itemize}

\end{frame}

\begin{frame}
\nochap

\vspace{-0.25cm}
To wrap up, we deployed \R{for} loops and \R{sample} to generate
bootstrap resamples.

\sk
Another option is \R{boot(data, statistic, R, ...)}, in the \R{boot} library,
specifying
\begin{itemize}
\item the data frame
\item the statistic to calculate (an {\sf R} expression/function)
\item and the number of bootstrap samples
\end{itemize}

\sk At first glance this seems easier.  
\begin{itemize}
\item But the \R{for} loops for the bootstrap aren't hard,
\item and I find comfort in the transparency of my own 
implementation(s).
\end{itemize}
\end{frame}

\begin{frame}
\chap{Monte Carlo Bakeoff}

Another way Monte Carlo is used in statistics is to 
compare estimators/predictors 
\begin{itemize}
\item on randomly generated training and testing sets.
\end{itemize}

\sk Sometimes this is done by bootstrapping, 
\begin{itemize}
\item but usually its by {\bl cross validation (CV)}.
\end{itemize}

\sk The hard part is that
\begin{itemize}
\item many predictors are computationally demanding;
\item we want to compare lots of them;
\item on big data in {\rd many repetitions} to assess
{\rd variability}.
\end{itemize}
\end{frame}

\begin{frame}
\nochap 

\vspace{-0.25cm}
{\bl CV} estimates the generalization error
$\text{Err} = \E\{L(Y, \hat{f}(X))\}$,
where $X$ and $Y$ are members of {\em both} training
and testing sets.

\sk This is accomplished by splitting the data into 
$K$, e.g., $K=5$, roughly equal-sized parts.

\begin{center}
\includegraphics[scale=0.25]{htf_7_cvfolds}
\end{center}

For each of $k$ {\bl folds}, fit the model using
data from the other $K-1$ {\bl folds}, and use data from the 
$k^\mathrm{th}$ {\bl fold} to
asses OOS predictive accuracy for the fitted model,
\begin{itemize}
\item and aggregate over $k=1,2,\dots,K$.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.25cm}
Here is a simple way to generate the folds in {\sf R}.

{\bl
\begin{verbatim}
> cv.folds <- function (n, folds = 10)
+    split(sample(1:n), rep(1:folds, length=n))
\end{verbatim}
}

\vspace{-0.25cm}
\begin{itemize}
\item First generate a random permutation of \R{1:n} via \R{sample()}.
\item Then break into \R{folds} chunks via \R{split()}.
\end{itemize}

\vspace{-0.25cm}
{\bl
\begin{verbatim}
> folds <- cv.folds(30, folds=5)
> folds[[1]]
[1] 30 13  7 17  8 26
> folds[[5]]
[1] 28 27 19  6 14 20
\end{verbatim}
}

\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.25cm}
A {\em very} simple example using our gamma data:

\vspace{-0.25cm}
{\bl
\begin{verbatim}
> pab <- pmean <- pmed <- rep(NA, n)
> for(k in 1:length(folds)) {
+   fk <- folds[[k]]
+   pmean[fk] <- mean(x[-fk])
+   pmed[fk] <- median(x[-fk])
+   pab[fk] <- alpha/ibeta
+ }
> c(sqrt(mean((pmean - x)^2)), mean(abs(pmean-x)))
[1] 7.893474 6.578300
> c(sqrt(mean((pmed - x)^2)), mean(abs(pmed-x)))
[1] 7.887034 6.575112
> c(sqrt(mean((pab - x)^2)), mean(abs(pab-x)))
[1] 7.700866 6.371699
\end{verbatim}
}

\end{frame}

\begin{frame}[fragile]
\nochap

Consider {\em industrializing} this simple idea to estimators
on the spam data (from the stats lecture).

\sk See the {\bl \verb!spam_mc.R!} file.  

\sk The same principles are in play.
\begin{itemize}
\item Loop over CV folds.
\item Save predictions for {\rd each} element of the hold out set
\item thereby collecting predictions for {\rd every} data record.
\item Repeat several times to get a sense of the variability.
\item Visualize the results.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.5cm}
{\bl
\begin{verbatim}
> source("spam_mc.R") ## wait a few hours
> hit <- function(x, y=spam$spam)
+   mean(x == y, na.rm=TRUE)
> df <- data.frame(null=apply(pnull, 1, hit),
+                  full=apply(pfull, 1, hit),
+                  fwd=apply(pfwd, 1, hit),
+                  fwdi=apply(pfwdi, 1, hit),
+                  lda=apply(plda, 1, hit),
+                  qda=apply(pqda, 1, hit),
+                  fda=apply(pfda, 1, hit),
+                  rp=apply(prp, 1, hit),
+                  mnlm=apply(pmnlm, 1, hit),
+                  rf=apply(prf, 1, hit))
> boxplot(df[,-1], ylab="hit rate")
\end{verbatim}
}

\end{frame}

\begin{frame}
\nochap

\vspace{-0.5cm}
\begin{center}
\includegraphics[scale=0.7,trim=10 60 10 60]{spam_mc_0}
\end{center}

\begin{itemize}
\item More information (on average behavior and variability),
\item but is five repetitions enough?
\end{itemize}
\end{frame}

\begin{frame}
\nochap

It took several hours to do {\em just} those five repetitions.
\begin{itemize}
\item How can we get more without waiting much longer?
\item {\rd Parallelize!}
\end{itemize}

\sk
Modern desktops (even laptops these days) have multiple cores.
\begin{itemize}
\item My laptop has two hyperthreaded cores (effectively four).
\item My desktop has eight (effectively 16).
\end{itemize}

\sk What's the easiest way to take advantage of all that power?
\begin{itemize}
\item Via the {\rd shell} command line.
\end{itemize}

\end{frame}

\begin{frame}[fragile]
\chap{Batch Mode}

You can run {\sf R} scripts from the shell 
\begin{itemize}
\item e.g., a DOS prompt, or a Unix/Linux terminal
\end{itemize}
in {\rd batch} without starting the interactive prompt.

\sk Suppose you had a file called {\bl \verb!spam_mc.R!}.
\begin{enumerate}
\item Navigate to the directory you want {\sf R} to use as its CWD.
\item Invoke {\sf R} in batch mode on your script.
{\bl
\begin{verbatim}
% R CMD BATCH spam_mc.R
\end{verbatim}
}
\item Monitor progress via the {\bl \verb!spam_mc.Rout!} file.
\item Upon completion, the \R{.RData} file (by default) will contain the saved
session.
\end{enumerate}
\vspace{-0.25cm}
\end{frame}

\begin{frame}[fragile]
\nochap

You can repeat this several times, from different working directories
\begin{itemize}
\item so the output files don't trample one another
\end{itemize}
to run several {\sf R} batch scripts in parallel.

\sk But upon reflection, that's a cumbersome procedure.  With 
\begin{itemize}
\item a little extra {\sf R} code in the batch script ({\bl
\verb!spam_mc.R!}), 
\item some {\rd command-line arguments},
\item and with the no-hangup/background execution features of Unix,
\end{itemize}
 we can make our lives a lot easier.

\end{frame}

\begin{frame}[fragile]
\nochap

The goal is to be able to execute the following commands from the shell
{\bl \tiny
\begin{verbatim}
% nohup R CMD BATCH '--args seed=1 reps=5' spam_mc.R spam_mc_1.Rout &
% nohup R CMD BATCH '--args seed=2 reps=5' spam_mc.R spam_mc_2.Rout &
% nohup R CMD BATCH '--args seed=3 reps=5' spam_mc.R spam_mc_3.Rout &
...
\end{verbatim}
}
\vspace{-0.25cm}
specifying
\begin{itemize}
\item \R{seed} and \R{reps} variables to be used in the {\sf R} session to specify the RNG seed (why?), and the number of CV repetitions
\item and the output file(s) for (separate) monitoring,
\end{itemize}

\sk Finally, {\bl \verb!spam_mc.R!} should use those arguments to
create distinct files containing the needed output(s).

\end{frame}

\begin{frame}[fragile]
\nochap

This can be accomplished with the following {\rd pre-ample}
in the batch script ({\bl \verb!spam_mc.R!}).

{\bl
\begin{verbatim}
seed <- 0
reps <- 5
args <- commandArgs(TRUE)
if(length(args) > 0) 
  for(i in 1:length(args)) 
    eval(parse(text=args[[i]]))
cat("seed is ", seed, "\n", sep="")
set.seed(seed)
cat("reps is ", reps, "\n", sep="")
\end{verbatim}
}

\end{frame}

\begin{frame}[fragile]
\nochap

... and a \R{save()} at the end (or at each repetition/fold),
that uses the command-line arguments to name the file.

{\bl
\begin{verbatim}
save(pnull, pfull, pfwd, pfwdi, plda,
     pqda, pfda, prp, pmnlm, prf, 
     file=paste("spam_", seed, ".RData", sep=""))
\end{verbatim}
}

This setup
\begin{itemize}
\item allows multiple copies of the script to run in the same CWD,
\item reproducibility (set seeds) for debugging purposes.
\end{itemize}

\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.25cm}
It is also a good idea to include code to print a message that will
allow you to track the progress of (background) execution.

{\bl
\begin{verbatim}
    cat("(r,i)=(", r, ",", i, ")\n", sep="")
\end{verbatim}
}

\sk That way, we can {\bl grep} for those print statements.

{\bl
\begin{verbatim}
% grep "\(r,i\)" spam_mc_1.Rout 
+     cat("(r,i)=(", r, ",", i, ")\n", sep="")
(r,i)=(1,1)
(r,i)=(1,2)
\end{verbatim}}
\vspace{-0.25cm}
\begin{itemize}
\item The \R{seed=1} version is in the second fold of the first repetition.
\end{itemize}

\end{frame}

\begin{frame}[fragile]
\chap{Collecting results}

The saved output from each independent batch run is saved in a different file.
\begin{itemize}
\item We need to collect them together in order to summarize the output of the full MC experiment.
\end{itemize}

\sk The most important element of such a {\em script} is
{\bl \footnotesize
\begin{verbatim}
files <- list.files("./", pattern="spam_[0-9]*.RData")
\end{verbatim}
}
\vspace{-0.25cm}
\begin{itemize}
\item creating a vector of file names for each saved output file in the CWD.
\item Then loop over each file, load it, and collect the items into one
 data structure. \hfill [See {\bl \verb!spam_mc_collect.R!}.]
\end{itemize}

\end{frame}

\begin{frame}
\nochap

\vspace{-0.25cm}
Here is what we get from the $5\times7=35$, 5-fold CVs.

\begin{center}
\includegraphics[scale=0.7,trim=10 60 10 50]{spam_mc_all}
\end{center}

\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.25cm}
The boxplots provide a nice visualization, and we can rank the predictors by
their average hit-rate.
\begin{itemize}
\item To round things out we can use a {\bl paired $t$-test} to comment on
which pairs of orderings are statistically significant.
\end{itemize}

{\bl
\begin{verbatim}
> dfmean <- apply(df, 2, mean)
> o <- order(dfmean, decreasing=TRUE)
> tt <- rep(NA, length(dfmean))
> for(i in 1:(length(o)-1)) {
+   tto <- t.test(df[,o[i]], df[,o[i+1]], 
+            alternative="greater", paired=TRUE)
+   tt[o[i]] <- tto$p.value
+ }
\end{verbatim}
}

\end{frame}

\begin{frame}[fragile]

{\bl
\begin{verbatim}
> cbind(data.frame(dfmean), data.frame(tt))[o,]
        dfmean           tt
rf   0.9521470 2.625419e-33
fwdi 0.9328966 7.449251e-05
mnlm 0.9303319 8.087151e-09
full 0.9241097 1.652646e-06
fwd  0.9201975 2.733655e-35
rp   0.8939982 2.922216e-15
fda  0.8875151 2.856188e-01
lda  0.8875089 3.329330e-47
qda  0.8291542 1.174428e-66
null 0.6059552           NA
\end{verbatim}
}
\end{frame}

\begin{frame}
\chap{Parallel Programming: SNOW}

There are fancier (fuller-featured) ways to get parallel execution in {\sf R},
\begin{itemize}
\item Several use a {\rd simple network of workstations (SNOW)} model.
\item E.g., one option is the \R{snow} (now \R{parallel}) package.
\item A couple others called \R{snowfall} and \R{sfCluster} offer extensions.
\end{itemize}

There are many others.  See
\vspace{-0.25cm}
\begin{center}
{\bl\url{http://cran.r-project.org/web/views/HighPerformanceComputing.html}}
\end{center}
\vspace{-0.2cm}
including interfaces to MPI, Hadoop, and more.
\vspace{-0.25cm}
\end{frame}

\begin{frame}
\nochap

\vspace{-0.25cm}
To illustrate \R{snow}/\R{parallel}, consider the following example.

\sk Let \R{A} be an {\rd adjacency matrix} of a graph with $n$ nodes:
\begin{itemize}
\item  \R{A} is $n \times n$ and \R{A[i,j]} is either \R{1} or
\R{0}, depending on whether there is an edge from node \R{i} to \R{j} in the
graph.
\item The edges may represent links between websites or in a social network.
\end{itemize}

For any two nodes, say websites, we might be interested in {\rd mutual
outlinks}:
\begin{itemize}
\item outbound links (edges) that are common to two sites.
\end{itemize}

\sk Suppose we wanted to find the mean number of mutual outlinks,
averaged over all pairs of websites in the data.

\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.25cm}
Here is an initial implementation.

\vspace{-0.15cm}
{\bl
\begin{verbatim}
> mutlinks <- function(A)
+   {
+     n <- ncol(A)
+     if(nrow(A) != n) stop("A must be square")
+     S <- 0
+     for(i in 1:(n-1))
+       for(j in (i+1):n) 
+         for(k in 1:n) S <- S + A[i,k]*A[j,k]
+     S/choose(n,2)
+   } 
\end{verbatim}
}
\vspace{-0.25cm}
\begin{itemize}
\item Three nested for loops!  {\rd Necessary? Fast?}
\item That's a topic for the next lecture.
\end{itemize}

\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.5cm}
{\bl \footnotesize
\begin{verbatim}
> A <- matrix(sample(0:1, 16, replace=TRUE), nrow=4)
> mutlinks(A)
[1] 0.3333333
\end{verbatim}
}

Seems to work ok on a small problem.  How about larger ones?
\vspace{-0.3cm}
{\bl \footnotesize
\begin{verbatim}
> A <- matrix(sample(0:1,(8^2)^2,replace=TRUE),nrow=8^2)
> system.time(mutlinks(A))
   user  system elapsed 
  0.258   0.001   0.263 
> A <- matrix(sample(0:1,(16^2)^2,replace=TRUE),nrow=16^2) 
> system.time(mutlinks(A))
   user  system elapsed 
 15.994   0.081  17.363 
\end{verbatim}
}

\vspace{-0.25cm}
\begin{itemize}
\item Doesn't scale well with $n$, the dimension of \R{A}.
\end{itemize}
\end{frame}

\begin{frame}
\nochap

Modern computational architectures present a {\rd divide-and-conquer} 
option.

\sk Suppose we have two computers/cores at our disposal.
\begin{itemize}
\item One core could handle odd \R{i}
(in the \R{for i} loop)
\item and the other could do the even ones.
\end{itemize}

\sk And of course, you can extend that idea to $\geq 2$ cores.
\begin{itemize}
\item So we need a function that can do the same calculation as \R{multlinks()}
on a subset of the \R{i}-values.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.5cm}
{\bl
\begin{verbatim}
> mtl <- function(ichunk, A)
+   {
+     n <- ncol(A)
+     if(nrow(A) != n) stop("A must be square")
+     S <- 0
+     for(i in ichunk) {
+       if(i < n) {
+         rowi <- A[i,]
+         S <- S + sum(A[(i+1):n,] %*% rowi)
+       }
+     }
+     S
+   }
\end{verbatim}
}

\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.35cm}
Notice how the new function uses some vectorization,
but it deliberately does not factor in the denominator.
\begin{itemize}
\item Its faster, and (just checking) gives the same
output.
\end{itemize}

\vspace{-0.25cm}
{\bl \footnotesize
\begin{verbatim}
> mutlinks(A)
[1] 63.34283
> system.time(Amtl <- mtl(1:nrow(A), A))
   user  system elapsed 
  0.143   0.069   0.227 
> Amtl/choose(nrow(A), 2)
[1] 63.34283
> A <- matrix(sample(0:1,(24^2)^2,replace=TRUE),nrow=24^2)
> system.time(mtl(1:nrow(A), A))
   user  system elapsed 
  2.323   0.288   2.823 
\end{verbatim}
}
\vspace{-0.5cm}
\begin{itemize}
\item but scaling-with-$n$ issues are still present.
\end{itemize}

\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.25cm}
Using \R{mtl()} with \R{snow} requires a master function that divides up the
input matrix, \R{A}, into separate chunks and combines the results.

{\bl
\begin{verbatim}
> mutlinks.snow <- function(cls, A)
+   {
+     n <- ncol(A)
+     if(nrow(A) != n) stop("A must be square")
+     nc <- length(cls)
+     suppressWarnings(ichunks <- split(1:n, 1:nc))
+     Ss <- clusterApply(cls, ichunks, mtl, A)
+     do.call(sum, Ss) / choose(n, 2)
+   }
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap

Then we need to (1) establish the ``cluster''; (2) run \R{mutlinks.snow()}; (3)
close down the cluster.

{\bl
\begin{verbatim}
> library(parallel)
> cl <- makeCluster(type="PSOCK", 
+     c("localhost", "localhost"))
> system.time(mutlinks.snow(cl, A))
   user  system elapsed 
  0.015   0.005   1.627 
> stopCluster(cl)
\end{verbatim}
}

\begin{itemize}
\item Faster, for sure, but not $2\times$ faster.  Why?
\item In fact, we can generally expect diminishing returns as ``nodes'' are
added into the cluster.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\nochap

You can make clusters of any size, but it doesn't make sense to make ones that are larger than the number of computing cores at hand.

\begin{itemize}
\item You can specify names of machines rather than {\bl \verb!"localhost"!}.
\item However, note that it is important to minimize costly data transfers
(time) over the network.
\end{itemize}

\sk
You can utilize \R{snow}/\R{parallel} in several other ways.  E.g., via
\begin{itemize}
\item \R{Map()}/\R{Reduce()}.
\end{itemize}

\end{frame}

% \begin{frame}
% \chap{``Homework''}

% \begin{itemize}
% \item How would adjust the code for the Annie \& Sam example(s) to
%   accomodate other distributions?  E.g., if $S \sim \mathcal{N}(10.5,
%   1)$ and $A\sim\mathcal{N}(11, 1.5)$?
% \item Try using the bootstrap in order to get a sense of predictive
% undertainty in one of our \R{smooth.spline()} fits from lecture 6
% [\R{stats.R}].
% \item Try re-writing the spam MC bakeoff using \R{snow}.
% \end{itemize}

% \end{frame}

\end{document}
