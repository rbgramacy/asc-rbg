\documentclass[12pt,xcolor=svgnames]{beamer}
\usepackage{dsfont,natbib,setspace}
\mode<presentation>

% replaces beamer foot with simple page number
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}{
  \vspace{-1.5cm}
  \raisebox{10pt}{\makebox[\paperwidth]{\hfill\makebox[20pt]{\color{gray}\scriptsize\insertpagenumber}}}}

% colors
\newcommand{\bk}{\color{black}}
\newcommand{\rd}{\color{red}}
\newcommand{\fg}{\color{forestgreen}}
\newcommand{\bl}{\color{blue}}
\newcommand{\gr}{\color{gray}}
\newcommand{\theme}{\color{Maroon}}

% common math markups
\newcommand{\bs}[1]{\boldsymbol{#1}}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mr}[1]{\mathrm{#1}}
\newcommand{\bm}[1]{\mbox{\boldmath $#1$}}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\ds}[1]{\mathds{#1}}

% spacing and style shorthand
\newcommand{\sk}{\vspace{.4cm}}
\newcommand{\nochap}{\vspace{0.6cm}}
\newcommand{\nsk}{\vspace{-.4cm}}
\newcommand{\chap}[1]{{\theme \Large \bf #1} \sk}
\newcommand{\R}[1]{{\bl\tt #1}}
\newcommand{\til}{{\footnotesize$\bs{\stackrel{\sim}{}}$ }}

% specific stats markups for this doc
\newcommand{\E}{\ds{E}}
\newcommand{\Reals}{\ds{R}}
\newcommand{\var}{\text{var}}
\newcommand{\cov}{\text{cov}}
\newcommand{\mT}{\mc{T}}
\newcommand{\GP}{\mc{GP}}
\newcommand{\iidsim}{\stackrel{\mathrm{iid}}{\sim}}
\newcommand{\indsim}{\stackrel{\mathrm{ind}}{\sim}}
\newcommand{\mN}{\mc{N}}

\begin{document}

{ %\usebackgroundtemplate{\includegraphics[height=\paperheight]{phoenix}}
\thispagestyle{empty}
\setcounter{page}{0}

\title{\Large \theme 
\includegraphics[scale=0.1]{../Rlogo}\\
\bf {\sf Working with compiled code}}
\author{\vskip .5cm {\bf Robert B.~Gramacy}\\{\color{Maroon}
    Virginia Tech}
  Department of Statistics\\
  \vskip .2cm \texttt{\rd bobby.gramacy.com}
  \vskip .25cm}
\date{}
\maketitle }

% doc spacing
\setstretch{1.1}

\begin{frame}
\chap{Why Compiled Code?}

Faster:
\begin{itemize}
\item Compiled code is usually faster than interpreted code.
\item Better control over memory management, use of pointers.
\end{itemize}

\sk 
External Libraries:
\begin{itemize}
\item The best (most reliable and optimized) libraries, especially for
scientific computing, are  in {\sf C} and {\sf Fortran}.
\end{itemize}

\sk Production:
\begin{itemize}
\item {\sf R} is great for prototyping and data analysis, but its not ideal
for plugging into existing workflows.
\end{itemize}
\vspace{-0.25cm}
\end{frame}

\begin{frame}[fragile]
\chap{Loops}

Loops are very slow in {\sf R}.
\begin{itemize}
\item E.g., \R{mutlinks.R()} from last week's lecture.
\end{itemize}

{\bl \small
\begin{verbatim}
mutlinks.R <- function(A)
  {
    n <- ncol(A)
    if(nrow(A) != n) stop("A must be square")
    S <- 0
    for(i in 1:(n-1))
      for(j in (i+1):n) 
        for(k in 1:n) S <- S + A[i,k]*A[j,k]
    S/choose(n,2)
  } 
\end{verbatim}
}
\vspace{-0.5cm}
\end{frame}

\begin{frame}[fragile]
\nochap

Here is a {\sf C} implementation.

{\bl
\begin{verbatim}
double mutlinks(int **A, int n)
  {
    unsigned int i, j, k, S;

    S = 0;
    for(i=0; i<n; i++) 
      for(j=i+1; j<n; j++)
        for(k=0; k<n; k++) S += A[i][k]*A[j][k];

    return(((double) S)/(n*(n-1)/2));
  }
\end{verbatim}}
\end{frame}

\begin{frame}
\nochap

How can we use the {\sf C} version in {\sf R}?

\sk Four ingredients are required.
\begin{enumerate}
\item A way to compile the {\sf C} code --- build a library --- an {\sf
R}-friendly way.
\item Load that compiled code into {\sf R}.
\item Pass data ({\sf R} objects) to the {\sf C}-side, and prepare data
structures to receive the results.
\item Receive the data on the {\sf C} side and pass the results back to {\sf
R}.
\end{enumerate}

\end{frame}

\begin{frame}[fragile]
\chap{Compiling}

I usually keep {\sf C} code for a specific {\sf R} project in a directory
called
\R{src}, adjacent to an \R{R} directory, containing the {\sf R} code.

\sk Inside the \R{src} directory, I use \R{R CMD SHLIB} to build a shared
object containing the compiled code I want to load into {\sf R}.

\vspace{0.25cm}
\begin{itemize}
\item Here is how I built the shared object for code supporting this lecture.
\end{itemize}

\vspace{-0.25cm}
{\bl
\begin{verbatim}
% R CMD SHLIB -o clect.so mutlinks.c bootreg.c
\end{verbatim}
}

\vspace{-0.25cm}
\begin{itemize}
\item building the shared object \R{clect.so}.
\end{itemize}

\end{frame}

\begin{frame}[fragile]
\chap{Loading shared objects}

From inside the \R{R} directory, the shared object can be loaded with
\R{dyn.load()}.

\sk In {\sf R}:
{\bl \begin{verbatim}
> dyn.load("../src/clect.so")
\end{verbatim}
}

Then, all {\sf C} functions in the shared object are callable from {\sf R}
\begin{itemize}
\item e.g., with \R{.C()}.
\end{itemize}

\sk However, most {\sf C} functions are not ``ready'' to receive data from {\sf R}
\begin{itemize}
\item e.g., via \R{.C()}.
\end{itemize}
\vspace{-0.25cm}
\end{frame}

\begin{frame}
\chap{.C interface}

\R{.C()} can only ``send'' (and ``recieve'') {\rd pointers} to \R{double}s,
\R{int}egers, and \R{char}acter strings.
\begin{itemize}
\item and it must {\rd pre-allocate} memory pointed to by results it will
recieve.
\item The lengths of data passed (i.e., pointed to) can be arbitrary, but must be
unstructured (i.e., a flat array).
\item Any flattening of arrays will be in {\rd col-major} order.
\item Dimensions of objects must be passed separately, also as pointers.
\end{itemize}

\sk \R{.C()} is less sophisticated than {\sf MATLAB} \R{mex}.

\end{frame}

\begin{frame}[fragile]
\chap

Here is an appropriate \R{.C()} call for \R{mutlinks()}.
{\bl \small
\begin{verbatim}
mutlinks <- function(A) 
  {
    n <- ncol(A)
    if(nrow(A) != n) stop("A must be square")

    ret <- .C("mutlinks_R",
              A = as.integer(t(A)),  ## col-major!
              n = as.integer(n),
              aS = double(1),
              DUP = FALSE)

    return(ret$aS)
  } 
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\chap{Receiving .C()}

Now, we need a {\sf C}-side function that can accept the pointers,
\begin{itemize}
\item and call our \R{mutlinks} {\sf C} function.
\end{itemize}

{\bl \small
\begin{verbatim}
void mutlinks_R(int *A_in, int *n_in, double *as_out)
  {
    unsigned int i;
    int **A;
    A = (int **) malloc(sizeof(int*) * (*n_in));
    A[0] = A_in;
    for(i=1; i<*n_in; i++) A[i] = A[i-1] + *n_in;
    *as_out = mutlinks(A, *n_in);
    free(A);
  }
\end{verbatim}
}
\vspace{-0.5cm}
\end{frame}

\begin{frame}[fragile]
\nochap

Is all this effort worth it?

{\bl
\begin{verbatim}
> A <- matrix(sample(0:1, (16^2)^2, replace=TRUE), 
+             nrow=16^2)
> system.time(aS <- mutlinks(A))
   user  system elapsed 
   0.04    0.00    0.04 
> aS
[1] 64.53793
> system.time(aS.R <- mutlinks.R(A))
   user  system elapsed 
 11.085   0.022  11.118 
> aS.R
[1] 64.53793
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\chap{Best practice/Style}

There are lots of ways to cut corners and come out ok.

\begin{itemize}
\item We don't really need separate \R{mutlinks} and {\bl \verb!mutlinks_R!} functions.
\item We could skip converting \R{A} to a 2-d array (and avoid the transpose).
\item \R{DUP=FALSE} isn't necessary (in fact, its risky).
\end{itemize}

\sk But I consider this structure to be {\rd good practice}.
\begin{itemize}
\item {\rd Prototype} an {\sf R}-only version first (with a \R{.R} extension),
\item and then write a {\sf C}-version (with matching output).
\end{itemize}
\vspace{-0.25cm}
\end{frame}

\begin{frame}[fragile]
\nochap

Lets revisit our bootstrap regression example from last time.

\sk Here is a simple R version.
{\bl \footnotesize
\begin{verbatim}
bootols.R <- function(X, y, B=199, icept=TRUE, uselm=FALSE)
{
  if(icept) X <- cbind(1, X)
  if(nrow(X) != length(y)) stop("dimension mismatch")
  beta <- matrix(NA, nrow=B, ncol=ncol(X))
  for(b in 1:B) {
    i <- sample(1:n, n, replace=TRUE)
    Xb <- X[i,]; yb <- Y[i]
    beta[b,] <- ols.R(Xb, yb, uselm=uselm, icept=FALSE)
  }
  return(beta)
}
\end{verbatim}
}

\end{frame}

\begin{frame}[fragile]
\nochap

Here is the OLS subroutine applied to each bootstrap sample.

{\bl \footnotesize
\begin{verbatim}
ols.R <- function(X, y, icept=TRUE, uselm=FALSE)
{
  if(icept) X <- cbind(1, X)
  if(nrow(X) != length(y)) stop("dimension mismatch")
  if(uselm) beta <- drop(coef(lm(y~X-1)))
  else beta <- drop(solve(t(X) %*% X) %*% t(X) %*% y)
  return(beta)
}
\end{verbatim}
}
\vspace{-0.5cm}
\begin{itemize}
\item It offers the option of calling \R{lm()}, which does extra work we don't
need for our bootstrap (but it does it very quickly in {\sf C});
\item Or, it can do the simple matrix algebra alone (but in {\sf R}).
\end{itemize}

\end{frame}

\begin{frame}
\nochap

Can we write a better/faster version in {\sf C} 
\begin{itemize}
\item without reinventing the wheel on aspects of linear algebra?
\item without writing a random number generator (for \R{sample()})?
\end{itemize}

\sk {\rd Yes}, because {\sf R}'s compiled {\sf Fortran} and {\sf C} 
libraries are available to us on the {\sf C}-side.
\begin{itemize}
\item But that doesn't mean it'll be trivial.
\end{itemize}

\sk We'll start by writing a simple OLS calculation in {\sf C} and calling it
from {\sf R}.
\end{frame}


\begin{frame}
\nochap 

See \R{bootreg.c} for a {\sf C} version of our {\sf R} prototype \R{ols.R()}, called \R{ols()}
\begin{itemize}
\item and all of the other {\sf C} code for this example as well.
\end{itemize}

\sk Notice that this {\sf C} function 
\begin{itemize}
\item calls some {\sf Fortran} libraries for optmized {\sf BLAS/Lapack} linear
 algebra: (\R{dgemv}, \R{dgemm}, \R{dposv}, \R{dsymv}) --- the same ones {\sf
 R} and {\sf MATLAB} use.
\item It uses temporary space allocated elsewhere (\R{XtY},
 \R{XtX}, \R{XtXi}).  {\rd Why?}
\item The comments indicate what {\sf R} commands the {\sf C/Fortran} calls correspond to.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\chap

\vspace{-0.25cm}
It also uses a function that writes the identity to
a pre-allocated matrix:

{\bl \footnotesize
\begin{verbatim}
void zero(double **M, unsigned int n1, unsigned int n2)
{
  unsigned int i, j;
  for(i=0; i<n1; i++) for(j=0; j<n2; j++) M[i][j] = 0;
}

void id(double **M, unsigned int n)
{
  unsigned int i;
  zero(M, n, n);
  for(i=0; i<n; i++) M[i][i] = 1.0;
}
\end{verbatim}
}

That's required by \R{dposv} which solves a linear system.

\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.5cm}
Use of the {\sf BLAS/Lapack} library requires special linking arguments in the
building of the shared object.
\begin{itemize}
\item Basically, we have to tell the linker where to look for those libraries.
\end{itemize}

\sk \R{R CMD SHLIB} will read a \R{Makevars} file in the CWD.
\begin{itemize}
\item For {\sf BLAS/Lapack} that file should contain, at minimum
\sk
{\bl \small
\begin{verbatim}
PKG_LIBS = ${LAPACK_LIBS} ${BLAS_LIBS} ${FLIBS}
\end{verbatim}
}
\sk
\item We'll add some other things to it later.
\item These definitions extend the ones in {\bl \verb!$RINSTALL/etc/Makevars!}.
\item You can hard-code paths to whatever libraries you want.

\end{itemize}

\end{frame}

\begin{frame}
\nochap

\vspace{-0.25cm}
The {\sf BLAS/Lapack} bundled with {\sf R} binaries may not be optimized for
your machine.

\sk If you have access to custom linear algebra libraries
\begin{itemize}
\item Intel's MKL library
\item Apple's {\tt vecLib} Framework
\item ATLAS's automatically tuned libraries
\end{itemize}

then {\sf R}, and your add-on {\sf C}-routines, will get {\rd great benefit}
by linking to those libraries.

\sk Re-compiling {\sf R} to link to those libraries isn't that difficult.
\begin{itemize}
\item Competent IT folks are familiar with this sort of thing.
\item And its well worth your/their time.
\end{itemize}

\end{frame}

\begin{frame}[fragile]
\chap

The {\sf R} interface:
{\bl \footnotesize
\begin{verbatim}
ols <- function(X, y, icept=TRUE)
{
  if(icept) X <- cbind(1, X)  
  m <- ncol(X)
  n <- nrow(X)
  if(n != length(y)) stop("dimension mismatch")
  ret <- .C("ols_R",
          X = as.double(t(X)),
          y = as.double(y),
          n = as.integer(n),
          m = as.integer(m),
          beta.hat = double(m),
          DUP = FALSE)
  return(ret$beta.hat)
}
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap

So the {\sf C} function {\bl \verb!ols_R!} must 
\begin{itemize}
\item convert inputs
\item allocate the temporary space,
\item call \R{ols()}
\item clean up.
\end{itemize}

\sk It requires some new matrix creation/destruction functions, so that the temporary space is set up correctly.
\begin{itemize}
\item {\bl \verb!new_matrix()!} and {\bl \verb!delete_matrix()!}
\end{itemize}

\sk
\hfill [see \R{bootreg.c}]
\end{frame}

\begin{frame}[fragile]
\nochap

How does the {\sf C}-version compare?

{\bl \footnotesize
\begin{verbatim}
> source("bootreg.R")
> n <- 2000; d <- 100
> X <- 1/matrix(rt(n*d, df=1), ncol=d)
> beta <- c(1:d, 0)
> Y <- beta[1] + X %*% beta[-1] + rnorm(100, sd=3)
> system.time(fit.R <- ols.R(X, Y))
   user  system elapsed 
  0.046   0.001   0.046 
> system.time(fit.lm <- ols.R(X, Y, uselm=TRUE))
   user  system elapsed 
  0.068   0.002   0.070 
> system.time(fit <- ols(X, Y))
   user  system elapsed 
  0.024   0.001   0.024 
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap

Maybe not super impressive ...
\begin{itemize}
\item But how about inside the bootstrap?
\item This is where the re-use of space will come in handy --- something {\sf R} can't do.
\end{itemize}

%% \sk Here is our {\sf R} prototype.
{\bl \footnotesize
\begin{verbatim}
bootols.R <- function(X, y, B=199, icept=TRUE, uselm=FALSE)
{
  if(icept) X <- cbind(1, X)
  beta <- matrix(NA, nrow=B, ncol=ncol(X))
  for(b in 1:B) {
    i <- sample(1:n, n, replace=TRUE)
    Xb <- X[i,]; yb <- Y[i]
    beta[b,] <- ols.R(Xb, yb, uselm=TRUE, icept=FALSE)
  }
  return(beta)
}
\end{verbatim}
}

\end{frame}

\begin{frame}[fragile]
\nochap

The {\sf C} version, \R{bootols()} looks rather similar, 
\begin{itemize}
\item except it pre-allocates space for \R{ols}
\end{itemize}
\hfill [see \R{bootreg.R}]

\sk It uses {\sf R}'s uniform random number generator (RNG), {\verb!unif_rand()!},
\begin{itemize}
\item which defaults to the the Mersenne Twister algorithm.
\item This is state of the art (I'd trust {\sf R} on RNGs).
\item Our usage of the RNG assumes we've already got the seed/state form {\sf
R}.
\item We'll save that for {\bl \verb!bootols_R()!} since it is good
practice to deal with the RNG state as part of the {\sf C}/{\sf R} interface.
\end{itemize}

\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.25cm}
{\bl \footnotesize
\begin{verbatim}
bootols <- function(X, y, B=199, icept=TRUE)
{
  if(icept) X <- cbind(1, X)  
  m <- ncol(X)
  n <- nrow(X)
  if(n != length(y)) stop("dimension mismatch")
  ret <- .C("bootols_R",
          X = as.double(t(X)),
          y = as.double(y),
          n = as.integer(n),
          m = as.integer(m),
          B = as.integer(B),
          beta.hat = double(m*B),
          DUP = FALSE)
  return(matrix(ret$beta.hat, nrow=B, byrow=TRUE))
}
\end{verbatim}
}

\end{frame}

\begin{frame}[fragile]
\nochap

Notice how we allocate a \R{double()}-vector of size \R{m*B} for \R{beta.hat},
when we really want the output to be a $B \times m$ matrix.
\begin{itemize}
\item Again, we'll need to remember that {\sf R} uses {\rd column-major}
ordering when we convert it back to a matrix upon return.
\end{itemize}

\sk {\bl \verb!bootols_R()!} must ...
\begin{itemize}
\item convert {\bl X} back into a matrix
\item and do the same with \R{beta.hat}.
\item It also has to get the RNG state, and
\item give it back to {\sf R} when its done.
\end{itemize}
\hfill [see \R{bootreg.c}]
\end{frame}

\begin{frame}[fragile]
\nochap
Now for a comparison
\begin{itemize}
\item using the same data as before.
\end{itemize}

\vspace{-0.25cm}
{\bl \small
\begin{verbatim}
> system.time(beta.hat.R <- 
+     bootols.R(X, Y, B=1000, uselm=TRUE))
   user  system elapsed 
 90.619   3.916 105.457 
> system.time(beta.hat.R <- bootols.R(X, Y, B=1000))
   user  system elapsed 
 91.451   3.226  96.882 
> system.time(beta.hat <- bootols(X, Y, B=1000))
   user  system elapsed 
 32.467   0.041  32.809 
\end{verbatim}
}
\vspace{-0.25cm}
\begin{itemize}
\item $>3\times$ better with a slim compiled version.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\chap{{\sf R}'s {\sf C}-library routines.}

We used {\bl \verb!unif_rand()!} and {\sf R}'s linear algebra.

\sk You are free to use any {\sf C/Fortran} function used to build {\sf R}.
\begin{itemize}
\item The header files are in \R{\$RINSTALL/include}, and can be included with the usual \R{\#include} pre-compiler macros.
\item on {\tt OSX} \R{\$RINSTALL} is 
\begin{center} \small
\R{/Library/Frameworks/R.framework/Resources/}
\end{center}
\item E.g., \R{\#include<R.h>} and \R{\#include<Rmath.h>}.
\item Unfortunately, the headers aren't very revealing about what the
functions do (and what their arguments are).
\item Instead, I suggest consulting the full source.
\end{itemize}
\vspace{-0.25cm}
\end{frame}

\begin{frame}[fragile]
\chap{Debugging}

Debugging {\sf C} code attached to {\sf R} involves the same tools as
debugging standalone {\sf C code},
\begin{itemize}
\item e.g., using an interactive debugger like \R{gdb}.
\item or \R{valgrind} for memory checking, etc.
\end{itemize}

\sk To start the debugger, e.g., \R{gdb} or \R{valgrind}, do
{\bl
\begin{verbatim}
% R -d gdb
% R -d valgrind
\end{verbatim}
}
and interact with \R{gdb} or \R{valgrind} as usual.
\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.25cm}
For both, it is helpful to compile the shared object without any optimization
flags.
\begin{itemize}
\item \R{-O2} is the default.
\item The default already includes \R{-g} on most systems.
\end{itemize}
Otherwise, it may not be possible to inspect the values of all objects, e.g.,
in \R{gdb}.

\sk Add the following line to {\bl \verb!~/.R/Makevars!}:
{\bl
\begin{verbatim}
CFLAGS = -g -Wall -pedantic
\end{verbatim}
}
\begin{itemize}
\item The others help catch problems at compile time.
\item Don't forget to comment this line out when you're debugging: your code
will run slower without \R{-O2}.
\end{itemize}

\end{frame}

\begin{frame}
\chap{Profiling {\sf C}}

On {\sf Linux} you can use \R{sprof} to profile a shared object, the same way
you would any other shared object.
\begin{itemize}
\item \R{sprof} works like \R{gprof} which is for entire executables.
\end{itemize}

\sk On {\sf OSX} you can get a similar, online, summary via the \R{Time
Profiler} in the \R{Instrument} panel.
\begin{itemize}
\item This may be the best {\rd free} profiling tool for compiled code.
\item There are similar ones for Windows/Linux but I am not familiar with
them.
\item The memory/leaks instrument is pretty good too.
\end{itemize}

\end{frame}

\begin{frame}
\nochap

\begin{center}
\includegraphics[scale=0.32,trim=30 30 30 30]{sampler}
\end{center}

\end{frame}

\begin{frame}
\chap{Windows Rtools}

Most of what I have described has been for a Unix (Linux + OSX) environment.

\sk You can do all this on Windows too:
\begin{itemize}
\item compile {\sf R} from scratch (possibly linking to a fast {\sf
BLAS/Lapack})
\item write your own external {\sf C} libraries
\item load shared objects (DLLs)
\end{itemize}

\sk There are many options depending on your {\sf C} compiler.

\sk The simplest is to use \R{gcc}, just like in Unix
\begin{itemize}
\item but that requires installing {\rd Rtools}.
\end{itemize}
\end{frame}

\vspace{-0.75cm}
\begin{frame}
\nochap
\begin{center}
{\bl \url{http://cran.r-project.org/bin/windows/Rtools/}}
\end{center}

\begin{center}
\includegraphics[scale=0.29,trim=30 30 30 15]{rtools}
\end{center}

\end{frame}

\begin{frame}
\chap{Fortran}

Linking compiled {\sf Fortran} code into {\sf R} follows the same rubric as
{\sf C}, except
\begin{itemize}
\item use \R{.Fortran()} instead of \R{.C()}
\item no need to transpose matrices: {\sf Fortran} also uses column-major order.
\end{itemize}

\sk As an example, see the files below which build up the MLE of a MVN mean $\mu$ and covariance $\Sigma$ through a series of \R{sweep()}ing regressions
provided in two {\sf Fortran} subroutines.
\begin{itemize}
\item \R{sweep.f}
\item \R{sweep.R} and \R{rubin.R}.
\end{itemize}

\end{frame}

\begin{frame}
\chap{Other {\sf C} interfaces in {\sf R}}

There are two other ways to interface between {\sf R} and {\sf C}.
\begin{itemize}
\item \R{.Call()}
\item \R{.External()}
\end{itemize}
They are very similar (and share a documentation file).

\sk They are more sophisticated than \R{.C()} --- more like \R{mex}
\begin{itemize}
\item allowing any {\sf R} {\rd object} to be passed to/from the {\sf C}-side.
\item But cumbersome ``work'' is needed on the {\sf C}-side to manipulate such objects.
\item As a result, the {\sf C} code is {\rd not portable}.
\end{itemize}

\R{.C()} has never been insufficient for my own work.
\end{frame}

\begin{frame}
\nochap

One potential advantage of this framework is that it allows you to 
{\rd call-back} to {\sf R} and execute {\sf R} code.

\sk  This may be the only (clean) way to execute an {\sf R} function from
inside of a {\sf C} program.

\sk Of course, if that {\sf R} function is written in {\sf C} under-the-hood,
\begin{itemize}
\item then it is better (faster/easier) to call the {\sf C} version directly.
\end{itemize}

\sk If not (its entirely in {\sf R}),
\begin{itemize}
\item then perhaps calling slow {\sf R} code defeats the purpose of
writing a {\sf C}-subroutine in the first place.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\chap{{\sf C++}}

\R{.C()} can be used for interfacing to {\sf C++} codes.
\begin{itemize}
\item Mixtures are allowed too.
\end{itemize}

\sk If the {\sf C}-side function that receives the {\sf R} data
\begin{itemize}
\item e.g., {\bl \verb!bootreg_R()!} or {\bl \verb!mutlinks_R()!}
\end{itemize}
is in a {\sf C++} source (\R{.cc} or \R{.cpp}) file compiled by
\R{g++}, say,
\begin{itemize}
\item then it needs to be wrapped in ``{\bl\verb!#extern "C" { ... }!}''
\end{itemize}
\sk This tells the compiler not to ``dress'' the symbol built for the function
with extra {\sf C++} markup.

\sk [See \R{blasso.cc} in the \R{monomvn} package for example.]
\vspace{-0.25cm}
\end{frame}

\begin{frame}
\nochap

\vspace{-0.25cm}
Another option is the \R{Rcpp} package.
\begin{itemize}
\item It is the {\sf C++} version of \R{.Call()}/\R{.External()},
\item allowing {\sf R} objects to be passed to a {\sf C++} program.
\end{itemize}

\sk The added value is that \R{Rcpp} provides dedicated {\sf C++} classes
for all {\sf R} objects.  E.g., 
\begin{itemize}
\item \R{Rcpp::NumericVector},
\item \R{Rcpp::Function},
\item \R{Rcpp::Environment}
\item \R{Rcpp::List}
\item \R{Rcpp::Date}, ...
\end{itemize}

\sk Neat --- but I've yet to find it useful in my own work.

\end{frame}

\begin{frame}
\chap{OpenMP}

Anything you can do in {\sf C}/{\sf Fortran}/{\sf C++} is fair game when building shared objects for use with {\sf R}.

\sk E.g.,
\begin{enumerate}
\item {\tt MPI} or {\tt PVM} for cluster computing
\item {\tt Pthreads} for parallelization
\item STLs for {\sf C++}
\end{enumerate}

\sk As a testament to that, I've decided to illustrate the use of {\tt OpenMP}
inside of a {\sf C}-library for {\sf R}.
\begin{itemize}
\item {\tt OpenMP} is the easiest way to get a {\sf C} program to exploit
multiple computing (SMP) cores.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\nochap

\R{for} loops are the most easily parallelized with \R{OpenMP}.

\sk Simply precede the \R{for} statement with the following {\rd pragma}.
{\bl
\begin{verbatim}
#pragma omp parallel for private(i)
for(i=0; i<n; i++) {
\end{verbatim}
}

This causes each iteration of the \R{for} loop, for each \R{i}, to
(potentially) execute in parallel.
\begin{itemize}
\item It is up to you to ensure that each iteration is independent of others.
\item Sometimes that means changing the \R{for}-loop innards compared to a 
non-parallelized version.
\end{itemize}

\end{frame}

\begin{frame}[fragile]
\nochap

In the \R{mutlinks()} example, I re-coded the \R{for} loop to write components
of the grand sum into an array, slotted for each iteration \R{i} of the loop.

{\bl
\begin{verbatim}
Sa = (int *) malloc(sizeof(int) * n);
#pragma omp parallel for private(i)
for(i=0; i<n; i++) {
   Sa[i] = 0;
   int j,k; /* must be private to i-loop */
   for(j=i+1; j<n; j++)
     for(k=0; k<n; k++) Sa[i] += A[i][k]*A[j][k];
}
S = 0;
for(i=0; i<n; i++) S += Sa[i];
\end{verbatim}
}

\end{frame}

\begin{frame}[fragile]
\nochap

The compiler and linker need a few extra flags,
\begin{itemize}
\item or they won't know how to interpret the {\tt OpenMP} pragmas.
\end{itemize}

\sk Append
{\bl
\begin{verbatim}
PKG_CFLAGS = -fopenmp   
PKG_LIBS = -lgomp   
\end{verbatim}
}
to any existing specifications in your {\bl Makevars} file.

\sk Then compile as usual.
\begin{itemize}
\item I find it helpful to use pre-compiler macros to separate {\bl
\verb!_OPENMP!} implementations from non-parallel ones.
\item Shortcutting the need for new {\sf C}/{\sf R}-interfaces.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\nochap
A comparison on my 8-core iMac, where four cores were dedicated to another task.

{\bl \small
\begin{verbatim}
> A <- matrix(sample(0:1, (40^2)^2, replace=TRUE), 
+             nrow=40^2)
> system.time(aS <- mutlinks(A))
   user  system elapsed 
  9.785   0.012   9.797 
> dyn.load("../src/clect.so")
> system.time(aS <- mutlinks(A))
   user  system elapsed 
 28.093   0.023   6.091 
\end{verbatim}
}
\end{frame}

\begin{frame}
\nochap

A 30\% improvement.
\begin{itemize}
\item {\rd Not impressive!}
\item What is going on?
\end{itemize}

\sk The inner \R{for}-loops, for \R{j} and \R{k}, are very ``tight''.
\begin{itemize}
\item They don't take long to execute,
\item parallel execution or each iteration finishes quickly, passing control
back to the scheduler.
\item Plus, the code is burdened by memory management, and an extra final
\R{for} loop.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.25cm}
The extra \R{for} loop can be eliminated with an \R{atomic} pragma,
\begin{itemize}
\item eliminating an {\rd race condition}, causing the parallel threads to
check that they are ``alone'' in executing the following line,
{\bl
\begin{verbatim}
// #pragma omp atomic
// S += Sa[i];
\end{verbatim}
}
\item so no accumulations are missed.
\end{itemize}

\sk But that doesn't help much, and could defeat the purpose:
\begin{itemize}
\item On a many-core machine, preventing race conditions in this way could
drastically {\em slow} execution.
\end{itemize}
\end{frame}

\begin{frame}
\nochap

A better approach involves {\rd manually} breaking the computation into parallel
{\rd chunks}.

\sk Having fewer chunks
\begin{itemize}
\item ideally one per computing node
\item with roughly equal-sized work for each chucks
\end{itemize}
should minimize time spent coordinating the parallelization
\begin{itemize}
\item maximizing use of multiple cores.
\end{itemize}

\sk
It takes a bit more planning to pull this off,
\begin{itemize}
\item but not necessarily much more code.
\end{itemize}

\end{frame}

\begin{frame}[fragile]
\nochap

First, lets create a subroutine that tallies the sum for each \R{i},
\begin{itemize}
\item encapsulating the inner two, \R{j} and \R{k}, \R{for}-loops.
\end{itemize}

{\bl
\begin{verbatim}
int procpairs(int i, int **A, int n)
{  
  int j, k, S;
  S=0;
  for(j=i+1; j<n; j++)
    for (k = 0; k < n; k++) S += A[i][k]*A[j][k];
  return S; 
}
\end{verbatim}
}
\begin{itemize}
\item Then we can call \R{procpairs()} in batches.
\end{itemize}

\end{frame}

\begin{frame}[fragile]

{\bl \footnotesize
\begin{verbatim}
double mutlinks_omp2(int **A, int n) 
  {
    int tot = 0;
    #pragma omp parallel
    { 
      int i, mysum, me, nth;
      me = omp_get_thread_num();
      nth = omp_get_num_threads();
      mysum = 0;
      for(i=me; i<n; i+=nth) {
        mysum += procpairs(i, A, n);
      }
      #pragma omp atomic
      tot += mysum;
    }
    return((double) tot)/(n*(n-1)/2);
}
\end{verbatim}
}

\end{frame}

\begin{frame}[fragile]
\nochap

We probably could have used pre-compiler macros to make it so that a new
function name, {\bl \verb!mutlinks_omp2()!}, was not needed.
\begin{itemize}
\item But I created a new one so we didn't need to re-compile to make a timing
comparison.
\end{itemize}

\sk I wrote new interface functions too
\begin{itemize}
\item and they look a lot like the old ones.
\end{itemize}

\sk
\hfill [see \R{mutlinks.c} and \R{mutlinks.R}]

\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.5cm}
Now for a final comparison.

\begin{itemize}
\item From before:
{\bl
\begin{verbatim}
> system.time(aS <- mutlinks(A))
   user  system elapsed 
 28.093   0.023   6.091 
> aS
[1] 400.4916
\end{verbatim}
}

\item New chunky version:
{\bl
\begin{verbatim}
> system.time(aS.omp2 <- mutlinks.omp2(A))
   user  system elapsed 
 18.113   0.021   2.669 
 > aS.omp2
[1] 400.4916
\end{verbatim}
}
\end{itemize}
\end{frame}

\begin{frame}
\nochap

\vspace{-0.5cm}
\begin{itemize}
\item more than $2\times$ faster than our first, simple, {\tt OpenMP} version,
\item totaling $\sim4\times$ faster than our non-parallel version.
\end{itemize}

\sk That's pretty good for a machine with $\sim 4$ free computing nodes.
\begin{itemize}
\item You can't hope for better than a {\rd linear speedup}.
\end{itemize}

\sk {\tt OpenMP} has been around for a while, but its my new favorite thing.
\begin{itemize}
\item If you're a {\sf C}-programmer, you need to get familiar with it.
\item Multi-core computing is the modern {\em de facto}
\item and all trends point to ever more cores on a chip.
\end{itemize}
\end{frame}

% \begin{frame}
% \chap{``Homework''}

% Parallelize \R{bootreg()} with {\tt OpenMP}; there is clearly potential:
% \begin{itemize}
% \item Each bootstrap sample is independent from the next,
% \item requiring independent \R{ols()} calculations.
% \end{itemize}

% But
% \begin{itemize}
% \item Lots of temporary space is needed, 
% \item and care is needed in its management in order to get
% an efficient implementation.
% \end{itemize}

% Finally, we have to be wary of race conditions when interfacing
% with the RNG
% \begin{itemize}
% \item We don't want to independent threads to get the same random samples.
% \end{itemize}
% \vspace{-0.25cm}
% \end{frame}

\end{document}

