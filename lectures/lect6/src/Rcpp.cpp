#include <Rcpp.h>
using namespace Rcpp;

// This file contains the Rcpp functions corresponding to the presentation
// R code for test is at the bottom

// [[Rcpp::export]]
NumericVector timesTwo(NumericVector x) {
  return x * 2;
}

// [[Rcpp::export]]
NumericMatrix naive_mat_cpp0(NumericMatrix m, NumericVector v){
  NumericMatrix res(m.nrow(), m.ncol());
  for(int j = 0; j < m.ncol(); j++){
    for(int i = 0; i < m.nrow(); i++){
      res(i, j) = m(i, j) * v(j);
    }
  }
  return(res);
}

// [[Rcpp::export]]
void naive_mat_cpp1(NumericMatrix m, NumericVector v){
  for(int j = 0; j < m.ncol(); j++){
    for(int i = 0; i < m.nrow(); i++){
      
      m(i, j) *= v(j);
    }
  }
}

// [[Rcpp::export]]
void naive_mat_cpp_p(NumericMatrix m, NumericVector v){
  double* ptrm = &m(0,0);
  double* ptrv = &v(0);
  for(int j = 0; j < m.ncol(); j++, ptrv++){
    for(int i = 0; i < m.nrow(); i++, ptrm++){
      *ptrm *= *ptrv;
    }
  }
}

// [[Rcpp::export]]
NumericMatrix invert(NumericMatrix M, Function f){
  return(f(M));
}

// [[Rcpp::export]]
std::vector<int> which_cpp(NumericVector v, double d){
  std::vector<int> res; // empty vector
  for(int i = 0; i < v.length(); i++){
    if(v[i] >= d) res.push_back(i+1);
  }
  return(res);
}

// [[Rcpp::export]]
std::vector<double> which2_cpp(NumericVector v, double d){
  std::vector<double> res; // empty vector
  for(int i = 0; i < v.length(); i++){
    if(v[i] >= d) res.push_back(v[i]);
  }
  return(res);
}

// OpenMP version just add the following line
// [[Rcpp::plugins(openmp)]]
// [[Rcpp::export]]
NumericMatrix naive_mat_cppOMP(NumericMatrix m, NumericVector v){
  int j;
  NumericMatrix m2(m.nrow(), m.ncol());
  
#pragma omp parallel for private(j) // and this line too
  
  for(j = 0; j < m.ncol(); j++){
    for(int i = 0; i < m.nrow(); i++){
      m2(i, j) = m(i, j) * v(j);
    }
  }
  return(m2);
}


// You can include R code blocks in C++ files processed with sourceCpp
// (useful for testing and development). The R code will be automatically 
// run after the compilation.
//

/*** R

### Slide 4
evalCpp("1+1")

### S.7  
library(microbenchmark)

V <- runif(1e7)

microbenchmark(V*0.1, V/10)

### S. 11

## Don't do this
naive_mat <- function(m, v){
  for(i in 1:nrow(m)){
    for(j in 1:ncol(m)){m[i, j] <- m[i, j]/v[j]}
  }
  return(m)
}

### Better
naive_mat2 <- function(m, v){
  for(j in 1:ncol(m)){m[, j] <- m[, j]/v[j]}
  return(m)
}

### S. 12
library(microbenchmark)
m <- matrix(rnorm(1200000), ncol=600)
v <- rep(c(1.5, 3.5, 4.5, 5.5, 6.5, 7.5), length = ncol(m))


microbenchmark(# naive_mat(m,v), ## way too slow
  naive_mat2(m, v),
  t(t(m) * v), 
  m %*% diag(v),
  m * rep(v, rep.int(nrow(m),length(v))), 
  m * rep(v, rep(nrow(m),length(v))), 
  m * rep(v, each = nrow(m)))

### S. 13
microbenchmark(m * rep(v, rep.int(nrow(m),length(v))),
               naive_mat_cpp0(m, v))

### S. 14
print(m[1:3,1:3]); naive_mat_cpp1(m, v); print(m[1:3,1:3])

microbenchmark(m * rep(v, rep.int(nrow(m),length(v))),
               naive_mat_cpp1(m, v))

### S. 15
microbenchmark(m * rep(v, rep.int(nrow(m),length(v))),
               naive_mat_cpp_p(m, v))  

### S. 16
evalCpp("5/3"); evalCpp("5/3.")

### S. 17
v <- runif(1e7)

microbenchmark(which(v >= 0.99999),
               which_cpp(v, 0.99999))

### S. 20
any_naR <- function(x) any(is.na(x))

cppFunction("bool any_naC(NumericVector x) {
  return is_true(any(is_na(x)));
}")

x0 <- runif(1e5); x1 <- c(x0, NA); x2 <- c(NA, x0)

microbenchmark(
  any_naR(x0), any_naC(x0),
  any_naR(x1), any_naC(x1),
  any_naR(x2), any_naC(x2)
)

### S. 24
microbenchmark(naive_mat_cpp0(m, v),
               naive_mat_cppOMP(m, v))

*/
