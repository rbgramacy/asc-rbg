#include <stdlib.h>
#include <stdio.h>
# ifdef _OPENMP
#include <omp.h>
# endif


#ifdef _OPENMP
/*
 * mutlinks:
 *
 * C implementation of the mutlinks function
 * OPENMP parallel for version -- disadvantage is that
 * you can't specify the number of threads
 */

double mutlinks(int **A, int n)
  {
    int i; /* must be signed for openmp */
    unsigned int S;
    int *Sa;

    /* allocate space for S accumulation for each i */
    /* this is necessary to prevent a race condition */
    Sa = (int *) malloc(sizeof(int) * n);

    #pragma omp parallel for private(i)
    for(i=0; i<n; i++) {
      Sa[i] = 0;
      int j,k; /* must be private to i-loop */
      for(j=i+1; j<n; j++)
        for(k=0; k<n; k++) Sa[i] += A[i][k]*A[j][k];
      // #pragma omp atomic
      // S += Sa[i];
    }

    /* collect Sa into S */
    S = 0;
    for(i=0; i<n; i++) S += Sa[i];

    /* clean up */
    free(Sa);

    return(((double) S)/(n*(n-1)/2.0));
  }

#else

/*
 * mutlinks:
 *
 * C implementation of the mutlinks function
 */

double mutlinks(int **A, int n)
  {
      unsigned int i, j, k, S;

      S = 0;
      for(i=0; i<n; i++) 
        for(j=i+1; j<n; j++)
          for(k=0; k<n; k++) S += A[i][k]*A[j][k];

      return(((double) S)/(n*(n-1)/2));
  }

#endif


/*
 * mutlinks_R:
 *
 * R interface to mutlinks function 
 */

void mutlinks_R(int *A_in, int *n_in, double *as_out)
  {
    unsigned int i;
    int **A;

    /* change a vector representation of A into a array one */
    A = (int **) malloc(sizeof(int*) * (*n_in));
    A[0] = A_in;
    for(i=1; i<*n_in; i++) A[i] = A[i-1] + *n_in;

    /* call mutlinks subroutine */
    *as_out = mutlinks(A, *n_in);

    /* clean up */
    free(A);
   }



#ifdef _OPENMP


/*
 * procpairs: 
 *
 * subroutine for mutlinks_omp2 that processes 
 * the paris with mutlinks to node i
 */

int procpairs(int i, int **A, int n)
{  
  int j, k, S;

  S=0;
  for(j=i+1; j<n; j++)
      for (k = 0; k < n; k++) S += A[i][k]*A[j][k];

  return S; 
}

/* 
 * mutlinks_omp2:
 *
 * a fancier mutlinks implementation allowing customization
 * of the number of threads used
 */

double mutlinks_omp2(int **A, int n) 
  {
    int tot;

    /* initialization */
      tot = 0;

    #pragma omp parallel
    { 
      int i, mysum, me, nth;

      /* get thread information */
      me = omp_get_thread_num();
      nth = omp_get_num_threads();
                
      /* in checking all (i,j) pairs, partition the work 
          according to i; this thread me will handle all 
          i that equal me mod nth */
      mysum = 0;
      for(i=me; i<n; i+=nth) {
        mysum += procpairs(i, A, n);
      }
      
      #pragma omp atomic
      tot += mysum;
    }

    return((double) tot)/(n*(n-1)/2);
}

/*
 * mutlinks_R:
 *
 * R interface to mutlinks function 
 */

void mutlinks_omp2_R(int *A_in, int *n_in, double *as_out)
    {
      unsigned int i;
      int **A;

      /* change a vector representation of A into a array one */
      A = (int **) malloc(sizeof(int*) * (*n_in));
      A[0] = A_in;
      for(i=1; i<*n_in; i++) A[i] = A[i-1] + *n_in;

      /* call mutlinks subroutine */
      *as_out = mutlinks_omp2(A, *n_in);

      /* clean up */
      free(A);
    }

#endif

 /*
  Notes for lecture slides:

  Talk about src directory and Makevars
  Talk about -g options in .R/Makefars
  */

