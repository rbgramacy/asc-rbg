\documentclass[12pt,xcolor=svgnames]{beamer}
\usepackage{dsfont,natbib,setspace}
\mode<presentation>

% replaces beamer foot with simple page number
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}{
  \vspace{-1.5cm}
  \raisebox{10pt}{\makebox[\paperwidth]{\hfill\makebox[20pt]{\color{gray}\scriptsize\insertpagenumber}}}}

% colors
\newcommand{\bk}{\color{black}}
\newcommand{\rd}{\color{red}}
\newcommand{\fg}{\color{forestgreen}}
\newcommand{\bl}{\color{blue}}
\newcommand{\gr}{\color{gray}}
\newcommand{\theme}{\color{Maroon}}

% common math markups
\newcommand{\bs}[1]{\boldsymbol{#1}}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mr}[1]{\mathrm{#1}}
\newcommand{\bm}[1]{\mbox{\boldmath $#1$}}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\ds}[1]{\mathds{#1}}

% spacing and style shorthand
\newcommand{\sk}{\vspace{.4cm}}
\newcommand{\nochap}{\vspace{0.6cm}}
\newcommand{\nsk}{\vspace{-.4cm}}
\newcommand{\chap}[1]{{\theme \Large \bf #1} \sk}
\newcommand{\R}[1]{{\bl\tt #1}}
\newcommand{\til}{{\footnotesize$\bs{\stackrel{\sim}{}}$ }}

% specific stats markups for this doc
\newcommand{\E}{\ds{E}}
\newcommand{\Reals}{\ds{R}}
\newcommand{\var}{\text{var}}
\newcommand{\cov}{\text{cov}}
\newcommand{\mT}{\mc{T}}
\newcommand{\GP}{\mc{GP}}
\newcommand{\iidsim}{\stackrel{\mathrm{iid}}{\sim}}
\newcommand{\indsim}{\stackrel{\mathrm{ind}}{\sim}}
\newcommand{\mN}{\mc{N}}

\begin{document}

{ %\usebackgroundtemplate{\includegraphics[height=\paperheight]{phoenix}}
\thispagestyle{empty}
\setcounter{page}{0}

\title{\Large \theme 
\includegraphics[scale=0.1]{../Rlogo}\\
\bf {\sf Code Correctness \& Efficiency}}
\author{\vskip .5cm {\bf Robert B.~Gramacy}\\{\color{Maroon}
    Virginia Tech}
  Department of Statistics\\
  \vskip .2cm \texttt{\rd bobby.gramacy.com}
  \vskip .25cm}
\date{}
\maketitle }

% doc spacing
\setstretch{1.1}

\begin{frame}[fragile]
\nochap 

Most built-in subroutines in {\sf R} make {\rd judicious} checks for
the validity of input, and correctness of output.
\begin{itemize}
\item The better add-on packages do too.
\item This is an under-appreciated feature of a well-designed
  programming language,
\item and an easily overlooked aspect of software engineering, even
  for the simplest of tasks.
\end{itemize}

\sk Compared to other languages, {\sf R} is pretty good in this respect.
\begin{itemize}
\item It can be cryptic with errors, but it is not overly pedantic
  (think object precedence).
\end{itemize}
\vspace{-0.25cm}
\end{frame}

\begin{frame}
\nochap

That said, its built-in {\rd debugger} and {\rd profiler} features
paled in comparison to other languages (e.g., {\sf MATLAB}) until
Rstudio copied them ...

\sk {\sf R} has many features/functions to help you 
\begin{itemize}
\item prevent misuse of your own code (by others)
\item diagnose errors in your own code, or in your use of code written
  by others
\item automate recovery when errors are encountered,
\end{itemize}
and otherwise facilitate the practice of good (conservative) software
engineering.
\end{frame}

\begin{frame}
\chap{Sanity Checks}

Peppering your code with checks, 
\begin{itemize}
\item especially the validity of arguments passed to functions,
\end{itemize}
is {\em \rd so} important.  It should be {\rd habit}, even when
prototyping.
\begin{itemize}
\item It will pay dividends in time saved later.
\item Otherwise its garbage-in garbage-out and none the wiser.
\end{itemize}

\sk Recall the preamble of our \R{bisection()} root-finding 
from lecture 3.
\end{frame}

\begin{frame}[fragile]

{\bl 
\begin{verbatim}
bisection <- function(f, xl, xr, 
   tol=sqrt(.Machine$double.eps), verb=0)
  {
    ## check inputs
    if(xl > xr) stop("must have xl < xr")

    ## setup and check outputs
    fl <- f(xl)
    fr <- f(xr)
    if(fl * fr > 0) stop("f(xl) * f(xr) > 0")
    
\end{verbatim}
}
\vspace{-0.25cm}
\begin{itemize}
\item What if we didn't have those checks?
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.25cm}
The function \R{bisection2()} in the \R{.R} file is the same as our
old \R{bisection()}, but without the checks.  

{\bl \small
\begin{verbatim}
> f <- function(x) log(x) - exp(-x)
> ## results in an error
> fr <- bisection(f, 2, 1)
Error in bisection(f, 2, 1) : must have xl < xr
> ## results in nonsense output
> fr2 <- bisection2(f, 2, 1)
> fr2$ans
[1] 1.5
> ## correct result
> fr3 <- bisection(f, 1, 2)
> fr3$ans
[1] 1.3098
\end{verbatim}
}

\end{frame}

\begin{frame}
\chap{Errors}

\R{stop(...)} signals an {\rd error} with a message to the
screen.

\sk It works like \R{cat(...)}
\begin{itemize}
\item accepting character strings that are pasted together to form the
  error message
\item except that it terminates execution,
  returning control to the calling environment
\item and ultimately back to the user at the command prompt (unless
  {\em caught}).
\end{itemize}

\sk \R{stop()} is one of several {\rd exceptions} in {\sf R}.

\end{frame}

\begin{frame}[fragile]
\nochap

Here is another example that deploys sanity checks/\R{stop()}.

{\bl \footnotesize
\begin{verbatim}
> doWork <- function(filename) {
+ 
+   ## checking the type of file
+   if(class(filename) != "character" ||
+      length(filename) != 1)
+     stop("Filenames should be a single character string")
+ 
+   ## checking that the file exists
+   if(! file.exists(filename)) 
+     stop("Could not open file: ", filename)
+ 
+   ## otherwise do something with the file, e.g.,
+   read.delim(filename)
+ }
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.5cm}
{\bl \footnotesize
\begin{verbatim}
> doWork("this")
Error in doWork("this") : Could not open file: this
> doWork(1)
Error in doWork(1) : Filenames should be a single 
                     character string
\end{verbatim}
}

Of course, \R{read.delim()} has its own error checking:

{\bl \footnotesize
\begin{verbatim}
> read.delim("this")
Error in file(file, "rt") : cannot open the connection
In addition: Warning message:
In file(file, "rt") : cannot open file 'this': 
                      No such file or directory
> read.delim(1)
Error in read.table(file = file, header = header, ... :
  'file' must be a character string or connection
\end{verbatim}
}

\end{frame}

\begin{frame}
\nochap

\vspace{-0.5cm}
It is not uncommon for high-level/interface functions to have 
\begin{itemize}
\item many lines of sanity checks,
\item 
possibly combined with light code that {\em
  massages} input arguments into a convenient form.
\end{itemize}

\sk
{\rd The number of checking lines may be dwarfed by the number of lines of
genuine computation.}
\begin{itemize}
\item Again, garbage-in garbage-out, and authors don't want to be
  blamed for garbage-out.
\item Often the real computation is buried in a subroutine.
\end{itemize}

\sk E.g., see \R{blasso()} or \R{regress()} from the \R{monomvn}
package.
\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.6cm}
\sk Check/conform arguments to be of the right type, and check their
dimensions against those of other arguments.
{\bl \small
\begin{verbatim}
X <- as.matrix(X)
y <- as.numeric(y)
if(length(y) != nrow(X))
    stop("must have nrow(X) == length(y)")
\end{verbatim}
}

\vspace{0.25cm}
 Check length, sign, and class (with flexibility).
{\bl \small\
\begin{verbatim}
if(length(T) != 1 || T <= 1)
    stop("T must be a scalar integer > 1")
if(length(RJ) != 1 || !is.logical(RJ))
    stop("RJ must be a scalar logical")
if(length(lambda2) != 1 || lambda2 < 0)
    stop("lambda2 must be a non-negative scalar")
\end{verbatim}
}

\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.25cm}
Use \R{match.arg()} with switch-style function arguments.

E.g., \R{regress()} has an argument called \R{method}, with ``default'':
{\bl
\begin{verbatim}
method = c("lsr", "plsr", "pcr", "lasso", "lar", 
    "forward.stagewise", "stepwise", "ridge", 
    "factor")
\end{verbatim}
}

This serves two purposes.
\begin{enumerate}
\item Shows the user the options; the first one is the real {\rd default}.
\item Allows \R{match.arg()} to check the argument against a list for
  (partial) matches.
{\bl
\begin{verbatim}
method <- match.arg(method)
\end{verbatim}
}

\end{enumerate}

\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.25cm}
Finally, another option for sanity checking is \R{stopifnot(...)}.  
\begin{itemize}
\item These work like {\tt assert} statements in {\sf C}.
\item They evaluate each statement in \R{...}, and throw an
  {\rd error} if {\em any} evaluate to \R{FALSE}
\end{itemize}

{\bl
\begin{verbatim}
> x <- -2
> stopifnot(x > 0)
Error: x > 0 is not TRUE
\end{verbatim}
}

They are nice as sanity checks in code development, but they are
unhelpful in production code
\begin{itemize}
\item because they offer no error message capability.
\end{itemize}
\end{frame}

\begin{frame}
\chap{Warnings}

Sometimes you want to alert the user to something that might be
troublesome, 
\begin{itemize}
\item but that does not necessarily bar normal execution or the
provision of sensible output.
\end{itemize}

\sk In that case a \R{warning(...)} is appropriate; it works just like
\R{stop(...)} with two exceptions.
\begin{enumerate}
\item They do not stop execution.
\item They are not immediately printed unless the \R{immediate.=TRUE}
  argument is used.  Instead, they join a queue (max 50) for printing
  when control is passed back to the user at the console.
\end{enumerate}

\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.25cm}
We've seen several examples already.

\begin{itemize}
\item \R{if} statements with multiple conditions.
{\bl \footnotesize
\begin{verbatim}
> if(c(TRUE, FALSE)) TRUE else FALSE
[1] TRUE
Warning message:
In if (c(TRUE, FALSE)) TRUE else FALSE :
  the condition has length > 1 and only t
  the first element will be used
\end{verbatim}
}
\item When the names of the columns of a data frame being
  \R{attach()}ed clash with symbols in the current environment.
{\bl \footnotesize
\begin{verbatim}
> x <- 1; attach(data.frame(x=2))
The following object(s) are masked by '.GlobalEnv':
    x
> x
[1] 1
\end{verbatim}
}
\end{itemize}
\hfill
\begin{minipage}{6cm}
\vspace{-1.75cm}
\begin{itemize}
\item not technically a \R{warning()}
\end{itemize}
\end{minipage}
\end{frame}

\begin{frame}[fragile]
\nochap

\R{regress()} gives a warning when an OLS ({\bl \verb!method="lsr"!})
regression fails,
\begin{itemize}
\item e.g., when the matrix \R{X} is not of full rank.
\item In that case, it uses one of the penalized methods instead,
\item so the output it gives still summarizes a reasonable inference.
\end{itemize}

\sk Although the output ``under warning'' is usually sensible, one
must be wary that it is a symptom of something more troubling
\begin{itemize}
\item a bug in input/data preparation
\item or a misunderstanding about appropriate usage.
\end{itemize}

\end{frame}

\begin{frame}
\nochap

When developing my own code
\begin{itemize}
\item peppered with warnings of my own creation,
\item or using add-on packages (peppered with warnings),
\end{itemize}
I like to {\rd elevate} warnings so that they are either 
\begin{itemize}
\item all printed immediately \R{options(warn=1)}, or
\item converted to errors \R{options(warn=2)}.
\end{itemize}

\sk The latter is particularly useful for {\rd postmortem debugging},
\begin{itemize}
\item (in tandem with \R{options(error=recover)}), 
\end{itemize}
to be discussed in more detail shortly.
\end{frame}

\begin{frame}
\nochap

If you're really sure that a warning (in an add-on package) is
innocuous, and 
\begin{itemize}
\item you're tired of seeing it 
\item or don't want your users to see it and become frustrated with
  your code even though its not your warning
\end{itemize}
they can be suppressed with \R{suppressWarnings(expr)}.

\sk
E.g., the \R{regress.pls()} subroutine to \R{regress()} in the
\R{monomvn} package suppresses warnings coming from \R{pcr()} and
\R{plsr()} from the \R{pls} package.

\end{frame}

\begin{frame}
\chap{Catching errors}

Sometimes it makes sense to automate and recover from an error, deep
in a subroutine, without bothering the user.

\sk E.g., maybe you've written code calling a
function from an external library, say, which sometimes trips an error
\begin{itemize}
\item a circumstance for which you have a backup plan.
\end{itemize}

\sk \R{try(\{expr, ...\})} allows you to evaluate any expression and
\begin{itemize}
\item return the usual output if successful (no error)
\item or {\rd catch} any errors before they get to the user, allowing
an alternate execution path
\item which may involve a custom error message or warning.
\end{itemize}
\vspace{-0.25cm}
\end{frame}

\begin{frame}[fragile]
\nochap

Here is how \R{try()} works.

{\bl
\begin{verbatim}
> res <- try({ 1/2 }, silent=TRUE)
> res
[1] 0.5
> res <- try({ 1/"2" }, silent=TRUE)
> res
[1] "Error in 1/\"2\" : non-numeric argument 
     to binary operator\n"
attr(,"class")
[1] "try-error"
attr(,"condition")
<simpleError in 1/"2": non-numeric argument 
     to binary operator>
\end{verbatim}
}

\end{frame}

\begin{frame}[fragile]
\nochap

\R{try()} thus gives us the opportunity of a automating a second
chance.

{\bl
\begin{verbatim}
> res <- try({ 1/"2" }, silent=TRUE)
> if(class(res) == "try-error") {
+   res <- 1/as.numeric("2")
+ }
> res
[1] 0.5
\end{verbatim}
}

\end{frame}

\begin{frame}
\nochap

\sk Just as it is good programming practice to add sanity checks to
your code, signaling informative errors to users,
\begin{itemize}
\item it is equally helpful to protect your users from errors tripped
  by subroutines in code you did {\em not} write.
\end{itemize}

\sk As one example from my own work, see the function \R{glmn.hr()} in
the accompanying \R{.R} file.  
\begin{itemize}
\item It automates a penalized logistic regression via \R{glmnet()},
\item using \R{try()} to guard against errors that can arise due the
  random nature of the choice of $\lambda$ via CV.
\end{itemize}
\end{frame}

\begin{frame}
\chap{Debugging}

Debugging is an essential part of programming, in any language. 
\begin{itemize}
\item It often represents 90\% or more of the effort.
\item I'm skeptical when a first pass at coding leads to (apparently)
  working code.
\end{itemize}

\sk {\rd Write code expecting bugs.}
\begin{itemize}
\item And take advantage of debugging tools;
\item climbing their learning curve is a good investment.
\end{itemize}

\end{frame}

\begin{frame}
\nochap

Most of the {\rd principles} in debugging apply generically.  
\begin{itemize}
\item Start small.
\item Code in a {\rd top-down} manner (routines comprised of
  well-defined, small, sub-routines coded as functions)
\item but test sub-routines exhaustively ({\rd bottom-up} too).
\item Debug exhaustively: once anything looks wrong, check {\rd everything}
systematically.
\end{itemize}

\end{frame}

\begin{frame}
\nochap

\vspace{-0.25cm}
{\rd Sanity checks} are an integral part of the debugging process.  
\begin{itemize}
\item Your task is vastly simplified when one is tripped.
\item If no sanity checks are tripped, and the output is still wrong,
then you don't have enough checks.
\end{itemize}

\sk But sometimes it is not obvious, at first, what to check for.
\begin{itemize}
\item That takes some experience, and more infrastructure.
\item And we'll come back to that later.
\end{itemize}


\sk For now, suppose your code is generating an {\rd error} or
{\rd warning}.  
\begin{itemize}
\item Start by elevating all warnings to errors with \R{options(warn=2)}.
\end{itemize}

\end{frame}

\begin{frame}
\chap{Post-mortem debugging}

Your best tools for diagnosing/fixing {\rd errors} in code are
\begin{itemize}
\item \R{traceback()} after code breaks, immediately after an {\rd
    error}, and
\item \R{options(error=recover)}, to enter the {\rd debugger}
  following an error.
\end{itemize}

\sk Sometimes \R{traceback()} is enough, and its the only option if
you have not, previously, engaged the debugger
\begin{itemize}
\item via \R{debug()} or \R{options(error=recover)}.
\end{itemize}

\end{frame}

\begin{frame}[fragile]
\nochap

These features are best explained via demonstration.

\sk The file \R{mind.R} contains a simple pair of functions which
calculate the minimum value of a (symmetric) matrix \R{d[i,j]}, \R{i
  != j}, and returns the row \R{i} and col \R{j} of that minimum.
\begin{itemize}
\item but the implementation has at least one bug.
\end{itemize}

{\bl
\begin{verbatim}
> source("mind.R") 
> m <- rbind(c(0,12,5), c(12,0,8), c(5,8,0))
> mind(m)
Error in (i + 1):(lx - 1) : argument of length 0
\end{verbatim}
}
\begin{itemize}
\item Not particularly helpful.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\nochap

If we were intimately familiar with the code we might recognize the
offending statement \R{(i + 1):(lx - 1)}.
\begin{itemize}
\item If not, it might be hard to know where to look.
\end{itemize}

{\bl
\begin{verbatim}
> traceback()
4: which.min(x[(i + 1):(lx - 1)]) at mind.R#22
3: FUN(newX[, i], ...) at mind.R#7
2: apply(dd[-n, ], 1, imin) at mind.R#7
1: mind(m)
\end{verbatim}
}

\begin{itemize}
\item So the error was tripped in \R{mind.R} on line 22.
\item But that doesn't necessarily mean that's where the bug is.
\end{itemize}

\end{frame}

\begin{frame}[fragile]
\nochap

A rudimentary diagnostic adds \R{print()s} the code to inspect
the objects before the offending expression.

{\bl
\begin{verbatim}
> imin <- function(x) {
+   lx <- nrow(x); i <- x[lx]
+   print(lx); print(i)
+   j <- which.min(x[(i+1):(lx-1)])
+   return(c(j,x[j]))
+ }
> mind(m)
NULL
numeric(0)
Error in (i + 1):(lx - 1) : argument of length 0
\end{verbatim}
}

\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.25cm}
Now, we could go in and add print statements on \R{x}, etc.
\begin{itemize}
\item We'll get there eventually, but this is not the most direct
  route.
\item We're better off inside the {\rd debugger}.
\end{itemize}

\sk Tell {\sf R} to automatically enter the {\rd debugging}
\R{browser()} upon an error.  {\bl
\begin{verbatim}
> options(error=recover)
\end{verbatim}
}

This augments the information from \R{traceback()} with an
  interactive environment.
\begin{itemize}
\item We can interact, via the command prompt, anywhere in
  the {\rd call stack} maintained at the time of the error.
\end{itemize}

\end{frame}

\begin{frame}[fragile]
\nochap

\nsk\nsk
{\bl
\begin{verbatim}
> mind(m)
Error in (i + 1):(lx - 1) : argument of length 0

Enter a frame number, or 0 to exit   

1: mind(m)
2: mind.R#7: apply(dd[-n, ], 1, imin)
3: mind.R#7: FUN(newX[, i], ...)
4: mind.R#22: which.min(x[(i + 1):(lx - 1)])

Selection: 
\end{verbatim}
}
\begin{itemize}
\item To print \R{x}, \R{i}, or \R{lx}, we want to be inside \R{imin},
\item which is the \R{FUN=} argument to \R{apply()}.
\end{itemize}

\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.5cm}
\begin{itemize}
\item ... so choose 2:
\end{itemize}

{\bl
\begin{verbatim}
Selection: 2
Called from: apply(dd[-n, ], 1, imin)
Browse[1]> x
[1] 1
Browse[1]> class(x)
[1] "numeric"
\end{verbatim}
}

\begin{itemize}
\item The problem is that \R{x} is not a matrix, so \R{lx <- nrow(x)}
  doesn't make any sense.
\item Perhaps we meant \R{lx <- length(x)}?
\end{itemize}

\end{frame}

\begin{frame}[fragile]
\nochap

Press \R{Enter} then \R{0} to leave the \R{browser()}.

{\bl
\begin{verbatim}
Browse[1]> 

Enter a frame number, or 0 to exit   

1: mind(m)
2: mind.R#7: apply(dd[-n, ], 1, imin)
3: mind.R#7: FUN(newX[, i], ...)
4: mind.R#22: which.min(x[(i + 1):(lx - 1)])

Selection: 0
>
\end{verbatim}
}

\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.25cm}
Then fix \R{imin()}.

{\bl
\begin{verbatim}
> imin <- function(x) {
+   lx <- length(x)
+   i <- x[lx]
+   j <- which.min(x[(i+1):(lx-1)])
+   return(c(j,x[j]))
+ }
\end{verbatim}
}
and re-run: \hfill
\begin{minipage}{2cm}
\begin{itemize}
\item Darn!
\end{itemize}
\end{minipage}

{\bl
\begin{verbatim}
> mind(m)
Error in mind(m) : subscript out of bounds
\end{verbatim}
}

\end{frame}

\begin{frame}[fragile]
\nochap

We see that the {\rd error} was tripped in \R{mind()} now, not
\R{imin}
\begin{itemize}
\item although the problem could still be in \R{imin()}.
\item It could be returning a bad index.
\end{itemize}
{\bl
\begin{verbatim}
Enter a frame number, or 0 to exit   

1: mind(m)

Selection: 1
\end{verbatim}
}
\begin{itemize}
\item An easy choice. {\rd :)}
\end{itemize}

\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.5cm}
{\bl
\begin{verbatim}
Browse[1]> where
where 1 at mind.R#14: eval(expr, envir, enclos)
where 2 at mind.R#14: eval(substitute(...
    envir = sys.frame(which))
where 3 at mind.R#14: function () 
..
\end{verbatim}
}
Ok, so the problem occurred on line 14.  That code is:
{\bl
\begin{verbatim}
  return(c(d[i,j],i,j))  
\end{verbatim}
}
\begin{itemize}
\item So we should inspect \R{i}, \R{j}, and \R{d}.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\nochap
{\bl
\begin{verbatim}
Browse[1]> i
[1] 2
Browse[1]> j
[1] 12
Browse[1]> d
     [,1] [,2] [,3]
[1,]    0   12    5
[2,]   12    0    8
[3,]    5    8    0
\end{verbatim}
}
So we're trying to do \R{d[2,12]} but \R{d} is \R{3 x 3}.
\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.5cm}
\R{j} is obtained from \R{wmins} as
{\bl
\begin{verbatim}
i <- which.min(wmins[1,]) 
j <- wmins[2,i]  
\end{verbatim}
}
So the problem must be in \R{wmins},
{\bl
\begin{verbatim}
Browse[1]> wmins
     [,1] [,2]
[1,]    2    1
[2,]   12   12
\end{verbatim}
} whose \R{k}$^{\mathrm{th}}$ row (from the output of \R{imin()} via
\R{apply()}) is supposed to contain information about the minimum
value in row \R{k} of \R{d}.
\end{frame}

\begin{frame}[fragile]
\nochap

\R{wmins} says that the first row (\R{k=1}) of \R{d} is
{\bl
\begin{verbatim}
Browse[1]> d[1,]
[1]  0 12  5
\end{verbatim}
}
has a minimum of \R{12} at index \R{2}
\begin{itemize}
\item but it should be \R{5} at \R{3}.
\end{itemize}

\sk
So something is wrong with this line:
{\bl
\begin{verbatim}
wmins <- apply(dd[-n,],1,imin) 
\end{verbatim}
}

\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.25cm}
There are several possibilities here.

\sk But since ultimately \R{imin()} is called, we can check them all
from within that function.

\begin{itemize}
\item Trouble is, the {\rd error} was not tripped inside that function,
\item so we can't \R{browse()} it in the current debugging session.
\end{itemize}

\sk We need to 
\begin{itemize}
\item quit out and tell the debugger to stop in \R{imin()}
and let us poke around.
{\bl
\begin{verbatim}
Browse[1]> Q    ## shorter than Enter+0
> debug(imin)
\end{verbatim}
}
\item And then restart the code.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\nochap

{\bl
\begin{verbatim}
> mind(m)
debugging in: FUN(newX[, i], ...)
debug at #1: {
    lx <- length(x)
    i <- x[lx]
    j <- which.min(x[(i + 1):(lx - 1)])
    return(c(j, x[j]))
}
Browse[2]> 
\end{verbatim}
}

\begin{itemize}
\item Ok, we're in \R{imin()}.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.25cm}
Lets see if \R{imin()} properly received the first row of \R{dd},

{\bl
\begin{verbatim}
Browse[2]> x
[1]  0 12  5  1
\end{verbatim}
}

\vspace{-0.25cm}
\begin{itemize}
\item Ok, \R{imin()} getting the right arguments.  Stepping thru:
\end{itemize}
\vspace{-0.25cm}

{\bl
\begin{verbatim}
Browse[2]> n
debug at #2: lx <- length(x)
Browse[2]> n
debug at #3: i <- x[lx]
Browse[2]> n
debug at #4: j <- which.min(x[(i + 1):(lx - 1)])
Browse[2]> n
debug at #5: return(c(j, x[j]))
\end{verbatim}
}

\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.75cm}
{\bl
\begin{verbatim}
Browse[4]> lx
[1] 4
Browse[4]> i
[1] 1
Browse[4]> j
[1] 2
Browse[4]> x[(i+1):(lx-1)]
[1] 12  5
\end{verbatim}
}
So \R{j = 2} is indeed the correct index of the minimum of
\R{x[(i+1):(lx-1)]},
but that is not the correct index of the row of \R{dd}.
\begin{itemize}
\item we forgot to add in the row number \R{i}: 
\end{itemize}

\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.5cm}
{\bl
\begin{verbatim}
Browse[2]> Q
> imin <- function(x) {
+   lx <- length(x)
+   i <- x[lx]
+   j <- which.min(x[(i+1):(lx-1)])
+   k <- j + i
+   return(c(k,x[k]))
+ }
> mind(m)
Error in mind(m) : subscript out of bounds
\end{verbatim}
}
\begin{itemize}
\item Oh no!  Another bounds error!
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\nochap 

\vspace{-0.75cm}
{\bl
\begin{verbatim}
Enter a frame number, or 0 to exit   

1: mind(m)

Selection: 1
Called from: top level 
Browse[1]> i
[1] 1
Browse[1]> j
[1] 5
\end{verbatim}
}
\vspace{-.25cm}
\begin{itemize}
\item \R{i} is correct, but the value of \R{j} is still wrong,\\ it should be \R{<= 3}.
\item \R{where} indicates line 14 again.  Argh!
\end{itemize}

\end{frame}

\begin{frame}[fragile]
\nochap

\R{wmins} looks correct
{\bl
\begin{verbatim}
Browse[1]> wmins
     [,1] [,2]
[1,]    3    3
[2,]    5    8
\end{verbatim}
}
\begin{itemize}
\item Ah, but we're using it wrong.
\item The first row has indices and the second has values, and we were
  using them the other way around.
\end{itemize}
{\bl
\begin{verbatim}
i <- which.min(wmins[1,]) 
j <- wmins[2,i]  
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.75cm}
{\bl
\begin{verbatim}
Browse[1]> Q
> mind <- function(d) {
+   n <- nrow(d)
+   dd <- cbind(d,1:n)  
+   wmins <- apply(dd[-n,],1,imin) 
+   i <- which.min(wmins[2,]) 
+   j <- wmins[1,i]
+   return(c(d[i,j],i,j))  
+ }
> mind(m)
[1] 5 1 3
\end{verbatim}
}
\begin{itemize}
\item Golden!
\end{itemize}
\end{frame}

\begin{frame}
\nochap

\vspace{-.25cm}
That's about all there is to it.
\begin{itemize}
\item More of an art than a science, but good tools help.
\item You can set {\rd breakpoints} at particular lines in files too\\ 
(see\R{?~setBreakpoint}).  
\item This calls \R{trace()} on a particular function, so unsetting
  breakpoints involves \R{untrace()}.
\item Some prefer this to \R{debug(f)ing} a whole
  function \R{f()}. 
\end{itemize}

\sk Also see:
\begin{itemize}
\item The \R{debug} package
\item The \R{edtdbg} package which works inside Vim and Emacs.
\item RStudio's debugging enhancements.
\end{itemize}
\end{frame}

\begin{frame}
\chap{Profiling for speed}

If you think your {\sf R} code is running unnecessarily slowly,
\begin{itemize}
\item a handy tool for finding the culprit is \R{Rprof()}.
\item It gives you a report of (approximately) how much time your
  code is spending in each of the functions it calls.
\end{itemize}

\sk This is important, as it may not be wise to optimize {\em every}
section of your program.
\begin{itemize}
\item Optimization may come at the expense of coding time and code
  clarity,
\item so it is of value to know where optimization would reap the
  largest dividends.
\end{itemize}
\vspace{-0.25cm}
\end{frame}

\begin{frame}[fragile]
\nochap

Consider the following code which computes powers of a vector.

{\bl
\begin{verbatim}
> powers1 <- function(x, dg)
+   {
+     pw <- matrix(x, nrow=length(x))
+     prod <- x 
+     for(i in 2:dg) {
+       prod <- prod * x
+       pw <- cbind(pw, prod)
+     }
+     return(pw)
+   }
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap
Timing the whole function is straightforward, but does not provide a
granular report.

{\bl
\begin{verbatim}
> x <- runif(1000000)
> system.time(p1 <- powers1(x, 16))
   user  system elapsed 
  1.021   0.706   1.730 
\end{verbatim}
}

\R{Rprof()} provides more detail.  
\begin{itemize}
\item We have to tell it when to start and stop profiling.
\end{itemize}

{\bl
\begin{verbatim}
> Rprof()
> p1 <- powers1(x, 16)
> Rprof(NULL)
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap 

\vspace{-0.5cm}
\begin{itemize}
\item This creates a temporary file called \R{Rprof.out}, the contents
  of which we will discuss shortly.
\item A summary of that output may be obtained by \R{summaryRprof()}.
\end{itemize}

\vspace{-0.25cm}
{\bl \footnotesize
\begin{verbatim}
> summaryRprof()
$by.self
        self.time self.pct total.time total.pct
"cbind"      1.10     87.3       1.10      87.3
"*"          0.16     12.7       0.16      12.7

$by.total
          total.time total.pct self.time self.pct
"powers1"       1.26     100.0      0.00      0.0
"cbind"         1.10      87.3      1.10     87.3
"*"             0.16      12.7      0.16     12.7
...
\end{verbatim}
}

\end{frame}

\begin{frame}
\nochap

The \R{\$by.self} list sorts functions by the time spent therein, and
therein {\em only}:
\begin{itemize}
\item i.e., not accumulating time spent in other functions called
  therein.
\item \R{\$by.total} shows cumulative timings including callees.
\end{itemize}

\sk Functions which appear near the top of {\em both} lists are cause
for most concern/best targets for extra optimization effort.
\begin{itemize}
\item Roughly, we see that \R{cbind()} is requiring $>5\times$ the
  computing effort of other functions.
\item Remember the importance of {\rd pre-allocation}!
\end{itemize}

\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.25cm}
Here is a new version, with smart {\rd pre-allocation}.

{\bl \footnotesize
\begin{verbatim}
> powers2 <- function(x, dg)
+   {
+     pw <- matrix(nrow=length(x), ncol=dg)
+     prod <- x ## current product
+     pw[,1] <- prod
+     for(i in 2:dg) {
+       prod <- prod * x
+       pw[,i] <- prod
+     }
+   }
> system.time(p2 <- powers2(x, 16))
   user  system elapsed 
  0.390   0.109   0.507 
\end{verbatim}
}
\vspace{-0.4cm}
\begin{itemize}
\item ... and it takes less than one-third of the time.  {\bl (.R file)}
\end{itemize}
\end{frame}

\begin{frame}
\nochap

\nsk
What is \R{Rprof()} doing?

\sk Every 0.02 seconds (the default value), {\sf R}
\begin{itemize}
\item inspects the call stack to determine which function calls are in
  effect at that time.
\item It writes the result of each inspection to a file (\R{Rprof.out}
  by default).
\item \R{summaryRprof()} aggregates those timings (by self and total)
  for each unique function in the \R{Rprof.out} file, displaying
  results for those functions with the largest proportion of times.
\end{itemize}

\sk This type of profiling technique is called {\rd sampling}.
\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.25cm}
For a second, more involved, example consider the code in \R{ar.R}
that provides MCMC samples from the posterior distribution of an
auto-regressive model.
\begin{itemize}
\item The file \R{ar\_test.R} provides a stress test/demo of the
  implemented routines.
\end{itemize}

\sk This illustration will focus on the \R{ar.gibbs()} method, which
requires the following minimal setup.

{\bl 
\begin{verbatim}
> source("ar.R")
> x <- ts(scan("ar3.txt", quiet=TRUE))
> lx <- length(x)
> XKs <- make.XKs(x, 4)
\end{verbatim}
}

\end{frame}

\begin{frame}[fragile]
\nochap
Now for the profiling exercise.

{\bl
\begin{verbatim}
> Rprof("ar3.Rprof")
> theta.3 <- ar.gibbs(1000, x[5:lx], Xk=XKs[[3]])
> Rprof(NULL)
> ar3.Rprof <- summaryRprof("ar3.Rprof")
> ar3.Rprof$by.total[1:10,]
  ## output suppressed
> ar3.Rprof$by.self[1:10,]
  ## output suppressed
\end{verbatim}
}
\vspace{-0.25cm}
\hfill
\begin{minipage}{4cm}
\begin{itemize}
\item (See {\sf R} session)
\end{itemize}
\end{minipage}

\end{frame}

\begin{frame}
\nochap
\R{rmvnorm()} appears highest, {\em jointly}, in both lists.  
\begin{itemize}
\item Some of its subroutines \R{*all.equal*}, \R{is.Symmetric*} also
  appear high in the list(s).
\end{itemize}

\sk Inspecting \R{rmvnorm()}, from the \R{mvtnorm} library, reveals
that these functions are used in a sanity checking capacity.  
\begin{itemize}
\item This is a {\em very} conservative implementation.
\item Checking for symmetry, e.g.,  is $O(n^2)$.
\end{itemize}

\sk If we want to be {\rd cowboys}, and throw caution to the wind, we
might get a faster piece of code with a less conservative
implementation.
\end{frame}

\begin{frame}[fragile]
\nochap

Lets try
{\bl
\begin{verbatim}
> rmvnorm <- function(n,mu,sigma)
+   {
+     p <- length(mu)
+     z <- matrix(rnorm(n * p),nrow=n)
+     ch <- chol(sigma,pivot=T)
+     piv <- attr(ch,"pivot")
+     zz <- (z%*%ch)
+     zzz <- 0*zz
+     zzz[,piv] <- zz
+     zzz + matrix(mu,nrow=n,ncol=p,byrow=T)
+ }
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.35cm}
A new profiling session:
{\bl
\begin{verbatim}
> Rprof("ar3.Rprof2")
> theta.3 <- ar.gibbs(1000, x[5:lx], Xk=XKs[[3]])
> Rprof(NULL)
> ar3.Rprof2 <- summaryRprof("ar3.Rprof2")
\end{verbatim}
}

And a comparison:
{\bl
\begin{verbatim}
> ar3.Rprof$sampling.time
[1] 0.72
> ar3.Rprof2$sampling.time
[1] 0.28
\end{verbatim}
}
\vspace{-0.35cm}
\begin{itemize}
\item Woah! Almost $3\times$ faster!
\item Hard to imagine finding that without \R{Rprof()}.
\end{itemize}
\end{frame}

\begin{frame}
\chap{Profiling for memory usage}

\R{Rprofmem()} does for memory what \R{Rprof()} does for time.

\sk It works exactly the same way.
\begin{enumerate}
\item Call \R{Rprofmem()} with an optional filename.
\item Run code to profile for memory usage.
\item Call \R{Rprofmem(NULL)}.
\item Get a summary with \R{noquote(readLines("Rprofmem.out", n = 5))}.
\end{enumerate}

\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.25cm}
Unfortunately, however, you need to compile a special {\sf R} from
scratch or you'll get an {\rd error message}:
{\bl
\begin{verbatim}
> Rprofmem()
Error in Rprofmem() : memory profiling is not 
   available on this system
\end{verbatim}
}

\sk Also, there is no equivalent to \R{summaryRprof()}.
\begin{itemize}
\item Needless to say, this is not a commonly used technique.
\end{itemize}

\sk Another option, \R{tracemem(x)}, causes a message to be printed to
the screen whenever \R{x} is copied.
\begin{itemize}
\item But it too requires a custom {\sf R} compilation.
\end{itemize}
\end{frame}

% \begin{frame}[fragile]
% \chap{``Homework''}

% Below are two, very compact, versions of functions for calculating
% powers of a matrix.  
% \begin{itemize}
% \item Use \R{Rprof()} to explain why they disappoint relative to
%   \R{powers1()} and \R{powers2()} above.
% \end{itemize}

% {\bl
% \begin{verbatim}
% powers3 <- function(x, dg) outer(x, 1:dg,"^")
% powers4 <- function(x, dg) {
%   repx <- matrix(rep(x, dg), nrow=length(x))
%   return(t(apply(repx, 1, cumprod)))
% }
% \end{verbatim}
% }
% \end{frame}


\end{document}
