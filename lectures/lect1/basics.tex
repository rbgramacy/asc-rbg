\documentclass[12pt,xcolor=svgnames]{beamer}
\usepackage{dsfont,natbib,setspace}
\mode<presentation>

% replaces beamer foot with simple page number
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}{
  \vspace{-1.5cm}
  \raisebox{10pt}{\makebox[\paperwidth]{\hfill\makebox[20pt]{\color{gray}\scriptsize\insertpagenumber}}}}

% colors
\newcommand{\bk}{\color{black}}
\newcommand{\rd}{\color{red}}
\newcommand{\fg}{\color{forestgreen}}
\newcommand{\bl}{\color{blue}}
\newcommand{\gr}{\color{gray}}
\newcommand{\theme}{\color{violet}}

% common math markups
\newcommand{\bs}[1]{\boldsymbol{#1}}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mr}[1]{\mathrm{#1}}
\newcommand{\bm}[1]{\mbox{\boldmath $#1$}}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\ds}[1]{\mathds{#1}}

% spacing and style shorthan
\newcommand{\sk}{\vspace{.4cm}}
\newcommand{\nochap}{\vspace{0.6cm}}
\newcommand{\nsk}{\vspace{-.4cm}}
\newcommand{\chap}[1]{{\theme \Large \bf #1} \sk}
\newcommand{\R}[1]{{\bl\tt #1}}
\newcommand{\til}{{\footnotesize$\bs{\stackrel{\sim}{}}$ }}

% specific stats markups for this doc
\newcommand{\E}{\ds{E}}
\newcommand{\Reals}{\ds{R}}
\newcommand{\var}{\text{var}}
\newcommand{\cov}{\text{cov}}
\newcommand{\mT}{\mc{T}}
\newcommand{\GP}{\mc{GP}}
\newcommand{\iidsim}{\stackrel{\mathrm{iid}}{\sim}}
\newcommand{\indsim}{\stackrel{\mathrm{ind}}{\sim}}
\newcommand{\mN}{\mc{N}}

\begin{document}

{ %\usebackgroundtemplate{\includegraphics[height=\paperheight]{phoenix}}
\thispagestyle{empty}
\setcounter{page}{0}

\title{\Large \theme 
\includegraphics[scale=0.1]{../Rlogo}\\
\bf {\sf Basics and Showcase}}
\author{\vskip .5cm {\bf Robert B.~Gramacy}\\{\color{violet}
    Virginia Tech}
  Department of Statistics\\
  \vskip .2cm \texttt{\rd bobby.gramacy.com}
  \vskip .25cm}
\date{}
\maketitle }

% doc spacing
\setstretch{1.1}

\begin{frame}
\chap{What is {\sf R}?}

{\sf R} is an {\bl open source} {\rd platform} for statistical
computing.

\sk
It offers
\begin{itemize}
\item a general purpose {\em interpreted} programming language
\item a vast library of subroutines, both built-in and contributed
\item and a support community
\end{itemize}
all with an emphasis on data manipulation and statistical inference.

\sk
{\sf R} is maintained by a team of developers around the world.
\end{frame}

\begin{frame}
\nochap

Updated versions of the {\sf R} {\rd core} are released twice a year,
usually with minor improvements, enhancements, and bug fixes.

\begin{itemize}
\item Not much has changed in the last several years.
\end{itemize}

\sk User contributed add-on libraries are updated regularly, with
dozens of additions every month.
\begin{itemize}
\item For many these are what make {\sf R} attractive.
\end{itemize}

\end{frame}

\begin{frame}
\chap{What is {\sf R} most like?}

\sk 
{\sf S}/{\sf S-PLUS}: a commercial software that pre-dates {\sf R}.
\begin{itemize}
\item {\sf R} began as an open source version of {\sf S}, contributed
  to by many of the original creators of {\sf S}.
\item {\sf R} has essentially killed {\sf S}, although its current
  owners (TIBCO) continue to get mileage out of supporting both
  softwares.
\end{itemize}

\end{frame}

\begin{frame}
\nochap

\vspace{-0.5cm}
The modern language that {\sf R} is most {\bl similar} to is {\sf
  MATLAB}.

\sk
{\rd Differences:}
\begin{itemize}
\item {\sf R} is free and open.
\item {\sf R} is object oriented; the {\sf MATLAB} language is more streamlined.
\item {\sf MATLAB} is faster out of the box.
\item The {\sf MATLAB} stats ``toolbox'' is weak.
\item Other, more engineering-oriented {\sf MATLAB} toolboxes, e.g,
  for Signal Processing, are superior.
\item {\sf MATLAB}'s documentation is more professional.
\item {\sf R}'s contributed add-ons are of generally higher quality,
  and are easier to navigate.
\end{itemize}

\end{frame}

\begin{frame}
\nochap

{\sf R} has a {\bl richer feature set}, and is more highly {\bl
  customizable}, than other statistical software suites.  E.g.,
\begin{itemize}
\item {\sf SAS}: great for Big Data
\item {\sf STATA}: popular with economists
\item {\sf Minitab}: what?
\item {\sf Excel}: perfect for making pie charts.
\end{itemize}

\sk It is designed for tinkering, prototyping, simulating, 
high performance computing, and sharing.
\begin{itemize}
\item {\sf R} is a one-stop shop.
\end{itemize}

\end{frame}

\begin{frame}
\chap{{\sf R} is not}

\begin{itemize}
\item as flexible as {\sf Python}, but it is easier to learn
\item a compiled language, but subroutines can be
\item a spreadsheet, but it can interface with Excel
\item a database, although it can interface with them
\item a scripting language like Perl, although you can get by in a
  pinch
\item for application building, although it could be part of a
  back-end or serve as a front-end
\end{itemize}

\end{frame}

\begin{frame}
\chap{Obtaining {\sf R}}

\nsk
\begin{center}
{\bl \url{http://cran.r-project.org}}\\

\includegraphics[scale=0.14,trim=0 300 0 0]{cran}
\end{center}
\end{frame}

\begin{frame}
\nochap

\begin{itemize}
\item {\rd Binaries} for Windows and Mac OSX are available for
  download directly from CRAN.
\item Full {\bl source code} is available for compilation on any
  machine.
\item Many linux distributions (e.g., Ubuntu) provide binaries.
\end{itemize}

\sk There are commercial services that will sell you custom/optimized
binaries
\begin{itemize}
\item e.g., Revolution Analytics/Microsoft R
\end{itemize}
but I think that's snake oil.

\end{frame}

\begin{frame}
\chap{Interfaces}

\begin{itemize}
\item Terminal (DOS/Unix/Linux/OSX); {\bl full-featured}
\end{itemize}

\begin{center}
\includegraphics[scale=0.27,trim=50 50 50 0]{commandline}
\end{center}

This is how I choose to interface with {\sf R}.
\end{frame}

\begin{frame}
\nochap

\nsk
\begin{itemize}
\item Windows GUI
\end{itemize}

\begin{center}
\includegraphics[scale=0.23,trim=50 50 50 0]{windowsGUI}
\end{center}

... not more features than the terminal version, although
the built-in editor, file browser, and menus can at times be helpful.
\end{frame}


\begin{frame}
\nochap

\nsk
It is not a fancy editor, and there is a window within a window.
\begin{center}
\includegraphics[scale=0.3,trim=50 50 50 0]{windowsGUIedit}
\end{center}
\end{frame}

\begin{frame}
\nochap 
\nsk
\begin{itemize}
\item OSX GUI
\end{itemize}

\begin{center}
\includegraphics[scale=0.25,trim=50 60 50 0]{osxGUI}
\hfill
\includegraphics[scale=0.25,trim=50 60 40 0]{osxEditor}
\end{center}

... on par with the Windows one, but without windows within windows,
and the editor does syntax highlighting.
\end{frame}

\begin{frame}

\begin{itemize}
\item Rstudio ({\bl \url{http://www.Rstudio.com}})
\end{itemize}

\begin{center}
\includegraphics[scale=0.2,trim=50 100 50 20]{Rstudio}
\end{center}

\end{frame}

\begin{frame}
\chap{The {\sf R} console}

The {\bl console} is the main way of interacting with {\sf R}.

\sk
It allows you to type commands into R and see how the system responds.

\sk It may seem rudimentary but it is state of the art.  Although
point-and-click has replaced command lines throughout much of
computing (think {\rd DOS}), the console remains the best way to
analyze data.

\sk It is not just an efficient way to interact with a computing
platform.  It makes it easy to keep a record of everything you do so
you can recreate it later if needed.
\end{frame}

\begin{frame}[fragile]
\nochap

\nsk
R is waiting for your input ...

{\bl \tiny
\begin{verbatim}
R version 2.15.2 (2012-04-09 r58957) -- "Trick or Treat"
Copyright (C) 2012 The R Foundation for Statistical Computing
ISBN 3-900051-07-0
Platform: x86_64-apple-darwin11.3.0/x86_64 (64-bit)

R is free software and comes with ABSOLUTELY NO WARRANTY.
You are welcome to redistribute it under certain conditions.
Type 'license()' or 'licence()' for distribution details.

  Natural language support but running in an English locale

R is a collaborative project with many contributors.
Type 'contributors()' for more information and
'citation()' on how to cite R or R packages in publications.

Type 'demo()' for some demos, 'help()' for on-line help, or
'help.start()' for an HTML browser interface to help.
Type 'q()' to quit R.

> 
\end{verbatim}
}
\nsk
... at the {\rd command prompt} ``{\bl \verb!>!}''.

\end{frame}

\begin{frame}[fragile]
\chap{Basic Operations}

When you enter an expression into the {\sf R} console and press {\bl
  Enter}, {\sf R} will evaluate it and display the
results (if any).

\sk At its most basic, {\sf R} can be your calculator.
{\bl 
\begin{verbatim}
> 1 + 2 + 3
[1] 6
> 1 + 2*3
[1] 7
> (1 + 2)*3
[1] 9
\end{verbatim}
}
The output in each case is a \R{[1]}-element vector.
\end{frame}

\begin{frame}[fragile]
\nochap

{\bl Vectors} are first-class citizens in {\sf R}.  They are formed by
\begin{itemize}
\item {\rd c}oncatenation, e.g.,
{\bl 
\begin{verbatim}
> c(0,1,1,2,3,5,8)
[1] 0 1 1 2 3 5 8
\end{verbatim}
}
\item or by sequences, e.g.,
{\bl
\begin{verbatim}
> 1:20
 [1]  1  2  3  4  5  6  7  8  9 10 11 12
[13] 13 14 15 16 17 18 19 20
\end{verbatim}
}
\end{itemize}

\sk
The brackets show the index of the first element of each row.
\end{frame}

\begin{frame}[fragile]
\nochap
When you perform an operation on two vectors, {\sf R} will match their
elements pairwise and return a vector.
{\bl
\begin{verbatim}
> c(1,2,3,4) + c(10,20,30,40)
[1] 11 22 33 44
> c(1,2,3,4) * c(10,20,30,40)
[1]  10  40  90 160
> (1:4) - c(1,1,1,1)
[1] 0 1 2 3
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap

If the vectors aren't the same size, {\sf R} will repeat the smaller
sequence multiple times.

{\bl \footnotesize
\begin{verbatim}
> c(1,2,3,4) + 1
[1] 2 3 4 5
> 1/(1:5)
[1] 1.0000000 0.5000000 0.3333333
[4] 0.2500000 0.2000000
> c(1,2,3,4) + c(10,100)
[1]  11 102  13 104
> c(1:5) + c(10, 100)
[1]  11 102  13 104  15
Warning message:
In c(1:5) + c(10, 100) :
  longer object length is not a multiple of shorter 
  object length
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap

You can also enter expressions with characters:
{\bl
\begin{verbatim}
> "Hello world."
[1] "Hello world."
\end{verbatim}
}
This is called a {\rd character vector} of {\rd length 1}.  

\sk\sk Here is
one of {\rd length 2}:
{\bl
\begin{verbatim}
> c("Hello world", "Hello R interpreter")
[1] "Hello world"        
[2] "Hello R interpreter"
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\chap{Comments}

You can add comments to your {\sf R} code.  Anything after \R{\#} is
ignored:
{\bl
\begin{verbatim}
> ## ... at the beginning of a line
> 1 + 2 + # ... in the middle
+   + 3 
[1] 6
\end{verbatim}
}

\sk Editors may format the comments differently depending on their
multiplicity; {\sf R }doesn't care.
\begin{itemize}
\item Judicious commenting is an integral part of programming.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\chap{Functions}

In {\sf R}, the operations that do all the work are called {\em
  functions}.
\begin{itemize}
\item {\sf R} is said to be a hybrid procedural and {\bl functional}
  programming language.
% \item which means that functions are ``first-class'' objects.
\end{itemize}

\sk Most are of the form
{\bl 
\begin{verbatim}
f(arg1, arg2, ...)
\end{verbatim}
}
where \R{f} is the name of the function, and \R{arg1}, \R{arg2},
... are the arguments,
\begin{itemize}
\item some of which may have default values.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\nochap

A few example functions:
{\bl 
\begin{verbatim}
> exp(1)
[1] 2.718282
> cos(3.141593)
[1] -1
> cos(seq(-pi, pi, 1))
[1] -1.0000000 -0.5403023  0.4161468
[4]  0.9899925  0.6536436 -0.2836622
[7] -0.9601703
> log(1)
[1] 0
> log(exp(1))
[1] 1
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.25cm}
When functions take more than one argument you can specify them by
name
{\bl
\begin{verbatim}
> log(x=64, base=4)
[1] 3
> log(base=4, x=64)
[1] 3
\end{verbatim}
}

\sk
Or, if you give the arguments in the default order, you can omit the
names.
{\bl
\begin{verbatim}
> log(64, 4)
[1] 3
> log(4, 64)
[1] 0.3333333
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap

Not all functions are of the form \R{f(...)}.

\sk Some are {\rd operators}.  For example, for addition we use the
``\R{+}'' operator.
{\bl
\begin{verbatim}
> 17+2
[1] 19
> 2^10
[1] 1024
> 3 == 4
[1] FALSE
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\chap{Variables}

{\sf R} lets you assign values to variables and refer to them by name.
\begin{itemize}
\item Once assigned, the {\sf R} interpreter will substitute that
  value in-place of the variable name when it evaluates an expression.
\end{itemize}

{\bl
\begin{verbatim}
> x <- 1
> y <- 2
> z <- c(x,y)
> z
[1] 1 2
\end{verbatim}
}

\end{frame}

\begin{frame}[fragile]
\nochap

\nsk
The substitution is done at the time that the value is assigned, not
later when it is evaluated in an expression.
{\bl
\begin{verbatim}
> y <- 4
> z
[1] 1 2
\end{verbatim}
}

\begin{itemize}
\item {\sf R} is provides no visual output when assigning, but
{\bl 
\begin{verbatim}
> print(y <- 4)
[1] 4
\end{verbatim}
}
\item You can use \R{=} and \R{->} but I don't recommend it.
{\bl
\begin{verbatim}
> x = 2
> print(c(x,y) -> z)
[1] 2 4
\end{verbatim}
}
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\nochap

Referring to members of vectors:
{\bl
\begin{verbatim}
> b <- (1:12)^2
> b
 [1]   1   4   9  16  25  36  49  64  81
[10] 100 121 144
> b[7]
[1] 49
> b[1:6]
[1]  1  4  9 16 25 36
> b[c(1,11,6)]
[1]   1 121  36
> b[b %% 3 == 0]
[1]   9  36  81 144
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap

Puzzled about a compound expression?  
\begin{itemize}
\item Break it into its constituent
parts.
\end{itemize}
{\bl 
\begin{verbatim}
> b %% 3
 [1] 1 1 0 1 1 0 1 1 0 1 1 0
> print(b30 <- b %% 3 == 0)
 [1] FALSE FALSE  TRUE FALSE FALSE  TRUE
 [7] FALSE FALSE  TRUE FALSE FALSE  TRUE
> b[b30]
[1]   9  36  81 144
\end{verbatim}
} 
Notice how indexing with {\rd logicals} differs from
{\rd integers}.
\end{frame}

\begin{frame}[fragile]
\nochap

Careful with \R{=} and \R{==}.
{\bl
\begin{verbatim}
> one <- 1
> two <- 2
> one = two
> one
[1] 2
> one <- 1
> one == two
[1] FALSE
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\chap{Functions}

A function in {\sf R} is just another object that is assigned to a
symbol.  

\sk
You can make your own functions in {\sf R}, assign them a name, and
then call them like the built-in functions.
{\bl
\begin{verbatim}
> f <- function(x,y) { c(x+1, y+1) }
> f(1, 2)
[1] 2 3
> f
function(x,y) { c(x+1, y+1) }
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\chap{Loops and control}

{\sf R} has a several ways of repeating code, or branching execution upon
condition.

\sk
E.g.,
{\bl
\begin{verbatim}
> fib <- rep(NA, 12)
> fib[1:2] <- 0:1
> for(i in 3:length(fib)) {
+   fib[i] <- fib[i-1] + fib[i-2]
+ }
> fib
 [1]  0  1  1  2  3  5  8 13 21 34 55 89
\end{verbatim}
}
\end{frame}

\begin{frame}
\chap{Data Structures}

You can construct more complicated data structures than just vectors.

\sk
An {\rd array} is a multidimensional vector.  
\begin{itemize}
\item Arrays and vectors are stored (internally) in the same way, but
  an array may be displayed and accessed differently.
\item It is basically a vector that has an additional {\bl dimension}
  attribute.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\nochap

\nsk
{\bl
\begin{verbatim}
> a <- array(c(1,2,3,4,5,6,7,
+              8,9,10,11,12), dim=c(3,4))
> a
     [,1] [,2] [,3] [,4]
[1,]    1    4    7   10
[2,]    2    5    8   11
[3,]    3    6    9   12
\end{verbatim}
}

\sk
Particular cells can be reference via 2-d coordinates:
\begin{itemize}
\item first row, then column
{\bl
\begin{verbatim}
> a[2,3]
[1] 8
\end{verbatim}
}
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\nochap

A {\rd vector} lacks that extra structure.
{\bl 
\begin{verbatim}
> as.vector(a)
 [1]  1  2  3  4  5  6  7  8  9 10 11 12
\end{verbatim}
}

\sk And a {\rd matrix} is just a two-dimensional array.
{\bl
\begin{verbatim}
> m <- matrix(data=c(1,2,3,4,5,6,7,
+              8,9,10,11,12), nrow=3, ncol=4)
> m
     [,1] [,2] [,3] [,4]
[1,]    1    4    7   10
[2,]    2    5    8   11
[3,]    3    6    9   12
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap 

\vspace{-0.5cm}
Arrays can have more than two dimensions.

{\bl
\begin{verbatim}
w <- array(1:12, dim=c(2,3,2))
\end{verbatim}
}

\sk
{\bl
\begin{minipage}{4.5cm}
\begin{verbatim}
> w
, , 1
     [,1] [,2] [,3]
[1,]    1    3    5
[2,]    2    4    6

, , 2
     [,1] [,2] [,3]
[1,]    7    9   11
[2,]    8   10   12
\end{verbatim}
\end{minipage}
\hfill {\rd \vline} \hfill
\begin{minipage}{4.5cm}
\begin{verbatim}
> w[1,3,2]
[1] 11
> w[1,3,]
[1]  5 11
> w[,,2]
     [,1] [,2] [,3]
[1,]    7    9   11
[2,]    8   10   12
\end{verbatim}
\end{minipage}
}
\end{frame}

\begin{frame}[fragile]
\nochap

Arrays/vectors can be subset by other (integer)
arrays/vectors.
\begin{itemize}
\item we just saw a couple of examples
\end{itemize}

{\bl
\begin{verbatim}
> a[1:2,]
     [,1] [,2] [,3] [,4]
[1,]    1    4    7   10
[2,]    2    5    8   11
> a[c(1,3),]
     [,1] [,2] [,3] [,4]
[1,]    1    4    7   10
[2,]    3    6    9   12
\end{verbatim}
}
\end{frame}

\begin{frame}
\chap{List objects}

Vectors, arrays, and matrices are data structures based on a single
underlying (e.g., numeric or character) type.

\sk
The most generic data structure for collecting mixed-type data is a
{\rd list}.

\sk
Entries in a list can be entered and referenced by {\bl name} and/or
by {\rd location}, i.e., by number.
\end{frame}

\begin{frame}[fragile]
\nochap

\nsk
Here is an example of a list with two named components.
{\bl
\begin{verbatim}
> e <- list(thing="hat", size=8.25)
> e
$thing
[1] "hat"

$size
[1] 8.25
\end{verbatim}
}
You can access an item in a list multiple ways.
{\bl
\begin{verbatim}
> e$thing
[1] "hat"
> e[[1]]
[1] "hat"
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap

A list can even contain other lists.
{\bl
\begin{verbatim}
> g <- list("lists within lists", e)
> g
[[1]]
[1] "lists within lists"

[[2]]
[[2]]$thing
[1] "hat"

[[2]]$size
[1] 8.25
\end{verbatim}
}
\end{frame}

\begin{frame}
\chap{Data frames}

A {\rd data frame} is a list that contains multiple named vectors that
are the same length.
\begin{itemize}
\item It is a lot like a spreadsheet or a database table
\item It is stored like a {\rd matrix}, but like a list it allows
  columns to differ in type
\item They are particularly good at representing experimental data.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\nochap
Here is an example list containing win/loss results for baseball teams
in the NL East in 2008:
{\bl
\begin{verbatim}
> teams <- c("PHI", "NYM", "FLA", "ATL", "WSN")
> w <- c(92, 89, 94, 72, 59)
> l <- 162 - w
> nleast <- data.frame(teams, w, l)
> nleast
  teams  w   l
1   PHI 92  70
2   NYM 89  73
3   FLA 94  68
4   ATL 72  90
5   WSN 59 103
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap
You can refer to the components of a data frame by name or by column
number.
{\bl
\begin{verbatim}
>  nleast$w
[1] 92 89 94 72 59
> nleast[,2]
[1] 92 89 94 72 59
\end{verbatim}
}

You can use logical expressions to pick out particular rows:
{\bl
\begin{verbatim}
> nleast[nleast$teams == "FLA",]
  teams  w  l
3   FLA 94 68
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\chap{Objects and classes}

{\sf R} is an {\rd object-oriented} language, and so every object has
a {\bl class}.

{\bl
\begin{verbatim}
> class(teams)
[1] "character"
> class(w)
[1] "numeric"
> class(nleast)
[1] "data.frame"
> class(class)
[1] "function"
\end{verbatim}
}
\end{frame}

\begin{frame}
\nochap

Some functions are associated with a specific class; these are called
{\rd methods}.

\sk
When methods for different classes share the same name they are called
{\rd generic functions}.

\sk Generic functions serve two purposes.
\begin{enumerate}
\item They make it easy to guess the right function for a unfamiliar
  class.
\item They make it possible to use the same code for objects of
  different types.
\end{enumerate}
\end{frame}

\begin{frame}[fragile]
\nochap

\nsk
For example, \R{+} is a generic function for adding objects.  
\begin{itemize}
\item You can add numbers.
{\bl 
\begin{verbatim}
> 17 + 6
[1] 23
\end{verbatim}
}
\item And it probably does something sensible with other objects, e.g.,
  those of the {\rd date class}.
{\bl
\begin{verbatim}
> d <- as.Date("2009-08-08")
> class(d)
[1] "Date"
> d + 7
[1] "2009-08-15"
\end{verbatim}
}
\end{itemize}
\sk
\R{print()} is another good example.
\end{frame}

\begin{frame}[fragile]
\chap{Charts, graphics and summaries}


{\sf R} has many ways to inspect/visualize data.  Consider the
\R{cars} data provided in the base {\sf R} library.

{\bl 
\begin{verbatim}
> cars
   speed dist
1      4    2
2      4   10
3      7    4
...
> dim(cars)
[1] 50  2
> names(cars)
[1] "speed" "dist" 
\end{verbatim}
}
\nsk
\end{frame}

\begin{frame}[fragile]
\nochap

Each of 50 observations records the speed of the car and the distance
required to stop.

\sk The {\rd generic} \R{summary()} function is a useful first
exploratory data analysis (EDA) tool.

{\bl
\begin{verbatim}
> summary(cars)
     speed           dist       
 Min.   : 4.0   Min.   :  2.00  
 1st Qu.:12.0   1st Qu.: 26.00  
 Median :15.0   Median : 36.00  
 Mean   :15.4   Mean   : 42.98  
 3rd Qu.:19.0   3rd Qu.: 56.00  
 Max.   :25.0   Max.   :120.00  
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap
For a visual summary, try a histogram.
{\bl
\begin{verbatim}
> hist(cars$speed, main="")
\end{verbatim}
}

\begin{center}
\includegraphics[scale=0.5, trim=20 0 0 50]{carspeed}
\end{center}
\end{frame}

\begin{frame}[fragile]
\nochap

Scatterplots are useful.
{\bl
\begin{verbatim}
> plot(cars, xlab="Speed (mph)",
+      ylab="Stopping distance (ft)")
\end{verbatim}
}

\begin{center}
\includegraphics[scale=0.5, trim=20 0 0 50]{cars}
\end{center}

\end{frame}

\begin{frame}[fragile]
\chap{Statistical modeling}


There would appear to be a linear relationship between speed and
stopping distance.  Lets check.

{\bl
\begin{verbatim}
> cars.lm <- lm(dist~speed, data=cars)
\end{verbatim}
}

This envokes an OLS fit to
\[
\mathrm{dist}_i = \beta_0 + \beta_1 \mathrm{speed}_i + \varepsilon_i,
\;\;\;
\varepsilon_i \stackrel{\mathrm{iid}}{\sim} \mathcal{N}(0, \sigma^2)
\]
for $i=1,\dots, n$.

\begin{itemize}
\item {\bl \verb!dist~speed!} is a {\rd formula} encoding that model.
\end{itemize}

\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.5cm}
\R{print()} provides a brief summary of the fitted model:

{\bl 
\begin{verbatim}
> cars.lm

Call:
lm(formula = dist ~ speed, data = cars)

Coefficients:
(Intercept)        speed  
    -17.579        3.932  
\end{verbatim}
\begin{itemize}
\item Those coefficients are $\hat{\beta}_0$ and $\hat{\beta}_1$.
\item see \R{print} and \R{stats:::print.lm}
\end{itemize}
}

\vspace{-0.5cm}
\end{frame}

\begin{frame}[fragile]
\nochap

 \R{summary()} provides more info.
{\bl \tiny
\begin{verbatim}
> summary(cars.lm)

Call:
lm(formula = dist ~ speed, data = cars)

Residuals:
    Min      1Q  Median      3Q     Max 
-29.069  -9.525  -2.272   9.215  43.201 

Coefficients:
            Estimate Std. Error t value Pr(>|t|)    
(Intercept) -17.5791     6.7584  -2.601   0.0123 *  
speed         3.9324     0.4155   9.464 1.49e-12 ***
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1 

Residual standard error: 15.38 on 48 degrees of freedom
Multiple R-squared: 0.6511,	Adjusted R-squared: 0.6438 
F-statistic: 89.57 on 1 and 48 DF,  p-value: 1.49e-12 
\end{verbatim}
}

\begin{itemize}
\item see \R{summary} and \R{summary.lm}
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\nochap

Adding the line of best fit is easy, using another generic function.
{\bl
\begin{verbatim}
> abline(cars.lm)
\end{verbatim}
}

\begin{center}
\includegraphics[scale=0.5, trim=20 0 0 50]{carsline}
\end{center}

\end{frame}

\begin{frame}
\nochap

Finally, it helps to understand the full scope of uncertainties.

\begin{center}
\includegraphics[scale=0.5, trim=20 20 0 50]{carspred}
\end{center}

\R{(code in supplement)} \hfill 
Also try \R{plot(cars.lm)}.
\end{frame}

\begin{frame}[fragile]
\chap{Getting Help}

{\sf R} includes a help system to help you get information about the
core language and installed packages.

\sk For help on fitting linear models, type:
{\bl
\begin{verbatim}
> help(lm)
\end{verbatim}
}
or equivalently
{\bl
\begin{verbatim}
> ? lm
\end{verbatim}
}

How the help interface looks depends on your GUI.
\end{frame}

\begin{frame}

\nsk
\begin{center}
\includegraphics[scale=0.35]{lmhelp}
\end{center}
\end{frame}

\begin{frame}
\nochap

That {\sf R}'s help is cryptic is a fair criticism.  
\begin{itemize}
\item But there is some method to the madness.
\end{itemize}

\sk
I start with the examples, which are at the bottom.  
\begin{itemize}
\item You can cut-and-paste, or
\item use \R{example(lm)}.
\end{itemize}

\sk The ``usage'' section is where I look next, to see what the
arguments are and check the defaults.

\sk I read the ``details'' section last or not at all.
\end{frame}

\begin{frame}[fragile]
\nochap

You can search the help system for a topic, which is handy if you
don't know (or have forgotten) the relevant function name.

{\bl 
\begin{verbatim}
> help.search("regression")
\end{verbatim}
}
or equivalently
{\bl
\begin{verbatim}
> ?? regression
\end{verbatim}
}

The search is based on {\em keywords} hidden in the documentation
system and sadly isn't very helpful.
\begin{itemize}
\item You're better off {\rd Googling}.
\end{itemize}
\end{frame}

\begin{frame}
\chap{{\sf R} resources}

Google really is the best place to start.  
\begin{itemize}
\item Often, you end up being directed to {\sf R}-sponsored pages and
  discussion groups.
\end{itemize}

\sk But if you're looking for something more structured or more
polished, you can try:
\begin{itemize}
\item {\bl \url{http://cran.r-project.org/manuals.html}}
\item {\bl \url{http://cran.r-project.org/other-docs.html}}
\end{itemize}
which are linked from the CRAN page.
\end{frame}

\begin{frame}
\chap{{\sf R} packages}

Packages are the life blood of {\sf R}.

\sk A {\rd package} is a related set of functions, help files, and
data files that have all been bundled together.

\sk {\sf R} offers an enormous array of packages
\begin{itemize}
\item displaying graphics, statistical tests,
\item machine learning, signal processing
\item analyzing microarray data, modeling credit risk
\end{itemize}

\sk
Some are included in {\sf R}; others are contributed by the public and
are available online via package repositories.
\end{frame}

\begin{frame}[fragile]
\nochap
To use a package you need to {\em load} it into your current session.

\sk The packages loaded by default are:
{\bl
\begin{verbatim}
> (.packages())
[1] "stats"     "graphics"  "grDevices"
[4] "utils"     "datasets"  "methods"  
[7] "base"  
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap

\nsk
The full set that come with {\sf R} are:
{\bl
\begin{verbatim}
> (.packages(all.available=TRUE))
 [1] "base"       "boot"       "class"     
 [4] "cluster"    "codetools"  "compiler"  
 [7] "datasets"   "foreign"    "graphics"  
[10] "grDevices"  "grid"       "KernSmooth"
[13] "lattice"    "MASS"       "Matrix"    
[16] "methods"    "mgcv"       "nlme"      
[19] "nnet"       "parallel"   "rpart"     
[22] "spatial"    "splines"    "stats"     
[25] "stats4"     "survival"   "tcltk"     
[28] "tools"      "utils"     
\end{verbatim}
}
Also try \R{library()}.
\end{frame}

\begin{frame}[fragile]
\nochap

Packages are loaded with the \R{library()} function, supplying the
name of the desired package as an argument.  {\bl
\begin{verbatim}
> library(rpart)
\end{verbatim}
}

You can use the GUI too.
\begin{itemize}
\item Each works a little differently.
\item A drawback is that you have to remember to re-do the load in
  each new session; whereas the \R{library()} call can be scripted.
\end{itemize}
\end{frame}

\begin{frame}
\nochap

\begin{center}
\includegraphics[scale=0.38,trim=15 0 0 20]{packages}
\end{center}

\end{frame}

\begin{frame}
\chap{Package Repositories}

You will find thousands of {\sf R} packages online.  The two biggest
sources are:
\begin{itemize}
\item CRAN (Comprehensive {\sf R} Archive Network)\\
{\bl \url{http://cran.r-project.org}}
\item Bioconductor, primary for genomic analysis\\
{\bl \url{http://www.bioconductor.org}}
\end{itemize}

\sk {\bl R-Forge} ({\bl \url{http://r-forge.r-project.org}}) is another
interesting place to look for {\sf R} packages
\begin{itemize}
\item but it is more of a collaborative/works in progress site.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\nochap

Packages can be installed in several ways; CRAN packages are the
easiest.

{\bl
\begin{verbatim}
> install.packages("tgp", dependencies=TRUE)
...
\end{verbatim}
}

\sk
Finding packages for your task can be challenging.
\begin{itemize} 
\item There {\em is} an app for that.
\item Use {\rd Google}.
\end{itemize}

\sk The GUIs are also an option. 
\begin{itemize}
\item You only need to install a package once per machine.
\end{itemize}
\end{frame}

\begin{frame}
\nochap

\begin{center}
\includegraphics[scale=0.38,trim=15 0 0 20]{install}
\end{center}

\end{frame}

%\begin{frame}
%\chap{``Homework''}
%
%\begin{itemize}
%\item Write a function returning 
%\[
 %f(x) = 1 - x + 3x^2 - x^3, \;\;\; x \in [-0.5, 2.5].
%\]
%\item Plot the function over that range and note the critical points.
  %Check them against the truth (via calculus).
%\item Use an {\sf R} library function to find those critical points
  %numerically, and check them against the plot/truth.
%\item How many iterations did it take to find each critical point?
%\end{itemize}
%\end{frame}

\end{document}

NEED TO MENTION THE WORKING DIRECTORY SOMEWHERE -- SEE JONES
