\documentclass[12pt,xcolor=svgnames]{beamer}
\usepackage{dsfont,natbib,setspace}
\mode<presentation>

% replaces beamer foot with simple page number
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}{
  \vspace{-1.5cm}
  \raisebox{10pt}{\makebox[\paperwidth]{\hfill\makebox[20pt]{\color{gray}\scriptsize\insertpagenumber}}}}

% colors
\newcommand{\bk}{\color{black}}
\newcommand{\rd}{\color{red}}
\newcommand{\fg}{\color{forestgreen}}
\newcommand{\bl}{\color{blue}}
\newcommand{\gr}{\color{gray}}
\newcommand{\theme}{\color{Maroon}}

% common math markups
\newcommand{\bs}[1]{\boldsymbol{#1}}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mr}[1]{\mathrm{#1}}
\newcommand{\bm}[1]{\mbox{\boldmath $#1$}}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\ds}[1]{\mathds{#1}}

% spacing and style shorthand
\newcommand{\sk}{\vspace{.4cm}}
\newcommand{\nochap}{\vspace{0.6cm}}
\newcommand{\nsk}{\vspace{-.4cm}}
\newcommand{\chap}[1]{{\theme \Large \bf #1} \sk}
\newcommand{\R}[1]{{\bl\tt #1}}
\newcommand{\til}{{\footnotesize$\bs{\stackrel{\sim}{}}$ }}

% specific stats markups for this doc
\newcommand{\E}{\ds{E}}
\newcommand{\Reals}{\ds{R}}
\newcommand{\var}{\text{var}}
\newcommand{\cov}{\text{cov}}
\newcommand{\mT}{\mc{T}}
\newcommand{\GP}{\mc{GP}}
\newcommand{\iidsim}{\stackrel{\mathrm{iid}}{\sim}}
\newcommand{\indsim}{\stackrel{\mathrm{ind}}{\sim}}
\newcommand{\mN}{\mc{N}}

\begin{document}

{ %\usebackgroundtemplate{\includegraphics[height=\paperheight]{phoenix}}
\thispagestyle{empty}
\setcounter{page}{0}

\title{\Large \theme 
\includegraphics[scale=0.1]{../Rlogo}\\
\bf {\sf Working with data}}
\author{\vskip .5cm {\bf Robert B.~Gramacy}\\{\color{Maroon}
    Virginia Tech}
  Department of Statistics\\
  \vskip .2cm \texttt{\rd bobby.gramacy.com}
  \vskip .25cm}
\date{}
\maketitle }


% doc spacing
\setstretch{1.1}

\begin{frame}[fragile]
\chap{Entering Data}

For a small number of observations, entering data directly into R
is an option.  There are a couple of ways to do that.

\sk  As we've seen already, we can create new objects directly on the
console.

{\bl\small
\begin{verbatim}
> salary <- c(18700000, 14626700, 14137500, 13800000)
> position <- c("QB", "QB", "DE", "QB")
> team <- c("Colts", "Patriots", "Panthers")
> last <- c("Manning", "Brady", "Pepper", "Palmer")
> first <- c("Peyton", "Tom", "Julius", "Carson")
\end{verbatim}
}
\vspace{-0.5cm}
\end{frame}

\begin{frame}[fragile]
\nochap

It is usually convenient to put these vectors together into a data
frame.

{\bl
\begin{verbatim}
> top5salaries <- data.frame(last, first, 
+    team, position, salary)
> top5salaries
     last  first     team position   salary
1 Manning Peyton    Colts       QB 18700000
2   Brady    Tom Patriots       QB 14626700
3  Pepper Julius Panthers       DE 14137500
4  Palmer Carson  Bengals       QB 13800000
5 Manning    Eli   Giants       QB 12916666
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap

Entering data using individual statements can be awkward for more than
a handful of observations.

\sk  Luckily {\sf R} provides a GUI for editing tabular data.
\begin{itemize}
\item Use \R{edit()} to open the data editor.

{\bl
\begin{verbatim}
> top5salaries <- edit(top5salaries)
\end{verbatim}
}

\item Notice that you need to assign the output of the \R{edit()}
  function to a symbol, {\rd otherwise the edits will be lost}.
\item It is designed for data frames and matrices.  
\item \R{edit()} will work on other objects, like vectors, functions,
  and lists, but it will open a text editor.
\end{itemize}
\end{frame}

\begin{frame}
\nochap

\vspace{-1cm}
\begin{center}
\includegraphics[scale=0.45]{edit}
\end{center}

\vspace{-1.5cm}
\begin{itemize}
\item The versions on Windows and Unix are similar.
\end{itemize}
\end{frame}

\begin{frame}
\nochap

\begin{itemize}
\item Alternatively, you can use \R{fix()}, which calls \R{edit()} on
  its argument and then assigns the result to the same symbol in the
  calling environment.
\item On Windows, there is a menu item ``Data Editor'' under the Edit
  menu that allows you to enter the name of an object into a dialog
  box.
\begin{itemize}
\item  It then calls \R{fix()} on the object.
\end{itemize}
\end{itemize}

\end{frame}

\begin{frame}
\nochap
\vspace{-0.5cm}
\begin{center}
\includegraphics[scale=0.3]{windowsedit}
\end{center}

\end{frame}

\begin{frame}
\nochap

\vspace{-0.25cm}
The R data editor can be convenient for inspecting a data frame or
matrix, or maybe for editing a couple of values, but {\rd I don't recommend
using it for doing serious work.}

\sk  If you have lots of data to enter, use a real spreadsheet,
desktop database program, or full-featured editor.

\begin{itemize}
\item The {\sf R} data editor doesn't provide an {\tt Undo} or {\tt
    Redo} feature;
\item it doesn't make it easy to save your work (there is no {\tt
    Save} button).  You need to periodically close the editor to save
  your work, which is error prone;
\item ...
\end{itemize}

\end{frame}

\begin{frame}[fragile]
\chap{Saving and Loading}

The simplest way to save an object is with \R{save()}.

{\bl
\begin{verbatim}
> save(top5salaries, file="top5salaries.RData")
\end{verbatim}
}

\vspace{-0.25cm}
\begin{itemize}
\item In {\sf R}, file paths are {\em always} specified with forward
  slashes (``/''), even in Windows.
\item In Windows you'll need {\bl \verb!C:/Documents and Settings/...!}
\item Omitting {\bl \verb!~/!}, or another {\em full path}, results in
  the file being saved to the current working directory (CWD).
{\bl
\begin{verbatim}
> getwd()
[1] "~/work-hg/teaching/Rcourse/R"
\end{verbatim}
}
\vspace{-0.25cm}
\end{itemize}

\end{frame}

\begin{frame}
\nochap 

More about paths:

\begin{itemize}
\item You can change the CWD with \R{setwd()}, and the GUIs have
  pull-down menus, etc., which allow you to browse for the directory
  you want.  
\begin{itemize}
\item In Windows it is in the File menu.
\item In R-Studio it is in the Session menu.
\item In OSX its in the Misc menu.
\end{itemize}
\item In Unixes the CWD will be set as whatever directory you started
  {\sf R} in.
\item The ``.RData'' extension is a convention, but not required.
\end{itemize}

\end{frame}

\begin{frame}
\nochap

\vspace{-0.5cm}
\begin{center}
\includegraphics[scale=0.3]{setwd}
\end{center}

\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.25cm}
More about objects being saved:
\begin{itemize}
\item You can list multiple objects for saving via \R{save().}
\item You can save every object in the workspace with \R{save.image()}.
\end{itemize}

{\bl
\begin{verbatim}
> save.image(file="data.RData")
\end{verbatim}
}

{\sf R} offers to do this for you automatically when you quit via
  \R{q()}.
\begin{itemize}
\item It calls {\bl \verb!save.image(file=".RData")!}, saving the
  current workspace as a hidden file in the CWD.
\item These files have the same structure as those \R{save()}d.
\end{itemize}

\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.25cm}
You can load a saved object back into {\sf R} with \R{load()}.

{\bl
\begin{verbatim}
> load("top5salaries.RData")
\end{verbatim}
\begin{itemize}
\item This causes the saved object(s) to be loaded into the current
  environment, and assigns them the same symbols they had when they were
  saved.
\item {\rd Careful}: any pre-existing symbols pointing to other
  objects will be written over (destroyed) without warning.
\item Files \R{save()}d in {\sf R} will work across platforms when
  \R{load()}ed.
\item When {\sf R} starts up it will automatically \R{load()} the
  \verb!.RData! file in the CWD if there is one.
\end{itemize}
}

\end{frame}

\begin{frame}
\chap{Importing Data from External Files}

One of the nicest things about {\sf R} is how easy it is to pull data
from other programs.

\sk R can import data from
\begin{itemize}
\item text files,
\item other statistics software,
\item or spreadsheets.
\end{itemize}

\sk You don't even need a local copy of the file.
\begin{itemize}
\item You can specify a file at a URL, and {\sf R} will fetch it for
  you over the internet.
\end{itemize}

\end{frame}

\begin{frame}[fragile]
\nochap  

{\sf R} includes a family of functions for importing {\rd delimited text
  files} 
\begin{itemize}
\item where each line contains a {\rd record} of different variables associated
  with each observation, which are
\item separated by a {\rd delimiter}, like ``whitespace'' or commas.
\item Each line/record must have the same number of delimiters (variable).
\end{itemize}

\sk
They are all based on the \R{read.table()} function:
{\bl
\begin{verbatim}
read.table(file, header = FALSE, sep = "", ...)
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap

For example, suppose you had a file called \verb!top5salaries.csv!
containing our salary data, where
\begin{itemize}
\item the first row contains the column names (without quotes);
\item each (non-numeric) string is encapsulated in quotes;
\item and each field is separated by commas.
\end{itemize}

\sk Then we could load this data into {\sf R} as follows.

{\bl
\begin{verbatim}
> top5salaries <- 
+    read.table("top5salaries.csv", header=TRUE,
+               sep=",")
\end{verbatim}
}

\vspace{-0.25cm}
\begin{itemize}
\item The object returned is a data frame.
\end{itemize}

\end{frame}

\begin{frame}[fragile]
\nochap

\R{read.table()} has many different options 
\begin{itemize}
\item see \R{? read.table}
\end{itemize}


\sk The most important are
\begin{itemize}
\item \R{sep=}: giving the delimiter, and
\item \R{header=}: specifying whether or not the first line of the
  file is special.
\end{itemize}

\sk When loading large files you might find after a long wait
that you've misspecified something, like the
\R{sep=} or \R{header=}.
\begin{itemize}
\item A useful technique in this case is to first read a small number
  of rows with \R{nrows=20}.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\nochap

{\sf R} includes a few other functions that call \R{read.table()} in
convenient ways:
\begin{itemize}
\item \R{read.csv()} and \R{read.csv2()} for comma-separated files
  with headers or semi-colon separated ones that might use commas for
  decimals.
\item \R{read.delim()} and \R{read.delim2()} for tab-separated files
  with headers that might use commas for decimals.
\end{itemize}

\sk In most cases you will find that you can use \R{read.csv} and
\R{read.delim} unless you are in Europe.

{\bl
\begin{verbatim}
> top5salaries <- read.csv("top5salaries.csv")
\end{verbatim}
}
\end{frame}

\begin{frame}
\nochap

As another example, suppose that you wanted to analyze some
historical stock quote data. 

\sk
\begin{itemize}
\item Yahoo!~Finance provides this information in an easily downloadable
form on its website.
\item You can fetch a CSV file from a single URL.
\end{itemize}

\sk For example, suppose we want to download price info for the S\&P 500
index for every month between April 1, 2007 and April 1, 2015, stored 
on my web page

\begin{center}
{\bl \footnotesize
\url{
http://rbg.stat.vt.edu/teaching/asc/sp500.csv}}
\end{center}
\end{frame}

\begin{frame}[fragile]
\nochap
Conveniently, you can use a URL in place of a filename in {\sf R}.

{\bl \footnotesize
\begin{verbatim}
> url<-"http://rbg.stat.vt.edu/teaching/asc/sp500.csv"
> sp500 <- read.csv(url)
> sp500[1:5,1:6]
        Date  Open  High   Low Close Adj.Close
1 2007-04-01 26.38 26.39 25.61 25.75     25.75
2 2007-05-01 25.90 26.50 25.85 26.22     26.22
3 2007-06-01 26.25 26.65 25.92 26.07     26.07
4 2007-07-01 26.10 26.25 25.49 25.60     25.60
5 2007-08-01 25.40 25.47 23.25 24.09     24.09
\end{verbatim}
}
\end{frame}

\begin{frame}
\nochap

\vspace{-0.25cm}
There are a couple of other ways to get textual data into {\sf R}
which are handy when the data are not formatted appropriately for
\R{read.table()}.
\begin{itemize}
\item E.g., observations in the file might span multiple lines,
\item or may not be consistently delimited.
\end{itemize}

\sk One example is \R{readLines()}.
\begin{itemize}
\item reading each line as a character string, forming a vector of
  characters.
\item By default it facilitates interactive input from the console.\\
\hfill (see \R{.R} file)
\end{itemize}
\end{frame}

\begin{frame}
\nochap

The \R{scan()} function works similarly, but allows you to read the
contents of a file into a specifically defined data structure using
the \R{what=} argument.

\sk 
I find it most useful in its default configuration
\begin{itemize}
\item \R{what = double(0)}
\end{itemize}
which allows you to read in an (unstructured) vector of
double-precision numbers separated by whitespace.

\end{frame}

\begin{frame}[fragile]
\chap{Exporting data}

The ``inverse'' of \R{read.table()} is \R{write.table()}.
\begin{itemize}
\item Here is how I created that file we read in earlier.
\end{itemize}
{\bl
\begin{verbatim}
> write.table(top5salaries, 
+     file="top5salaries.csv", 
+     sep=",", row.names=FALSE)
\end{verbatim}
}
\begin{itemize}
\item Similarly, there are wrapper functions \R{write.csv()} and
  \R{write.csv2()} which offer a shorthand.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.25cm}
The \R{cat()} function is closest analog to the ``inverse'' of {\rd
  readLines()}
\begin{itemize}
\item allowing you two write arbitrary characters to a file.
\end{itemize}

{\bl
\begin{verbatim}
> x <- c(7, 11, 24, 28)
> cat("my favorite number is", x[1], "\n", 
+     file="favs.txt")
> cat("followed by", x[-1], "\n",
+     file="favs.txt", append=TRUE)
\end{verbatim}
}

\begin{itemize}
\item We've already seen how \R{cat()} can be helpful for printing
messages/progress indicators to the screen.
\end{itemize}

\end{frame}

\begin{frame}[fragile]
The \R{write()} function is deceptively {\rd useless}.
\begin{itemize}
\item It is a wrapper around \R{cat()} which is designed to help write
  matrices to files.
\item But you have to transpose your matrix and specify the number of
  (pre-transposed) columns.
\end{itemize}

{\bl
\begin{verbatim}
> write(t(top5salaries), file="top5salaries.txt",
+       ncolumns=ncol(top5salaries))
\end{verbatim}
}

\begin{itemize}
\item As a result, its both harder to use and less functional than \R{write.table()}.
\end{itemize}
\end{frame}

\begin{frame}
\chap{Objects/data from other softwares}

{\sf R} can read/write data saved in other languages
proprietary/binary formats.

\begin{itemize}
\item The \R{foreign} library supports {\sf Stata}, {\sf SPSS}, {\sf
    SAS}, {\sf Octave}, {\sf Minitab}, {\sf Systat} and several others.
\item The \R{Rmatlab} package can read and write ``MAT'' files.
\item There are dozens which work with Excel in some way and allow
  reading and writing of ``XLS'' files.
\end{itemize}

Most of data-oriented software programs allow data to be saved in a
CSV format, which is often the best way to go.
\end{frame}

\begin{frame}
\chap{Interfacing with databases}

Many large companies, healthcare providers, and academic institutions
keep data in relational databases.

\begin{itemize}
\item E.g., SQL variants, DB2, Oracle, Teradata, Sybase, ...
\end{itemize}

You can always export data from a database to a text file and then
import that into {\sf R}.
\begin{itemize}
\item If you plan to export a large amount of data once and then
  analyze it (once), this is often the best approach.
\item However, if you are using {\sf R} to produce regular reports or
  a repeat analyses, then it might be better to import data into {\sf
    R} directly through a database connection.
\end{itemize}
\end{frame}

\begin{frame}
  \nochap 

\vspace{-0.25cm}
  To connect directly to a database you will need to install a
  package, the best of which depend on which you want to connect to
  and by what method.

\begin{itemize}
\item \R{RODBC}: allows you to fetch data from ODBC (Open DataBase
  Connectivity) connections, which is a standardized interface.
\item \R{DBI}: which allows connections to databases using native
  database drivers or JBC drivers.  The package is an {\em
    abstraction} and requires the installation of additional packages
  to use the native drivers for each database.
\item \R{TSDBI:} specifically designed for time series data.
\end{itemize} 
Third-party {\rd drivers} may be required for particular databases.
\end{frame}

\begin{frame}[fragile]
\nochap

Lets take DBI as a quick example.  DBI is actually a framework and set
of packages.
\begin{itemize}
\item E.g., \R{RSQLite} is a DBI package.
\end{itemize}

{\bl
\begin{verbatim}
> install.packages("RSQLite")
> library(RSQLite)
Loading required package: DBI
\end{verbatim}
}

\sk Consider some baseball data stored in an SQLite database.
{\bl
\begin{verbatim}
con <- dbConnect(RSQLite::SQLite(), dbname="bb.db")
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap

Here are the list of tables available through this database
connection.
{\bl
\begin{verbatim}
> dbListTables(con)
 [1] "Allstar"             "AllstarFull"        
 [3] "Appearances"         "AwardsManagers"     
 [5] "AwardsPlayers"       "AwardsShareManagers"
 [7] "AwardsSharePlayers"  "Batting"            
 [9] "BattingPost"         "Fielding"           
[11] "FieldingOF"          "FieldingPost"       
[13] "HOFold"              "HallOfFame"         
[15] "Managers"            "ManagersHalf"       
...
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap

To find the list of columns for one of the tables, use
\R{dbListFields()}.

{\bl
\begin{verbatim}
> dbListFields(con, "AllStar")
[1] "playerID" "yearID"   "lgID"    
\end{verbatim}
}

Use \R{dbGetQuery()} to fetch a data frame with the results.
\begin{itemize}
\item E.g., the wins and losses for AL teams in 2008.
\end{itemize}

{\bl
\begin{verbatim}
> wlAL08 <- dbGetQuery(con, paste(
+    "SELECT teamID, W, L FROM Teams",
+     "where yearID=2008 and lgID='AL'"))
\end{verbatim}
}

\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.25cm}
Lets extract a couple for later use.

{\bl
\begin{verbatim}
> batting <- dbGetQuery(con, 
+     "SELECT * FROM Batting")
> write.csv(batting, file="batting.csv", 
+     row.names=FALSE)
> master <- dbGetQuery(con, 
+     "SELECT * FROM Master")
> write.csv(master, file="master.csv", 
+     row.names=FALSE)
\end{verbatim}
}

And then close down the connection.
{\bl
\begin{verbatim}
> dbDisconnect(con)
[1] TRUE
\end{verbatim}
}
\end{frame}

\begin{frame}
\chap{Preparing data}

In practice, data is almost never stored in the right form for
analysis.
\begin{itemize}
\item Even when it is, there are can be ``surprises''.
\item It can take a lot of work to pull together a usable data set.
\item {\sf R} has many helpful tools for the task.
\end{itemize}

\sk We'll begin with {\rd combining data} stored
in separate objects.
\begin{itemize}
\item E.g., combining batting stats with player age; player bio data
  is usually stored separately from performance data.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\nochap

... but first some basics.

\sk The simplest combine is a \R{paste()} of two sets of character
strings.

{\bl
\begin{verbatim}
> x <- c("a", "b", "c", "d", "e")
> y <- c("A", "B", "C", "D", "E")
> paste(x, y, sep="-")
[1] "a-A" "b-B" "c-C" "d-D" "e-E"
> paste(x, y, sep="-", collapse=":")
[1] "a-A:b-B:c-C:d-D:e-E"
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.25cm}
\R{rbind()} and \R{cbind()} help bind together multiple data frames,
matrices, or vectors
\begin{itemize}
\item by {\rd r}ow or {\rd c}olumn.
\end{itemize}

\sk Consider adding year and rank info to our salary data.

{\bl \footnotesize
\begin{verbatim}
> year <- rep(2008, 5)
> rank <- 1:5
> morecols <- data.frame(year, rank)
> cbind(top5salaries, morecols)
     last  first     team position   salary year rank
1 Manning Peyton    Colts       QB 18700000 2008    1
2   Brady    Tom Patriots       QB 14626700 2008    2
3  Pepper Julius Panthers       DE 14137500 2008    3
4  Palmer Carson  Bengals       QB 13800000 2008    4
5 Manning    Eli   Giants       QB 12916666 2008    5
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap

Consider adding a few more players. \hfill (see \R{.R} file)
{\bl
\begin{verbatim}
> rbind(top5salaries, next3)
      last  first     team position   salary
1  Manning Peyton    Colts       QB 18700000
2    Brady    Tom Patriots       QB 14626700
3   Pepper Julius Panthers       DE 14137500
4   Palmer Carson  Bengals       QB 13800000
5  Manning    Eli   Giants       QB 12916666
6    Favre   Bret  Packers       QB 12800000
7   Bailey  Champ  Broncos       CB 12600000
8 Harrison Marvin    Colts       WR 12000000
\end{verbatim}
}
\end{frame}

\begin{frame}
\nochap

\vspace{-0.25cm}
Lets extend the {\rd stock quotes example}.

\sk Suppose we wanted a single data set with stock quotes for multiple
securities,
\begin{itemize}
\item e.g., the 30 in the DJIA
\item all bound together into a single data frame.
\end{itemize}

\sk We'll write a functions that 
\begin{itemize}
\item assemble the correct URL to pull quotes for particular stocks
  over the last 365 days,
\item and another that combines them together, iteratively calling the
  first function.
\end{itemize}

\end{frame}

\begin{frame}[fragile]
\nochap
The function \R{get.quotes()} is provided in the accompanying \R{.R}
file.

{\bl \footnotesize
\begin{verbatim}
> get.multiple.quotes <- function(tkrs, 
     from=(Sys.Date()-365), to=(Sys.Date()), interval="d")
+   {
+     tmp <- NULL
+     for (tkr in tkrs) {  
+       if (is.null(tmp))
+         tmp <- get.quotes(tkr,from,to,interval)
+       else tmp <- rbind(tmp,
+               get.quotes(tkr,from,to,interval))
+     }
+     tmp
+   }
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap

{\bl \footnotesize 
\begin{verbatim}
> ## get the tickers
> dow30.tickers <-  ## actually only 29
+   c("MMM", "AXP", "AAPL", "BA", "CAT", "CVX","CSCO", 
+     "KO", "DIS", "XOM", "GE", "GS", "HD", "IBM", 
+     "INTC", "JNJ", "JPM", "MCD", "MRK", "MSFT", "NKE", 
+     "PFE", "PG", "TRV", "UNH", "VZ", "V", "WMT")
> Sys.Date()
[1] "2017-09-20"
> dow30 <- get.multiple.quotes(dow30.tickers)
\end{verbatim}
}

We'll return to this example shortly.
\end{frame}

\begin{frame}[fragile]
\chap{Merging data}

Returning to the baseball data, suppose we wanted to \R{merge()} the
\R{batting} and \R{master} databases to combine performance and bio
data.

{\bl
\begin{verbatim}
> rbind(dim(batting), dim(master))
      [,1] [,2]
[1,] 91457   24
[2,] 17264   33
> both <- merge(batting, master)
> dim(both)
[1] 91457    56
\end{verbatim}
}

\end{frame}

\begin{frame}
\nochap

By default, \R{merge()} uses common variables between the two data
frames as the merge keys.

\begin{itemize}
\item So in this case we did not have to specify any more arguments to
  \R{merge()}.
\item By default, \R{merge()} is equivalent to a NATURAL JOIN in SQL.
\item It can also do an INNER JOIN,
\item an OUTER or FULL join,
\item or the full Cartesian product of the two data sets.
\end{itemize}
\end{frame}

\begin{frame}
\chap{Transformations}

Sometimes there will be some variables in your source data that aren't
quite right.

\sk One of the most convenient ways to redefine a variable in a data
frame is to use the assignment operator.  

\sk For example, suppose we wanted to change the type of a variable in
the \R{dow30} data frame that we created above.
\end{frame}

\begin{frame}[fragile]
  \nochap 

\vspace{-0.25cm}
When \R{read.csv()} imported the data, it interpreted the
  {\bl \verb!$Date!}  field as a character string and converted it
  into a factor

{\bl
\begin{verbatim}
> class(dow30$Date)
[1] "factor"
\end{verbatim}
}

It would be more conveient (e.g., for ordering purposes) to have that
part of the data as a \R{Date} object.

{\bl
\begin{verbatim}
> dow30$Date <- as.Date(dow30$Date)
> class(dow30$Date)
[1] "Date"
\end{verbatim}
}
\vspace{-0.25cm}
\begin{itemize}
\item Luckily, Yahoo!~Finance gives dates in a standard format.
\end{itemize}

\end{frame}

\begin{frame}[fragile]
\nochap

No suppose we wanted to define a new midpoint variable that is the
average of the high and low price.
{\bl
\begin{verbatim}
> dow30$Mid <- (dow30$High + dow30$Low)/2
> names(dow30)
[1] "symbol"    "Date"      "Open"     
[4] "High"      "Low"       "Close"    
[7] "Volume"    "Adj.Close" "Mid"      
\end{verbatim}
}

\end{frame}

\begin{frame}
\nochap

A convenient way of changing variables in a data frame is via
\R{transform()}.

\sk Formally, its defined as
\vspace{0.25cm}
\begin{quote}
\R{transform(}'\_data', \dots\R{)}
\end{quote}

\nsk
i.e., without many {\em named} arguments.   You specify

\begin{itemize}
\item a data frame (as the first argument)
\item and a set of expressions that use variables within the data frame.
\end{itemize}
\R{transform()} applies each expression to the data frame and
then returns the final data frame.

\end{frame}

\begin{frame}[fragile]
\nochap

E.g., suppose we wanted to perform the two transformations we just
did, above.

{\bl
\begin{verbatim}
> dow30t <- get.multiple.quotes(dow30.tickers)
> dow30t <- transform(dow30t, Date=as.Date(Date),
+                     mid=(High+Low)/2)
> names(dow30t)
[1] "symbol"    "Date"      "Open"     
[4] "High"      "Low"       "Close"    
[7] "Volume"    "Adj.Close" "mid"      
> class(dow30t$Date)
[1] "Date"
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]

\chap{Apply}

The \R{apply} family of methods are useful for transforming data too.
We've seen some examples already with
\begin{itemize}
\item \R{sapply()} for vectors;
\item \R{apply()} for arrays and matrices;
\item \R{lapply()} works on vectors, lists, or data frames and returns
  a list;
\item \R{mapply()}: ``multivariate'' version of \R{sapply()};
\item \R{tapply()} and \R{by()} specifically for collapsing info/summarizing.
\end{itemize}

\sk \R{apply()} is the most useful; it even works with $>$ 2d arrays ..

\end{frame}

\begin{frame}[fragile]
\nochap
\vspace{-0.5cm} 
{\bl
\begin{verbatim}
> x <- 1:27
> dim(x) <- c(3,3,3)
> apply(x, 1, paste, collapse=",")
[1] "1,4,7,10,13,16,19,22,25"
[2] "2,5,8,11,14,17,20,23,26"
[3] "3,6,9,12,15,18,21,24,27"
> apply(x, 2, paste, collapse=",")
[1] "1,2,3,10,11,12,19,20,21"
[2] "4,5,6,13,14,15,22,23,24"
[3] "7,8,9,16,17,18,25,26,27"
> apply(x, 3, paste, collapse=",")
[1] "1,2,3,4,5,6,7,8,9"         
[2] "10,11,12,13,14,15,16,17,18"
[3] "19,20,21,22,23,24,25,26,27"
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.25cm}
You can even get complicated and specify the \R{MARGIN=} (the second
argument) to be more than one column.

{\bl
\begin{verbatim}
> apply(x, 1:2, paste, collapse=",")
     [,1]      [,2]      [,3]     
[1,] "1,10,19" "4,13,22" "7,16,25"
[2,] "2,11,20" "5,14,23" "8,17,26"
[3,] "3,12,21" "6,15,24" "9,18,27"
\end{verbatim}
}

\vspace{-0.25cm}
%% This is equivalent to:
\begin{itemize}
\item For each value of $i$ between 1 and 3, 
\item and each value of
  $j$ between 1 and 3,
\item calculate the \R{FUN=} (the function) of
\begin{center}
\verb!x[i][j][1], x[i][j][2], x[i][j][3]!
\end{center}
\end{itemize}

\end{frame}

\begin{frame}
\chap{Binning Data and Subsets}

Another common data transformation is to group a set of observations
into bins based on the value of a specific variable.
\begin{itemize}
\item E.g., converting a daily time series into a monthly one.
\end{itemize}

\sk Many of the functions we've seen already are useful when binning,
\begin{itemize}
\item e.g., \R{transform()}, \R{seq()}, and the bracket notation \R{[]}.
\end{itemize}

\sk We'll look at some new ones too and see them all in action.
\end{frame}

\begin{frame}
\nochap
The \R{cut()} function is useful for taking a continuous variable and
splitting it into discrete pieces.

\vspace{0.25cm}
\begin{quote}
\R{cut(}x, breaks, \dots\R{)}
\end{quote}

%\nsk
\begin{itemize}
\item taking a vector (\R{x=}) as input and returning a
  factor;
\item each level of the factor corresponds to an interval of values in
  the input vector.
\item \R{breaks=} either specifies the number of groups, or gives the
  cut points for the groups.
\end{itemize}

\end{frame}

\begin{frame}[fragile]
\nochap

For example, suppose we wanted to count the number of baseball players
with averages in certain ranges.

{\bl
\begin{verbatim}
> bat08 <- read.csv("../data/bat08.csv")
> bat08 <- transform(bat08, AVG=H/AB)
> bat08.100AB <- subset(bat08, AB>100)
> ## same as bat08[bat08$AB > 100,]
> av08bins <- cut(bat08.100AB$AVG, breaks=10)
\end{verbatim}
}

\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.5cm}
{\bl
\begin{verbatim}
> table(av08bins)
av08bins
(0.137,0.163] (0.163,0.189] (0.189,0.215] 
            4             6            24 
 (0.215,0.24]  (0.24,0.266] (0.266,0.292] 
           67           121           132 
(0.292,0.318] (0.318,0.344]  (0.344,0.37] 
           70            11             5 
 (0.37,0.396] 
            2 
\end{verbatim}
}

\vspace{-0.25cm}
\begin{itemize}
\item Here, \R{table()} is simply counting the number
  of that have each possible categorical value.
\end{itemize}

\end{frame}

\begin{frame}[fragile]
\nochap

As another example of \R{table()} consider using it to count the
number of left and right-handed batters, and switch-hitters.

{\bl
\begin{verbatim}
> table(bat08$bats)
  B   L   R 
118 401 865 
\end{verbatim}
Or, a 2-d table including throwing hand.
{\bl
\begin{verbatim}
> table(bat08[, c("throws", "bats")])
      bats
throws   B   L   R
     L  10 240  25
     R 108 161 840
\end{verbatim}
}
}

\end{frame}

\begin{frame}[fragile]
\nochap

\R{tabulate()} helps {\rd count} the number of {\rd non-categorical
observations} that take on each possible value.


\sk E.g., suppose we wanted a count of the number of players who hit
$0, 1, 2, \dots$ home runs.

{\bl
\begin{verbatim}
> HRcnts <- tabulate(bat08$HR)
> names(HRcnts) <- 0:(length(HRcnts)-1)
> HRcnts
 0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 
92 63 45 20 15 26 23 21 22 15 15 18 12 10 12  4 
16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 
 9  3  3 13  9  7 10  4  8  2  5  2  4  0  1  6 
...
\end{verbatim}
}

\end{frame}


\begin{frame}[fragile]
\nochap

Often it is desirable to take a {\rd random sample} of a data set.
\begin{itemize}
\item Sometimes you have too much data (for statistical or
  performance reasons).
\item Other times you might want to split the data into different
  parts for modeling (e.g., into training, testing and validation sets).
\end{itemize}

\sk  One of the simplest ways to extract a random sample is via
\R{sample()}, which returns a sample of the elements of a vector.

{\bl
\begin{verbatim}
> sample(1:10, 9)
[1] 10  4  1  9  6  8  5  2  3
> sample(1:10, 9, replace=TRUE)
[1] 6 6 7 4 2 8 6 2 8
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap

To take a random sample of the observations in a data frame, 
\begin{itemize}
\item use \R{sample()} to create a random sample of row numbers, 
\item then select these row numbers using an index operator.
\end{itemize}

{\bl
\begin{verbatim}
> bat08[sample(1:nrow(bat08), 5),1:5]
     nameLast nameFirst weight height bats
914   Britton     Chris    278     75    R
753    Maholm      Paul    225     74    L
629 Hennessey      Brad    185     74    R
797    Iguchi  Tadahito    185     70    R
242  Matthews      Gary    210     75    B
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
  \nochap 

\vspace{-0.25cm}
What about more complicated random subsets? E.g., if you
  wanted to randomly select statistics for three teams.

{\bl
\begin{verbatim}
> bat08$teamID <- as.factor(bat08$teamID)
> teams <- levels(bat08$teamID)
> bat08.3t <- bat08[bat08$teamID %in% 
+             sample(teams, 3),]
> summary(bat08.3t$teamID)
ARI ATL BAL BOS CHA CHN CIN CLE COL DET FLO HOU 
  0   0   0  47   0   0   0   0   0   0   0   0 
KCA LAA LAN MIL MIN NYA NYN OAK PHI PIT SDN SEA 
  0   0   0   0   0  51  50   0   0   0   0   0 
SFN SLN TBA TEX TOR WAS 
  0   0   0   0   0   0 
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.25cm}
\R{tapply()} and \R{by()} are useful functions for summarizing the
columns of a matrix or data frame.
\begin{itemize}
\item They work like other \R{apply()} functions.
\end{itemize}

\sk \R{aggregate()} is another, which works on data
frames and time series objects.
\begin{itemize}
\item E.g., to summarize batting stats by team.
\end{itemize}

{\bl
\begin{verbatim}
> cols <- c("AB", "H", "BB", "X2B", "X3B", "HR")
> aggregate(bat08[,cols], by=list(bat08$teamID),
+           sum)
   Group.1   AB    H  BB X2B X3B  HR
1      ARI 5409 1355 587 318  47 159
2      ATL 5604 1514 618 316  33 130
...
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\chap{Finding and removing duplicates}

Duplicated data can sometimes cause problems.
\begin{itemize}
\item {\sf R} provides some useful functions for detecting duplicate
  values.
\end{itemize}

\sk  E.g., suppose you accidentally included one stock ticker twice.
{\bl
\begin{verbatim}
> tickers2 <- c("GE", "GOOG", "AAPL", 
+     "AXP", "GS", "GE")
> quotes2 <- get.multiple.quotes(tickers2, 
+     from=as.Date("2009-01-01"),
+     to=as.Date("2009-03-31"), interval="m")
\end{verbatim}
}
\vspace{-0.5cm}
\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.25cm}
\R{duplicated()} returns a logical vector showing which
elements (entries or rows) are duplicates.

{\bl 
\begin{verbatim}
> duplicated(quotes2)
 [1] FALSE FALSE FALSE FALSE FALSE FALSE FALSE
 [8] FALSE FALSE FALSE FALSE FALSE FALSE FALSE
[15] FALSE  TRUE  TRUE  TRUE
> uquotes2 <- quotes2[!duplicated(quotes2),]
> dim(uquotes2)
[1] 15  8
\end{verbatim}
}
\sk Here is another way to do the same thing.
{\bl
\begin{verbatim}
> uquotes2 <- unique(quotes2)
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\chap{Sorting}

Sorting vectors is straightforward in R.

{\bl
\begin{verbatim}
> w <- c(5,4,7,2,7,1)
> sort(w)
[1] 1 2 4 5 7 7
> sort(w, decreasing=TRUE)
[1] 7 7 5 4 2 1
\end{verbatim}
}
\begin{itemize}
\item By default, any \R{NA} items are not shown.
\item By adjusting the \R{NA.last} argument you can make them appear
  first or last.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\nochap
Sorting a data frame is a little more complicated.  

\begin{itemize}
\item First use \R{order()} to obtain the sorted order of one of the
  indices.
\item Then reorder the rows of the data frame according to that order.
\end{itemize}

\sk \R{order()} works like \R{sort()} but gives you the permutation of
the indices (to sort them) rather than actually sorting them.

{\bl
\begin{verbatim}
> o <- order(w)
> o 
[1] 6 4 2 1 3 5
> w[o]
[1] 1 2 4 5 7 7
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap

Suppose we created the following data frame from the vector \R{w} and
a second vector \R{u}.

{\bl
\begin{verbatim}
> u <- c("pig", "cow", "duck", "horse", "rat", 
+     "moose")
> v <- data.frame(w, u)
> v
  w     u
1 5   pig
2 4   cow
3 7  duck
4 2 horse
5 7   rat
6 1 moose
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap

\vspace{-0.25cm}
We could sort the data frame \R{v} by \R{w} using:
{\bl
\begin{verbatim}
> v[order(v$w),]
  w     u
6 1 moose
4 2 horse
2 4   cow
1 5   pig
3 7  duck
5 7   rat
\end{verbatim}
}
\begin{itemize}
\item You can pass multiple vectors to \R{order()} in order to sort
  via multiple keys.  \hfill (See \R{.R} file for example.)
\end{itemize}
\end{frame}

% \begin{frame}
% \chap{"Homework"}

% Return to the \R{dow30} data from Yahoo!~Finance.

% \begin{itemize}
% \item Calculate {\em returns} $r_{a,t}$ for each asset $a$ based on
%   the mid (average between high and low) price $p_{a,t}$ on each day
%   $t$: $r_{a,t} = (p_{a,t} - p_{a,t-1})/p_{a,t-1}$.
% \item Use these returns to estimate the market (DJIA) return.
% \item Download an index which tracks the market (DJIA) and compare
%   them to the estimate you calculated.
% \item Calculate the correlation of each stock's returns to those of
%   the market (see \R{?~cor}).
% \item Create a new data set where the stocks are presented after
%   sorting on this correlation.
% \end{itemize}
% \end{frame}

\end{document}
