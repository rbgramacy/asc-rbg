## Homework 

You will need to download the `.RData` from the home directory for this home work.

- The purpose of part a is to data clean to a dataframe that is useful for part b and c.

- Create a multiple lineplot using `plotly`.

- Learn to animate the line plot you created in part b and make the animation smoother, i.e animation speed is faster.