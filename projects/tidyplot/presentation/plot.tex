\documentclass[12pt,xcolor=svgnames]{beamer}
\usepackage{dsfont,natbib,setspace}
\usepackage{subfig}

% url for references
\usepackage{hyperref}

\mode<presentation>

\usepackage{graphicx}
\setlength\parindent{0pt}

% replaces beamer foot with simple page number
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}{
  \vspace{-1.5cm}
  \raisebox{10pt}{\makebox[\paperwidth]{\hfill\makebox[20pt]{\color{gray}\scriptsize\insertpagenumber}}}}

% use subitem under item
\newcommand{\SubItem}[1]{
    {\setlength\itemindent{15pt} \item[-] #1}
}

% colors
\newcommand{\bk}{\color{black}}
\newcommand{\rd}{\color{red}}
\newcommand{\fg}{\color{forestgreen}}
\newcommand{\bl}{\color{blue}}
\newcommand{\gr}{\color{gray}}
\newcommand{\theme}{\color{Maroon}}

% common math markups
\newcommand{\bs}[1]{\boldsymbol{#1}}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mr}[1]{\mathrm{#1}}
\newcommand{\bm}[1]{\mbox{\boldmath $#1$}}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\ds}[1]{\mathds{#1}}

% spacing and style shorthand
\newcommand{\sk}{\vspace{.4cm}}
\newcommand{\nochap}{\vspace{0.6cm}}
\newcommand{\nsk}{\vspace{-.4cm}}
\newcommand{\chap}[1]{{\theme \Large \bf #1} \sk}
\newcommand{\R}[1]{{\bl\tt #1}}
\newcommand{\til}{{\footnotesize$\bs{\stackrel{\sim}{}}$ }}

% specific stats markups for this doc
\newcommand{\E}{\ds{E}}
\newcommand{\Reals}{\ds{R}}
\newcommand{\var}{\text{var}}
\newcommand{\cov}{\text{cov}}
\newcommand{\mT}{\mc{T}}
\newcommand{\GP}{\mc{GP}}
\newcommand{\iidsim}{\stackrel{\mathrm{iid}}{\sim}}
\newcommand{\indsim}{\stackrel{\mathrm{ind}}{\sim}}
\newcommand{\mN}{\mc{N}}
%\newcommand\userinput[1]{\textbf{#1}}
\begin{document}
{ 
\thispagestyle{empty}
\setcounter{page}{0}

\title{\Large \theme 
\includegraphics[scale=0.1]{../../Rlogo}\\
\bf {\sf  Data Cleaning and Visualization}}
\author{\vskip .5cm {\bf Lina~Lee and Tsering~Dolkar}\\{\color{Maroon}
    Virginia Tech}
  Department of Statistics\\
  \vskip .25cm}
\date{}
\maketitle }

% doc spacing
\setstretch{1.1}
\begin{frame}[fragile]
\textbf{Overview:}
\begin{itemize}
\item tidyr and dplyr are two popular R packages that are commonly used for data manipulation and transformation tasks.
\item The tidyr package focuses on data tidying, which involves restructuring and organizing data to make it easier to work with and analyze. 
 \item The dplyr package is focused on data manipulation tasks. It provides a set of functions for efficiently and intuitively manipulating data frames.
\end{itemize}
\end{frame}

\begin{frame}[fragile]

\textbf{tidyr: } 
The goal of tidyr is to help you create tidy data. \\
Tidy data is data where:
\begin{itemize}
\item Every column is a variable. 
\item Every row is an observation. 
\item Every cell is a single value. 
\end{itemize}
 When your data is organized in a standard way(tidy data), it makes using analysis tools designed for that setup much easier. 
\end{frame}

\begin{frame}[fragile]
\textbf{hallmark functions in tidyr}
\begin{itemize}
\item pivot\_longer()
\item separate()
\item unite()
\item pivot\_wider()
\end{itemize}
\end{frame}


\begin{frame}[fragile]
 A toy example, wide dataset consist of face1, 2, and 3. the value is response time in seconds for each face.
In this example study, participants were asked to categorize three faces by clicking various buttons that represent three different faces. The time it took to click a button is in milliseconds.
{\bl \footnotesize
\begin{verbatim}
n=10
wide <- data.frame(
  ID = c(1:n),
   face.1 = c(411,723,325,456,579,612,709,513,527,379),
   face.2 = c(123,300,400,500,600,654,789,906,413,567),
   face.3 = c(1457,1000,569,896,956,2345,780,599,1023,678)
)

wide
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\begin{center}
\includegraphics[scale=0.5,trim=30 0 0 10]{../material/wide.png}
\end{center}
\end{frame}

\begin{frame}[fragile]
\textbf{pivot\_longer()} collects multiple columns and converting them into a long format
{\bl \footnotesize
\begin{verbatim}
long <- wide %>% 
	   pivot\_longer(Face, ResponseTime, face.1:face.3)
long
\end{verbatim}
}
\begin{center}
\includegraphics[scale=0.4,trim=30 0 0 10]{../material/long.png}
\end{center}
\end{frame}

\begin{frame}[fragile]
\textbf{separate()} turns a single character column into multiple columns.
{\bl \footnotesize
\begin{verbatim}
long_separate <- long %>%
				 separate(Face, c("Target", "Number"))
long_separate
\end{verbatim}
}
\begin{center}
\includegraphics[scale=0.44,trim=30 0 0 10]{../material/long_separate.png}
\end{center}
\end{frame}

\begin{frame}[fragile]
\textbf{unite()} merge two or more columns into a single column or variable.
{\bl \footnotesize
\begin{verbatim}
long_unite <- long_separate %>% 
					unite(Face, Target, Number, sep = ".")
long_unite
\end{verbatim}
}
\begin{center}
\includegraphics[scale=0.44,trim=30 0 0 10]{../material/long_unite.png}
\end{center}
\end{frame}

\begin{frame}[fragile]

\textbf{pivot\_wider(} "widens" data, increasing the number of columns and decreasing the number of rows. 
{\bl \footnotesize
\begin{verbatim}
back_to_wide <- long_unite %>% pivot_wider(Face, ResponseTime)
back_to_wide
\end{verbatim}
}
\begin{center}
\includegraphics[scale=0.4,trim=30 0 0 10]{../material/back_to_wide.png}
\end{center}
\end{frame}


\begin{frame}[fragile]
\textbf{dplyr: } 
\begin{itemize}
\item It provides simple “verbs”, functions that correspond to the most common data manipulation tasks
\item It uses efficient backends, so you spend less time waiting for the computer.
\item 
{\bl \footnotesize
\begin{verbatim}
%>% \end{verbatim}
} allows you to pipe a value forward into an expression or to function call
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\textbf{hallmark functions in dplyr}
\begin{itemize}
\item group\_by()
\item  summarize()
\item  arrange()
\item mutate()
\item filter()
\end{itemize}
\end{frame}

\begin{frame}[fragile]
 1. Determine which 4 airlines are busiest with ‘other’ which include all other airlines. 
The “busiest” is determined by the number of flights in the dataset. 
{\bl \footnotesize
\begin{verbatim}
top4airlines <- delays %>%
  group_by(OP_UNIQUE_CARRIER) %>%
  summarize(Flights=n() ) %>%
  arrange(desc(Flights)) %>%
  top_n(n=4)
\end{verbatim}
}
 Determine which 5 departing airports have the most busiest.
{\bl \footnotesize
\begin{verbatim}
top5airports <- delays %>%
  group_by(ORIGIN) %>%
  summarize(Flights = n() ) %>%
  arrange(desc(Flights) ) %>%
  top_n(n=5)
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\textbf{groupby()} takes an existing tbl and converts it into a grouped tbl where operations are performed by group.
{\bl \footnotesize
\begin{verbatim}
top4airlines <- delays %>%
  group_by(OP_UNIQUE_CARRIER) 

\end{verbatim}
}

\begin{center}
\includegraphics[scale=0.5,trim=30 0 0 10]{../material/groupby.png}
\end{center}
\begin{center}
\includegraphics[scale=0.5,trim=30 0 0 10]{../material/groupby2.png}
\end{center}
\end{frame}

\begin{frame}[fragile]
 \textbf{summarize()}: summarize the data frame into just one value or vector.
{\bl \footnotesize
\begin{verbatim}
top4airlines  <- delays %>%
  group_by(OP_UNIQUE_CARRIER) %>%
  summarize(Flights=n() ) 
\end{verbatim}
}
\begin{center}
\includegraphics[scale=0.5,trim=30 0 0 10]{../material/summarize.png}
\end{center}
\end{frame}

\begin{frame}[fragile]
\textbf{arrange()}: arrange a column in descending order. 
{\bl \footnotesize
\begin{verbatim}
top4airlines  <- delays %>%
  group_by(OP_UNIQUE_CARRIER) %>%
  summarize(Flights=n() ) %>%
  arrange(desc(Flights)) 
\end{verbatim}
}
\begin{center}
\includegraphics[scale=0.5,trim=30 0 0 10]{../material/arrange.png}
\end{center}
\end{frame}

\begin{frame}[fragile]
 2. Calculate the number of flights for each of the 5 busiest airports and 4 busiest airlines.
{\bl \footnotesize
\begin{verbatim}
busiestAirlinesAirport <- delays %>%
mutate(Carrier = ifelse(OP_UNIQUE_CARRIER %in% 
top4airlines$OP_UNIQUE_CARRIER, OP_UNIQUE_CARRIER, "Other"),
Carrier=factor(Carrier,levels=c(top4airlines$OP_UNIQUE_CARRIER
                     ,"Other"))) %>%
  filter(ORIGIN %in% top5airports$ORIGIN) %>%
  group_by(Carrier, ORIGIN) %>%
  summarize(Flights = n() )
\end{verbatim}
}
\end{frame}



\begin{frame}[fragile]
 \textbf{mutate()}: creates new columns that are functions of existing variables
{\bl \footnotesize
\begin{verbatim}
busiestAirlinesAirport <- delays %>%
mutate(Carrier = ifelse(OP_UNIQUE_CARRIER %in% 
      top4airlines$OP_UNIQUE_CARRIER, 
      OP_UNIQUE_CARRIER, "Other"),
\end{verbatim}
}


In new variable Carrier,  \\
If OP\_UNIQUE\_CARRIER is in top4airlines , \\
put the same value of OP\_UNIQUE\_CARRIER \\ 
If not, put "Other". 
{\bl \footnotesize
\begin{verbatim}
Carrier = ifelse(OP_UNIQUE_CARRIER %in% 
     top4airlines$OP_UNIQUE_CARRIER, OP_UNIQUE_CARRIER,
				  "Other"),
\end{verbatim}
}

\begin{center}
\includegraphics[scale=0.5,trim=30 0 0 10]{../material/mutate.png}
\end{center}

\end{frame}

\begin{frame}[fragile]
\textbf{filter()} retains all rows that satisfy your conditions. Here, we filtered the origin which are in top 5 busiest airport.

{\bl \footnotesize
\begin{verbatim}
busiestAirlinesAirport <- delays %>%
mutate(Carrier = ifelse(OP_UNIQUE_CARRIER %in% 
top4airlines$OP_UNIQUE_CARRIER, OP_UNIQUE_CARRIER, "Other"),
Carrier=factor(Carrier,levels=c(top4airlines$OP_UNIQUE_CARRIER
                     ,"Other"))) %>%
  filter(ORIGIN %in% top5airports$ORIGIN) 
\end{verbatim}
}
\begin{center}
\includegraphics[scale=0.8,trim=30 0 0 10]{../material/filter.png}
\end{center}

\end{frame}


\begin{frame}[fragile]
\begin{center}
\includegraphics[scale=0.5,trim=30 0 0 10]{../material/busiestAirplainAirport.png}
\end{center}
\end{frame}


\begin{frame}[fragile]
\chap{Basics for Beginners}

{\sf R} has at least the following plotting systems :
\begin{itemize}
\item \textbf{Base Plots}: We can build up a plot in stages from a blank canvas.
\item \textbf{Lattice Plots}: A plot is created with a single function call. Once plotted, it cannot be modified later. It’s easy to create subplots.
\item \textbf{ggplot2 Plots}: This systems comes with defaults but one can also customize the plots in many ways. It combines elements of both Base and Lattice plotting systems. This is a popular system with good community support and lots of examples.
\end{itemize}
\end{frame}

\begin{frame}[fragile]
\begin{itemize}
\item \textbf{plotly Plots}: This is both a commercial service and open source product for creating high end interactive visualizations. The plotly package allows you to create plotly interactive graphs from within R. In addition, any ggplot2 graph can be turned into a plotly graph.
\end{itemize}

\sk We can analyze data using different forms of visualization
\begin{itemize}
\item scatterplots, histogram, boxplot, quantile-quantile (Q-Q) plots, mosaic plots, contour plots and etc.
\end{itemize}

You can show {\sf R} graphics on the screen, or save them in many
different formats.

\sk {\rd This lecture scratches the surface.}
\end{frame}


\begin{frame}[fragile]
\chap{ggplot2 Basics}

\R{ggplot2} is based on the grammar of graphics, the idea that you can build every graph from the same components: a \textbf{data} set, a \textbf{coordinate system}, and \textbf{geoms}--visual marks that represent data points.

\begin{center}
\includegraphics[scale=0.5,trim=30 0 0 10]{../material/ggplotbasic.png}
\end{center}
\end{frame}

\begin{frame}[fragile]
\nochap
\vspace{-0.5cm}
\small{
\begin{itemize}
\item \R{Aes} :  common aesthetic values (\R{color}, \R{fill}, \R{linetype}, \R{size}, and \R{shape})
\item \R{Geoms}:  use a geom function to represent data points, use the geom's aesthetic properties to represent variables. Each function returns a layer.
\item  \R{Stats}: an alternative way to build a layer. It builds new variable to plot (e.g, count, prop).
\item  \R{Scales}: override defaults. It maps data values to the visual values of an aesthetic. To change a mapping, add a new scale.
\item Other than above commands, we can use  \textbf{coordinate systems} such as \R{coord\_cartesian(xlim=c(0,5))}, \textbf{position adjustments} ( \R{geombar(position="jitter")}), \textbf{themes} ( \R{theme\_gray()}), \textbf{faceting} ( \R{facet\_wrap(formula)}), \textbf{labels and legends} ( \R{labs()}).
\end{itemize}
}
\end{frame}

\begin{frame}[fragile]
\chap{Scatter plots using Geoms, Stats, labels, and themes:}

We can observe simple scatterplot using \R{ggplot2} in two ways:
\vspace{-0.5cm}
\begin{itemize}
\item Using \R{qplot}, which has a simplified syntax, and
\item \R{ggplot}, which can be saved into a variable and transformed later with further commands.
\end{itemize}

{\bl
\begin{verbatim}
>qplot(x, y, data=dat)
\end{verbatim}
}
\vspace{-0.25cm}
This is the same as doing the following:
\vspace{-0.25cm}
{\bl
\begin{verbatim}
>dat <- data.frame(x=1:10,y=(1:10)^2)
>p <- ggplot(dat, aes(x=x,y=y)) + 
   geom_point(shape=1) 
\end{verbatim}
}
\vspace{-0.5cm}
\end{frame}

\begin{frame}[fragile]
\nochap
... a simple way to visualize functions in the $x$-$y$
plane

{\bl \footnotesize
\begin{verbatim}
>p + geom_line(color = "red", linetype = "dashed", size = 1) +
   stat_function(fun = function(x) x^2, color = "black", 
                  linetype = "solid", aes(group = "curve")) +
   labs(title = "Comparison of lines and curve in ggplot2",
        x = "X-axis Label",
        y = "Y-axis Label") +
   theme()
\end{verbatim}
}

\begin{center}
\includegraphics[scale=0.5,trim=10 5 5 30]{../material/pointslines}
\end{center}

\end{frame}

\begin{frame}[fragile]
\chap{Flight Delay with overflowing legend}

\vspace{-0.25cm}
{\bl \footnotesize
\begin{verbatim}
>b <- ggplot(delays, aes(x= DEP_DELAY, y=ARR_DELAY, 
             col=OP_UNIQUE_CARRIER)) +
          geom_point() +
          labs(x = "Departure Delay", y = "Arrival Delay") + 
          guides(color=guide_legend(ncol=2,byrow=F))
\end{verbatim}
}

\begin{center}
\includegraphics[scale=0.5,trim=20 0 0 20]{../material/delayscatter.png}
\end{center}

\end{frame}

\begin{frame}[fragile]
\chap{Histogram with faceting}

\vspace{-0.25cm}

{\bl \footnotesize
\begin{verbatim}
>orderairlines <- delays %>%
  group_by(OP_UNIQUE_CARRIER) %>%
  summarize(Flights=n() ) %>%
  arrange(desc(Flights)) %>%
  top_n(n=17)

>df1 <- delays[order(match(delays$OP_UNIQUE_CARRIER,
                       orderairlines$OP_UNIQUE_CARRIER)),]
>c <- ggplot(df1, aes(x=DEP_DELAY)) +
           geom_histogram(bins=30) +
           labs(x = "Departure Delay") + 
           guides(fill=guide_legend(ncol=2,byrow=F)) +
           facet_wrap(~factor(OP_UNIQUE_CARRIER, 
                orderairlines$OP_UNIQUE_CARRIER), ncol = 5,
                 scales = "free_y") + 
           theme_bw()
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap
\begin{center}
\includegraphics[scale=0.5,trim=10 0 0 20]{../material/depdelayhist}
\end{center}

\end{frame}

\begin{frame}[fragile]
\chap{Heatmap with ggplot and transforming ggplot to plotly}
{\bl \footnotesize
\begin{verbatim}
busiestAirlinesAirport <- delays %>%
  mutate(Carrier = ifelse(OP_UNIQUE_CARRIER %in% 
     top5airlines$OP_UNIQUE_CARRIER,
      OP_UNIQUE_CARRIER, "Other"),
     Carrier = factor(Carrier, 
       levels=c(top5airlines$OP_UNIQUE_CARRIER, "Other"))) %>%
  filter(ORIGIN %in% top5airports$ORIGIN) %>%
  group_by(Carrier, ORIGIN) %>%
  summarize(Flights = n() )

e <- ggplot(busiestAirlinesAirport, 
        aes(ORIGIN, Carrier, fill= Flights)) + 
  geom_tile()
ggplotly(e, tooltip="text")
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\begin{figure}
    \centering
    \subfloat[\centering ggplot]{{\includegraphics[width=5cm]{../material/ggheat} }}%
    \qquad
    \subfloat[\centering ggplotly]{{\includegraphics[width=5cm]{../material/plotlyheat.png} }}%
    \caption{2 ways of studying heatmap}%
    \label{fig:example}%
\end{figure}
\end{frame}

\begin{frame}[fragile]
\chap{plotly Basics}

\R{plotly} is a free and open source graphing library for \sk{R}.  
{\bl \footnotesize
\begin{verbatim}
trace_0 <- rnorm(100, mean = 5)
trace_1 <- rnorm(100, mean = 0)
trace_2 <- rnorm(100, mean = -5)
x <- c(1:100)

data <- data.frame(x, trace_0, trace_1, trace_2)
fig <- plot_ly(data, x = ~x, y = ~trace_0, name = 'trace 0',
          type = 'scatter', mode = 'lines')
fig <- fig %>% add_trace(y = ~trace_1, name = 'trace 1',
           mode = 'lines+markers')
fig <- fig %>% add_trace(y = ~trace_2, name = 'trace 2',
            mode = 'markers')
fig
\end{verbatim}
}
\end{frame}
\begin{frame}[fragile]
\nochap
... then use layout to rename xlab, ylab, and title of the plot.
{\bl \footnotesize
\begin{verbatim}
fig <- fig %>%
  layout(xaxis = list(title = "xlab"),
         yaxis = list(title = "traces"),
         showlegend = TRUE)

fig
\end{verbatim}
}

\begin{center}
\includegraphics[scale=0.15, trim=10 0 0 30]{../material/lineplotlynoanime.png}
\end{center}
\end{frame}


\begin{frame}[fragile]
\chap{Choropleth Map}

We will consider choropleth maps for today. Plotly comes with two built-in geometries which do not require external file.
\begin{itemize}
\item USA States
\item Countries as defined in the Natural Earth dataset.
\end{itemize}
{\bl \footnotesize
\begin{verbatim}
>us_delay_summary <- delays %>%
  group_by(ORIGIN_CITY_NAME) %>%
  summarize(Avg.Delay = mean(DEP_DELAY, na.rm=TRUE)) 
>us_delay_summary<-us_delay_summary%>%
  mutate(ORIGIN_STATE_ABR=
       str_extract(ORIGIN_CITY_NAME, "\\b[A-Z]{2}"))
>df <- us_delay_summary
\end{verbatim}
}
\end{frame}

\begin{frame}[fragile]
\nochap

{\bl \footnotesize
\begin{verbatim}
>e <- plot_ly(df,
  type = 'choropleth',
  locations = ~ORIGIN_STATE_ABR, locationmode = 'USA-states', 
  color = ~Avg.Delay, colors = 'Purples',
  z = ~Avg.Delay, text=df$ORIGIN_CITY_NAME)
\end{verbatim}
}
\begin{center}
\includegraphics[scale=0.15,trim=10 0 0 30]{../material/choroplotly1.png}
\end{center}
\end{frame}

\begin{frame}[fragile]
\nochap
{\bl \footnotesize
\begin{verbatim}
># specify some map projection/options
g.eo <- list(
  scope = 'usa',
  projection = list(type = 'albers usa'),
  showlakes = TRUE,
  lakecolor = toRGB('white')
)
>e <- e %>% layout(
  title = 'flight departure and arrival delay from each state',
  geo = g.eo)
\end{verbatim}
}
\begin{center}
\includegraphics[scale=0.15, trim=10 0 0 30]{../material/choroplotly2.png}
\end{center}
\end{frame}


\begin{frame}[fragile]
\chap{References}
\begin{itemize}
\item { \footnotesize \url {https://tidyr.tidyverse.org/}}
\item { \footnotesize \url {https://dplyr.tidyverse.org/}}
\item { \footnotesize \url {https://plotly.com/r/line-and-scatter/#mapping-data-to-symbols} }
\item { \footnotesize \url {https://plotly.com/r/choropleth-maps/} }
\item { \footnotesize  \url {https://images.plot.ly/plotly-documentation/images/r_cheat_sheet.pdf} }
\end{itemize}
\end{frame}
\end{document}
