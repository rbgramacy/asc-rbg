\documentclass[12pt,xcolor=svgnames]{article}

\usepackage[xcolor=svgnames,dvipsnames]{xcolor}
\usepackage{amsmath, amsfonts, amssymb, mathrsfs}
\usepackage{dsfont}
\usepackage{mathtools}
\usepackage{lscape}
\usepackage{wrapfig}
 \graphicspath{ {./images/} }
 \usepackage{graphicx}
\usepackage{float}
\usepackage{subfig}
\usepackage{multirow}
\usepackage{listings}
\usepackage{hyperref}
\usepackage[margin=0.5in]{geometry} 
\usepackage{setspace}
%\usepackage{hangcaption}
\setlength\parindent{0pt}

\usepackage{dcolumn}
\usepackage{natbib}
\usepackage [english]{babel}
\usepackage[autostyle]{csquotes}

\oddsidemargin=0.1in
\evensidemargin=0.1in
\textwidth=6in
\textheight=9.5in
\topmargin=-1in
\footskip=0.5in


\newcommand{\SubItem}[1]{
    {\setlength\itemindent{15pt} \item[-] #1}
}

% colors
\newcommand{\bk}{\color{black}}
\newcommand{\rd}{\color{red}}
\newcommand{\fg}{\color{forestgreen}}
\newcommand{\bl}{\color{blue}}
\newcommand{\gr}{\color{gray}}
\newcommand{\theme}{\color{Maroon}}

% common math markups
\newcommand{\bs}[1]{\boldsymbol{#1}}
\newcommand{\mc}[1]{\mathcal{#1}}
\newcommand{\mr}[1]{\mathrm{#1}}
\newcommand{\bm}[1]{\mbox{\boldmath $#1$}}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\ds}[1]{\mathds{#1}}

% spacing and style shorthand
\newcommand{\sk}{\vspace{.4cm}}
\newcommand{\nochap}{\vspace{0.6cm}}
\newcommand{\nsk}{\vspace{-.4cm}}
\newcommand{\chap}[1]{{\theme \Large \bf #1} \sk}
\newcommand{\R}[1]{{\bl\tt #1}}
\newcommand{\til}{{\footnotesize$\bs{\stackrel{\sim}{}}$ }}

% specific stats markups for this doc
\newcommand{\E}{\ds{E}}
\newcommand{\Reals}{\ds{R}}
\newcommand{\var}{\text{var}}
\newcommand{\cov}{\text{cov}}
\newcommand{\mT}{\mc{T}}
\newcommand{\GP}{\mc{GP}}
\newcommand{\iidsim}{\stackrel{\mathrm{iid}}{\sim}}
\newcommand{\indsim}{\stackrel{\mathrm{ind}}{\sim}}
\newcommand{\mN}{\mc{N}}


\newcounter{fig}
\newcounter{tab}
\newcounter{app}
\linespread{1.5}

\title{Data Cleaning and Visualization}
\author{Lina Lee and Tsering Dolkar,\\
ASC Final Project, 2023}
\date{}


\begin{document}
\maketitle
\section*{tidyr/dplyr}

tidyr and dplyr are two popular R packages that are commonly used for data manipulation and transformation tasks. They are part of the so-called "tidyverse," a collection of R packages designed to work together seamlessly for data science and analysis. The tidyr package focuses on data tidying, which involves restructuring and organizing data to make it easier to work with and analyze. The dplyr package is focused on data manipulation tasks. It provides a set of functions for efficiently and intuitively manipulating data frames.
\subsection*{tidyr}

\textbf{Overview} \\
 When your data is organized in a standard way(tidy data), it makes using analysis tools designed for that setup much easier. This means you don't have to come up with new approaches every time you deal with differently formatted data. Keeping things consistent in how your data is set up makes your analyses more straightforward and saves you from reinventing the wheel. The goal of tidyr is to help you create tidy data. \\
Tidy data is data where:
\begin{itemize}
\item Every column is a variable. 
\item Every row is an observation. 
\item Every cell is a single value. 
\end{itemize}

We will explain how you install "tidyr" in different ways, and load in R, and then work through some hallmark functions in tidyr. \\

\noindent \textbf{Installation} \\
You need to install "tidyr" to load it in R. The easiest way to get tidyr is to install the whole tidyverse, but you can install three different ways.
{\bl \footnotesize
\begin{verbatim}
install.packages("tidyverse")
install.packages("tidyr")
install.packages("pak") # the development version from GitHub
pak::pak("tidyverse/tidyr") 
\end{verbatim}
}




We introduced how to install and load tidyr in R. Let us introduce functions in tidyr. \textbf{tidyr functions} fall into five main categories:  \\

\textbf{Pivoting}: \\
This category involves converting data between long and wide formats. The introduction of tidyr version 1.0.0 brought new functions, pivot\_longer() and pivot\_wider(), which serve as replacements for the older spread() and gather() functions.


\textbf{Rectangling:} \\
The rectangling group focuses on transforming nested lists, such as those from JSON, into tidy tibbles. Key functions in this category include unnest\_longer(), unnest\_wider(), hoist().


\textbf{Nesting and Unnesting:}\\
These functions deal with converting grouped data into a format where each group becomes a single row containing a nested data frame. Conversely, unnesting accomplishes the opposite operation. The relevant functions are nest(), unnest().

\textbf{Splitting and Combining Character Columns:} \\
This category involves operations on character columns. Use separate() and extract() to break a single character column into multiple columns. Conversely, use unite() to combine multiple columns into a single character column.


\textbf{Handling Missing Values:} \\
Functions in this category address the management of missing values. complete() makes implicit missing values explicit, drop\_na() makes explicit missing values implicit, and fill() replaces missing values with the next/previous value. Additionally, replace\_na() replaces missing values with a specified known value.
\\ \\


\textbf{cheatsheet } 
You can find some simple explanation  of functions in cheatsheet for tidyr
\begin{figure}[H]%
    \centering
    \subfloat[\centering]{{\includegraphics[width=5cm]{../material/tidyrcheatsheet1.png} }}%
    \qquad
    \subfloat[\centering ]{{\includegraphics[width=5cm]{../material/tidyrcheatsheet2.png} }}%
    \caption{tidyr cheatsheet}%
    \label{fig:example}%
\end{figure}



\textbf{Hallmark functions with Examples:} \\
In this tutorial, we will explain \textbf{pivoting and splitting functions} in detail that you may use often for data cleaning with some examples. First, we create a toy example. In this example study, participants were asked to categorize three faces by clicking various buttons that represent three different categories. The time it took to click a button is in milliseconds.
{\bl \footnotesize
\begin{verbatim}
n=10
wide <- data.frame(
  ID = c(1:n),
  face.1 = c(411,723,325,456,579,612,709,513,527,379),
  face.2 = c(123,300,400,500,600,654,789,906,413,567),
  face.3 = c(1457,1000,569,896,956,2345,780,599,1023,678)
)
\end{verbatim}
}


\begin{figure}[h]
\begin{center}
\includegraphics[scale=0.5,trim=30 0 0 10]{../material/wide.png}
\caption{wide format dataset}
\end{center}
\end{figure}
Data are often entered in a wide format where each row is often a site/subject/patient and you have multiple observation variables containing the same type of data. In this dataset, we column from face1 to face3 and each has 10 observations which are their response time. Using pivot\_longer() We want to transform the dataset into long format by creating new variable Face and response time.
for example, for the first observation of face1, Face variable will have value face1, and response time variable will have corresponding value in the first observation of face1. \\

\textbf{ pivot\_longer()}  reshape data from a wider format to a longer format. The figure shows the only first 10 rows of the long dataset. The arguments used in the functions are:
\begin{itemize}
\item cols: Columns to pivot into longer format.
\item names\_to: A character vector specifying the new column or columns to create from the information stored in the column names of data specified by cols.
\item values\_to: specify which column (or columns) to get the cell values from
\end{itemize}

{\bl \footnotesize
\begin{verbatim}
long <- wide %>%
		 pivot_longer(cols :face.1:face.3,names_to = "Face",values_to ="ResponseTime")
\end{verbatim}
}
\begin{figure}[h]
\begin{center}
\includegraphics[scale=0.4,trim=30 0 0 10]{../material/long.png}
\caption{dataset created by pivot\_long()}
\end{center}
\end{figure}

Here, we only focused on the argument you may frequently use. There are more arguments you may want to use. More detailed description about argument can be found at https://tidyr.tidyverse.org/reference/pivot\_longer.html



The next function we want to show is separate(). In the long dataset, values of subject variable consists of face1, face2, and face3. We want to split face and number. \\  The figure shows the only first 10 rows of the long dataset. \\
\textbf{separate()} turns a single character column into multiple columns.
{\bl \footnotesize
\begin{verbatim}
long_separate <- long %>%
				 separate(Face, c("Target", "Number"))
\end{verbatim}
}

\begin{figure}[h]
\begin{center}
\includegraphics[scale=0.44,trim=30 0 0 10]{../material/long_separate.png}
\caption{dataset created by separate()}
\end{center}
\end{figure}
 Next, let us see how we can combine the two seprate character column back to one. \\


\textbf{unite()} merge two or more columns into a single column or variable.
{\bl \footnotesize
\begin{verbatim}
long_unite <- long_separate %>% 
					unite(Face, Target, Number, sep = ".")
\end{verbatim}
}

\begin{figure}[h]
\begin{center}
\includegraphics[scale=0.44,trim=30 0 0 10]{../material/long_unite.png}
\caption{dataset created by long\_unite()}
\end{center}
\end{figure}


we can unite the two separate character column back to one using unite() function. In the unite(), you need to specify the two character columns you want to unite. The dataset is identical with  long dataset. \\
Now, we want to convert long dataset back to wide dataset. We can achieve this by using pivot\_wider(). \\

\textbf{pivot\_wider()} "widens" data, increasing the number of columns and decreasing the number of rows. The arguements used for the functions:
\begin{itemize}
\item names\_from: specify which column (or columns) to get the name of the output column 
\item values\_from: specify which column (or columns) to get the cell values from
\end{itemize}

Here, we only focused on the argument you may frequently use. There are more arguments you may want to use. More detailed description about argument can be found at https://tidyr.tidyverse.org/reference/pivot\_wider.html

{\bl \footnotesize
\begin{verbatim}
back_to_wide <- long_unite %>% pivot_wider(names_from =Face, values_from =ResponseTime)
\end{verbatim}
}
\begin{figure}
\begin{center}
\includegraphics[scale=0.6,trim=30 0 0 10]{../material/back_to_wide.png}
\end{center}
\caption{dataset created by pivot\_wider()}
\end{figure}

We see that the long dataset was converted back to wide format. Depending on what you need, you can transform your dataset into long format or wide formate using pivot\_longer() and pivot\_wider()


\subsection*{dplyr}

\textbf{dplyr} is like a toolbox for handling data. It has a set of tools (or actions) that help with common tasks:
\begin{itemize}
\item mutate() lets you create new variable from what you already have.
\item select() lets you pick out specific column.
\item filter() lets you choose only certain parts of your data based on their values.
\item summarise() condenses multiple values into a single summary.
\item arrange() changes the order of your data.

These tools work well together, and there's also a special tool called group\_by() that helps you do things to your data in groups.
\end{itemize}


\textbf{Advantage of dplyr}
\begin{itemize}
\item It provides simple “verbs”, functions that correspond to the most common data manipulation tasks
\item It uses efficient backends, so you spend less time waiting for the computer.
\end{itemize}



\textbf{Installation} \\
The easiest way to get dplyr is to install the whole tidyverse, but you can install dplyr only too.
{\bl \footnotesize
\begin{verbatim}
install.packages("tidyverse")
install.packages("dplyr")
install.packages("pak") # Development version
pak::pak("tidyverse/dplyr")
\end{verbatim}
}

\textbf{Cheatsheet}\\
You can find some simple explanation  of functions in cheatsheet 

\begin{figure}[H]
    \centering
    \subfloat[\centering ]{{\includegraphics[width=5cm]{../material/dplyrcheatsheet1.png} }}%
    \qquad
    \subfloat[\centering ]{{\includegraphics[width=5cm]{../material/dplyrcheatsheet2.png} }}%
    \caption{dplyr cheatsheet}%
    \label{fig:example}%
\end{figure}

\textbf{Hallmark functions with Examples:} \\



In this tutorial, we explain group\_by(), summarize(), arrange(), mutate(), and filter(). Below, we generated two dataset top4airlines and top5airports.  We explained the detail of only first part, which is code to make top4airlines. The second part is similar with the first part except it used different variables.  Using the datasets, we will perform further data operations. \\


 1. Determine which 4 airlines are busiest with ‘other’ which include all other airlines. 
The “busiest” is determined by the number of flights in the dataset. 
{\bl \footnotesize
\begin{verbatim}
top4airlines <- delays %>%
  group_by(OP_UNIQUE_CARRIER) %>%
  summarize(Flights=n() ) %>%
  arrange(desc(Flights)) %>%
  top_n(n=4)
\end{verbatim}
}
 Determine which 5 departing airports have the most busiest.
{\bl \footnotesize
\begin{verbatim}
top5airports <- delays %>%
  group_by(ORIGIN) %>%
  summarize(Flights = n() ) %>%
  arrange(desc(Flights) ) %>%
  top_n(n=5)
\end{verbatim}
}


First, Let us introduce pipeline function: 
{\bl \footnotesize
\begin{verbatim}
%>% 
\end{verbatim}
} allows you to pipe a value forward into an expression or to function call \\


\textbf{groupby()} takes an existing tbl and converts it into a grouped tbl where operations are performed by group. You can remove it by using group\_by(). The first argument is data frame's name. The second arguments are the variable we want to group by. In this example, delays dataset is grouped by airlines (OP\_UNIQUE\_CARRIER). 
{\bl \footnotesize
\begin{verbatim}
top4airlines <- delays %>%
  group_by(OP_UNIQUE_CARRIER) 

\end{verbatim}
}

\begin{figure}[h]
\begin{center}
\includegraphics[scale=0.5,trim=30 0 0 10]{../material/groupby.png}
\end{center}
\begin{center}
\includegraphics[scale=0.5,trim=30 0 0 10]{../material/groupby2.png}
\end{center}
\caption{dataset created by group\_by()}
\end{figure}



 \textbf{summarize()}: summarize the data frame into just one value or vector. It returns one row for each combination of grouping variables. The first argument is data frame's name. The second arguments are expression so that we create a new variable by summarizing current dataset. In this example, number of flight was calculated by airline group (OP\_UNIQUE\_CARRIER).
{\bl \footnotesize
\begin{verbatim}
top4airlines <- delays %>%
  group_by(OP_UNIQUE_CARRIER) %>%
  summarize(Flights=n() ) 
\end{verbatim}
}

\begin{figure}[h]
\begin{center}
\includegraphics[scale=0.5,trim=30 0 0 10]{../material/summarize.png}
\end{center}
\caption{dataset created by summarize()}
\end{figure}


\textbf{arrange()}: arrange a column in descending order.  The first argument is data frame's name. The second arguments are variable in current dataset that we want to arrange by. In this example we arragne dataset by Flights variable, which was created in the previous piplline. All the operations following group\_by() function is performed by group.

{\bl \footnotesize
\begin{verbatim}
top4airlines <- delays %>%
  group_by(OP_UNIQUE_CARRIER) %>%
  summarize(Flights=n() ) %>%
  arrange(desc(Flights)) 
\end{verbatim}
}
\begin{figure}[h]
\begin{center}
\includegraphics[scale=0.5,trim=30 0 0 10]{../material/arrange.png}
\end{center}
\caption{dataset created by arrange()}
\end{figure}

Now, based on the above dataset set we created, we will perform further operation. \\
 2. Calculate the number of flights for each of the 5 busiest airports and 4 busiest airlines.
{\bl \footnotesize
\begin{verbatim}
busiestAirlinesAirport <- delays %>%
mutate(Carrier = ifelse(OP_UNIQUE_CARRIER %in% 
top4airlines$OP_UNIQUE_CARRIER, OP_UNIQUE_CARRIER, "Other"),
Carrier=factor(Carrier,levels=c(top4airlines$OP_UNIQUE_CARRIER
                     ,"Other"))) %>%
  filter(ORIGIN %in% top5airports$ORIGIN) %>%
  group_by(Carrier, ORIGIN) %>%
  summarize(Flights = n() )
\end{verbatim}
}





 \textbf{mutate()}: creates new columns that are functions of existing variables. In the function, we usually have two arguments. The first argument is data frame's name. The second argument is the expressions that satisfy the condition. Here, we create a new function Carrier. Due to data is large, we will not include the dataset resulted by mutate(). Instead, we included simple figure to demostrate dataset after applying mutate().

{\bl \footnotesize
\begin{verbatim}
busiestAirlinesAirport <- delays %>%
mutate(Carrier = ifelse(OP_UNIQUE_CARRIER %in% top4airlines$OP_UNIQUE_CARRIER,
 									OP_UNIQUE_CARRIER, "Other"),
\end{verbatim}
}

\begin{figure}[h]
\begin{center}
\includegraphics[scale=0.9,trim=30 0 0 10]{../material/mutate.png}
\end{center}
\caption{mutate())}
\end{figure}

In new variable Carrier, if OP\_UNIQUE\_CARRIER is in top4airlines , put the same value of OP\_UNIQUE\_CARRIER.  If not, put "Other".  
{\bl \footnotesize
\begin{verbatim}
Carrier = ifelse(OP_UNIQUE_CARRIER %in% top4airlines$OP_UNIQUE_CARRIER, OP_UNIQUE_CARRIER,
				  "Other"),
\end{verbatim}
}

\textbf{filter()} retains all rows that satisfy your conditions. Here, we filtered the origin which are in top 5 busiest airport. The first argument is data frame's name. The second arguments are the expressions that filter the data frame. 
filter() create subset which satisfy condition in (). Due to data is large, we will not include the dataset resulted by filter(). Instead, we included simple figure to demostrate dataset after applying filter().

{\bl \footnotesize
\begin{verbatim}
busiestAirlinesAirport <- delays %>%
mutate(Carrier = ifelse(OP_UNIQUE_CARRIER %in% 
top4airlines$OP_UNIQUE_CARRIER, OP_UNIQUE_CARRIER, "Other"),
Carrier=factor(Carrier,levels=c(top4airlines$OP_UNIQUE_CARRIER
                     ,"Other"))) %>%
  filter(ORIGIN %in% top5airports$ORIGIN) 
\end{verbatim}
}
\begin{figure}[h]
\begin{center}
\includegraphics[scale=0.9,trim=30 0 0 10]{../material/filter.png}
\end{center}
\caption{dataset created by filter()}
\end{figure}

After applying groupby() and summarize(), the final data will look like:
{\bl \footnotesize
\begin{verbatim}
busiestAirlinesAirport<-busiestAirlinesAirport %>%
  group_by(Carrier, ORIGIN) %>%
  summarize(Flights = n() )
\end{verbatim}
}

\begin{figure}[h]
\begin{center}
\includegraphics[scale=0.5,trim=30 0 0 10]{../material/busiestAirplainAirport.png}
\end{center}
\caption{The final dataset}
\end{figure}

\section*{ggplot/ plotly}
\subsection*{ggplot2}

\R{ggplot2} is based on the grammar of graphics, the idea that you can build every graph from the same components: a \textbf{data} set, a \textbf{coordinate system}, and \textbf{geoms}--visual marks that represent data points.

{\rd \footnotesize
\begin{verbatim}
ggplot(data= <DATA>) + 
 <GEOM_FUNCTION> (mapping = aes(<MAPPING>), stat=<STAT>,position=<POSITION>) +
 <COORDINATE_FUNCTION> +
 <FACET_FUNCTION> +
 <SCALE_FUNCTION>  +
 <THEME_FUNCTION>
\end{verbatim}
}


\subsubsection*{Scatter plots using Geoms, Stats, labels, and themes:}

We can observe simple scatterplot using \R{ggplot2} in two ways:
\vspace{-0.5cm}
\begin{itemize}
\item Using \R{qplot}, which has a simplified syntax, and
\item \R{ggplot}, which can be saved into a variable and transformed later with further commands.
\end{itemize}
\vspace{-0.5cm}

{\bl \footnotesize
\begin{verbatim}
>qplot(x, y, data=dat)
\end{verbatim}
}
\vspace{-0.25cm}
This is the same as doing the following:
\vspace{-0.25cm}
{\bl \footnotesize
\begin{verbatim}
>dat <- data.frame(x=1:10,y=(1:10)^2)
>p <- ggplot(dat, aes(x=x,y=y)) + 
   geom_point(shape=1) 
\end{verbatim}
}


... a simple way to visualize functions in the $x$-$y$
plane

{\bl \footnotesize
\begin{verbatim}
>p + geom_line(color = "red", linetype = "dashed", size = 1) +
   stat_function(fun = function(x) x^2, color = "black", 
                  linetype = "solid", aes(group = "curve")) +
   labs(title = "Comparison of lines and curve in ggplot2",
        x = "X-axis Label",
        y = "Y-axis Label") +
   theme()
\end{verbatim}
}

This plot is the \R{ggplot2} equivalent of base plot in lect8. We can see in this plot how we can use \R{geom} functions, \R{stats} function to recreate a base plot. The \R{stat} function here works like \verb|curve()| in base plot. Then we can fix the title, \verb|x| and \verb|y| lab using \R{labs}. \R{theme} here controls the background of our \R{ggplot}.

\begin{center}
\includegraphics[scale=0.8,trim=10 0 0 10]{../material/pointslines}
\end{center}

\subsubsection*{Flight Delay with overflowing legend}

\vspace{-0.25cm}
{\bl \footnotesize
\begin{verbatim}
>b <- ggplot(delays, aes(x= DEP_DELAY, y=ARR_DELAY, 
             col=OP_UNIQUE_CARRIER)) +
          geom_point() +
          labs(x = "Departure Delay", y = "Arrival Delay") + 
          guides(color=guide_legend(ncol=2,byrow=F))
\end{verbatim}
}

\begin{center}
\includegraphics[scale=0.8,trim=20 0 0 20]{../material/delayscatter.png}
\end{center}

This plots scatter plot of flight departure delay to arrival delay for each airlines. We can see that the two variables are positively correlated to each other and that the pinker airlines have less delays overall in the month of August. The main focus of this plot is for the cases with overflowing legends. We can use \R{guides} function to break the legend in however many rows/columns we want to specify. 

\subsubsection*{Histogram with ordered faceting}

\vspace{-0.25cm}

{\bl \footnotesize
\begin{verbatim}
>orderairlines <- delays %>%
  group_by(OP_UNIQUE_CARRIER) %>%
  summarize(Flights=n() ) %>%
  arrange(desc(Flights)) %>%
  top_n(n=17)

>df1 <- delays[order(match(delays$OP_UNIQUE_CARRIER,
                       orderairlines$OP_UNIQUE_CARRIER)),]
>c <- ggplot(df1, aes(x=DEP_DELAY)) +
           geom_histogram(bins=30) +
           labs(x = "Departure Delay") + 
           guides(fill=guide_legend(ncol=2,byrow=F)) +
           facet_wrap(~factor(OP_UNIQUE_CARRIER, 
                orderairlines$OP_UNIQUE_CARRIER), ncol = 5,
                 scales = "free_y") + 
           theme_bw()
\end{verbatim}
}

Here, we plot the histogram of flight departure delay in the month of August and facet wrap it for each airline. Since the carrier is categorical with no particular order, R seems to assume it's own order to the carriers. Therefore, I took the ordered airlines data by the most flights in August and facet wrapped it so the airlines show up with the one that flew the most, i.e \verb|WN|, to the one that flew the least, i.e \verb|HA|.

\begin{center}
\includegraphics[scale=0.8,trim=10 0 0 10]{../material/depdelayhist}
\end{center}


\subsubsection*{Scale:}
Observe the following plot to see how we can override defaults from \R{ggplot} with scale package. 

{\bl \footnotesize
\begin{verbatim}
>require("scales")
>df <- economics %>% 
  filter(date < ymd("1970-01-01"))

>p <- ggplot(df, aes(date, pce)) + geom_line()
>p
\end{verbatim}
}

\begin{center}
\includegraphics[scale=0.5,trim=30 10 10 10]{../material/ggscale1.png}
\end{center}
{\bl \footnotesize
\begin{verbatim}
>p <- p + scale_x_date(NULL,
                  breaks = breaks_width("3 months"), 
                  labels = label_date_short()) + 
  scale_y_continuous("Personal consumption expenditures",
                     breaks = breaks_extended(8),
                     labels = label_dollar())
>p
\end{verbatim}
}
\begin{center}
\includegraphics[scale=0.5,trim=30 10 10 10]{../material/ggscale2.png}
\end{center}

\subsubsection*{Heatmap with ggplot and transforming ggplot to plotly}

\vspace{-0.25cm}

{\bl \footnotesize
\begin{verbatim}
>busiestAirlinesAirport <- delays %>%
  mutate(Carrier = ifelse(OP_UNIQUE_CARRIER %in% 
     top5airlines$OP_UNIQUE_CARRIER,
      OP_UNIQUE_CARRIER, "Other"),
     Carrier = factor(Carrier, 
       levels=c(top5airlines$OP_UNIQUE_CARRIER, "Other"))) %>%
  filter(ORIGIN %in% top5airports$ORIGIN) %>%
  group_by(Carrier, ORIGIN) %>%
  summarize(Flights = n() )

>e <- ggplot(busiestAirlinesAirport, 
        aes(ORIGIN, Carrier, fill= Flights)) + 
  geom_tile()
>ggplotly(e, tooltip="text")
\end{verbatim}
}

The purpose of this heatmap code, is to show how we can transform \R{ggplots} to \R{plotly} using \R{ggplotly}. We will later cover this heatmap again with proper \R{plotly} structure. 

\begin{center}
\begin{figure}[H]
    \centering
    \subfloat[\centering ggplot]{\includegraphics[width=9cm]{../material/ggheat}}%
    \qquad
    \subfloat[\centering ggplotly]{\includegraphics[width=9cm]{../material/plotlyheat.png}}%
    \caption{2 ways of studying heatmap}%
\end{figure}
\end{center}


\subsection*{plotly}

\R{plotly} is a free and open source graphing library for \R{R}.  
The structure for \verb|plot_ly| is as follows:
{\rd \footnotesize
\begin{verbatim}
plot_ly(
  data = data.frame(), ..., type = NULL, name, color, colors = NULL,
  alpha = NULL, stroke, strokes = NULL, alpha_stroke = 1, size,
  sizes = c(10, 100), span, spans = c(1, 20),  symbol,  symbols = NULL,
  linetype,  linetypes = NULL,  split,  frame,  width = NULL,  height = NULL,
  source = "A"
)
\end{verbatim}
}
\verb|...|: e.g. \verb|plot_ly(x=1:10, y=1:10, color=I("red"), marker=list(color = "blue"))| \\
\verb|type| :  A character string specifying the trace type (e.g. "scatter", "bar", "box", etc)\\
\verb|name| : Values mapped to the trace's name attribute. Since a trace can only have one name, this argument acts very much like split in that it creates one trace for every unique value.\\
\verb|color| : Values mapped to relevant 'fill-color' attribute(s). The mapping from data values to color codes may be controlled using colors and alpha, or avoided altogether via I(). \\
\verb|colors| : Either a colorbrewer2.org palette name (e.g. "Purples" or "Blues") \\
\verb|alpha| : A number between 0 and 1 specifying the alpha channel applied to color, i.e transparency. \\
\verb|stroke/ strokes / alpha_stroke| : similar to color/ colors/ alpha. \\
\verb|size| : Can be used to create bubble chart or to show size mapping in plots.\\
\verb|symbol| : (Discrete) values mapped to marker.symbol. Any pch value can be used this way.\\
\verb|symbols| : A character vector of pch values or symbol names.\\
\verb|split| : (Discrete) values used to create multiple traces (one trace per value).\\
\verb|frame| : (Discrete) values used to create animation frames.

Starting with basics of plotly, we will look at how to write scatter plot and line plot.

{\bl \footnotesize
\begin{verbatim}
# markers vs lines
trace_0 <- rnorm(100, mean = 5)
trace_1 <- rnorm(100, mean = 0)
trace_2 <- rnorm(100, mean = -5)
x <- c(1:100)
data <- data.frame(x, trace_0, trace_1, trace_2)

fig <- plot_ly(data, x = ~x, y = ~trace_0, name = 'trace 0', type = 'scatter', mode = 'lines')
fig <- fig %>% add_trace(y = ~trace_1, name = 'trace 1', mode = 'lines+markers')
fig <- fig %>% add_trace(y = ~trace_2, name = 'trace 2', mode = 'markers')
fig
# Set layout options
fig <- fig %>%
  layout(xaxis = list(title = "xlab"),
         yaxis = list(title = "traces"),
         showlegend = TRUE)
fig
\end{verbatim}
}
\begin{center}
\includegraphics[scale=0.2, trim=10 0 0 30]{../material/lineplotlynoanime.png}
\end{center}

Next we can create an animation of line plot. The animation draws lines as the day changes. X axis represents xlab, Y axis shows the three traces. The animation will include a button for ‘play’ the animation and a slider for the day change.

{\bl \footnotesize
\begin{verbatim}
data <- data %>%mutate(x =as.numeric(day(x)))
data <- data %>% accumulate_by(~x)

p <- plot_ly(data,
             x = ~x, 
             y = ~trace_0,
             name = 'trace 0',
             frame = ~frame, 
             type = 'scatter',
             mode = 'lines', 
             line = list(simplyfy = F)) %>% 
  add_trace(y = ~trace_1, name = 'trace 1', mode = 'lines+markers') %>%
  add_trace(y = ~trace_2, name = 'trace 2', mode = 'markers')

# same plot as before, only the axis names changed
p <- p %>% layout(
  xaxis = list(
    title = "xlab",
    zeroline = F),
  yaxis = list(
    title = "traces",
    zeroline = F)) 
p
\end{verbatim}
}
The animated line plot is done, however, you will notice that the animation speed is still very slow. We increase this speed by changing frame and transition.

{\bl \footnotesize
\begin{verbatim}
p <- p %>% animation_opts(
  frame = 100, 
  transition = 0, 
  redraw = FALSE
)
p
\end{verbatim}
}
\begin{center}
\includegraphics[scale=0.2, trim=10 0 0 30]{../material/plotlyanime1.png}
\end{center}

We can look into how \verb|symbol/ symbols| works in \R{plotly}.
{\bl \footnotesize
\begin{verbatim}
fig <- plot_ly(data = iris, x = ~Sepal.Length, y = ~Petal.Length, type = 'scatter',
               mode = 'markers', symbol = ~Species, symbols = c('circle','x','o'),
               color = I('black'), marker = list(size = 10))

fig
\end{verbatim}
}
\begin{center}
\includegraphics[scale=0.2, trim=10 0 0 30]{../material/plotlysymbolscatter.png}
\end{center}

Before, we wrote heatmap in \R{ggplot} and transformed it to \R{plotly} using \R{ggplotly}. However, to write it in \R{plotly} structure, the following is the another way of writing it.

{\bl \footnotesize
\begin{verbatim}
fig <- plot_ly(busiestAirlinesAirport,
  z = ~Flights,
  type = "heatmap",
  colors = "Blues",  # Choose a color scale
  colorbar = list(title = "Values"),
  x = ~ORIGIN,  # X-axis labels
  y = ~Carrier,  # Y-axis labels
  showscale = TRUE
) %>% layout(
  title = "Heatmap ",
  xaxis = list(title = "X-Axis"),
  yaxis = list(title = "Y-Axis")
)
fig
\end{verbatim}
}

\begin{center}
\includegraphics[scale=0.2, trim=10 0 0 30]{../material/ggplotlyheat.png}
\end{center}

\subsubsection*{Choropleth Map}

We will consider choropleth maps for today. Plotly comes with two built-in geometries which do not require external file.
\begin{itemize}
\item USA States
\item Countries as defined in the Natural Earth dataset.
\end{itemize}


{\bl \footnotesize
\begin{verbatim}
>us_delay_summary <- delays %>%
  group_by(ORIGIN_CITY_NAME) %>%
  summarize(Avg.Delay = mean(DEP_DELAY, na.rm=TRUE)) 

>us_delay_summary<-us_delay_summary%>%
  mutate(ORIGIN_STATE_ABR=
       str_extract(ORIGIN_CITY_NAME, "\\b[A-Z]{2}"))
       
>df <- us_delay_summary
\end{verbatim}
}

{\bl \footnotesize
\begin{verbatim}
>e <- plot_ly(df,
  type = 'choropleth',
  locations = ~ORIGIN_STATE_ABR, locationmode = 'USA-states', color = ~Avg.Delay,
  colors = 'Purples',
  z = ~Avg.Delay, text=df$ORIGIN_CITY_NAME)
\end{verbatim}
}

\begin{center}
\includegraphics[scale=0.2,trim=10 0 0 10]{../material/choroplotly1.png}
\end{center}

{\bl \footnotesize
\begin{verbatim}
>># specify some map projection/options
g.eo <- list(
  scope = 'usa',
  projection = list(type = 'albers usa'),
  showlakes = TRUE,
  lakecolor = toRGB('white')
)
>e <- e %>% layout(
  title = 'flight departure and arrival delay from each state',
  geo = g.eo)
\end{verbatim}
}
\begin{center}
\includegraphics[scale=0.2, trim=10 0 0 30]{../material/choroplotly2.png}
\end{center}

Another way of doing this is:
{\bl \footnotesize
\begin{verbatim}
>fig <- plot_geo(df, locationmode = 'USA-states')
fig
\end{verbatim}

%\begin{center}
%\includegraphics[scale=0.2, trim=10 0 0 30]{../material/choroalt1.png}
%\end{center}

\begin{verbatim}
fig <- fig %>% add_trace(
  z = ~Avg.Delay, text = ~Avg.Delay, locations = ~ORIGIN_STATE_ABR,
  color = ~Avg.Delay, colors = 'Purples')

fig
\end{verbatim}
\begin{verbatim}
fig <- fig %>% colorbar(title = "Avg.Delay")
fig
\end{verbatim}
%\begin{center}
%\includegraphics[scale=0.2, trim=10 0 0 30]{../material/choroalt3.png}
%\end{center}

\begin{verbatim}
># specify some map projection/options
g.eo <- list(
  scope = 'usa',
  projection = list(type = 'albers usa'),
  showlakes = TRUE,
  lakecolor = toRGB('white'))

>fig <- fig %>% layout(
  title = 'flight departure and arrival delay from each state',
  geo = g.eo)
fig
\end{verbatim}
}
\begin{center}
\includegraphics[scale=0.2, trim=10 0 0 30]{../material/choroalt4.png}
\end{center}
\end{document}