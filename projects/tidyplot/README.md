## Final Project

This is ASC final project of Lina Lee (linal20@vt.edu) and Tsering Dolkar (tdolkar@vt.edu) on **tidyr/dplyr and ggplot2/plotly**.

- It contains a homework directory with `hw.rmd` and further clarifying instructions in README.

- It contains a homework solution directory with `rmd` file.

- All the relevant plot pdfs and pngs for the tutorial <`tutorial.tex` > and presentation <`plot.tex`> file are in the home directory. Tutorial is a little longer than 10 pages, but this is mostly due to inclusion of plots.

Contributions are as follows:

**Lina Lee :** Worked on `tidyr/dplyr` presentation and tutorial, worked on homework and homework solution. The alternate choropleth map in plotly example at the end.

**Tsering Dolkar :** Worked on `ggplot2/plotly` presentation and tutorial, created and debugged the latex file structure, formalized/typed final hw/hw solution folder. 


