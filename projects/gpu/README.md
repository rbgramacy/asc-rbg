## GPUs in R

Final project on the use of Graphics Processing Units by Steven Barnett (sdbarnett@vt.edu) and Jaeyoung Lee (jaeyounglee@vt.edu)

Contents:

- Tutorial
	- introduces GPUs through the `torch` package in `R`. Provides basic steps to begin working in `torch` and two example applications.

- Presentation
	- slides that largely follow the tutorial, but formatted for a class lecture.

- Homework
	- asks students to set up `torch` on remote server and implement distance matrix calculation and estimate of lengthscale in Multivariate Normal Likelihood

- Homework Solution
