**Topic: R Packages Using Roxygen**

This repository contains:

The tutorial and presentation (with all files necesssary to build them)

A 'homework' folder, containing the materials and solutions of the homework problem.

A 'mylm' folder, containing the files necessary to build the asclr R package.

A 'mypowers' folder, containing the files necessary to build the PowersR R package.

Tasks: 

1. Tutorial: Initial setup done by Parul, with Anna reviewing.

2. HW Problem: Initial setup done by Anna, with Parul reviewing.

3. PPT: Initial setup done by Anna (following stucture of tutorial), with Parul reviewing.

4. We troubleshot ROxygen issues together as they came up.
