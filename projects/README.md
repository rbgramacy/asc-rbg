## Final Project

The final project will be comprised of group presentations on topic areas, with materials comprising

- 30 minute in-class presentation
- 5-10 page tutorial with examples
- One homework problem with solutions.

Students must work together, keeping track of that work in their bitbucket repository.

- Up to three students per group, aiming for three groups total.
- Presentations will be during one of the class periods **after Thanksgiving**. 
- Tutorials, homework problem and presentation material are due **Friday Dec 1, 5pm**, no matter when you are presenting.  I will pull the material from your (private) repo and share it (except the homework solutions) with the rest of the class on this repo.
- In your README.md file you must explain who on your team contributed in what way.  
- Your team must meet with me before the Thanksgiving break to discuss your plan and your homework problem.
- Topics and presentation dates are first-come-first-served.
- You will be graded on the quality of your presentation and all materials including the tutorial, homework problem and solution.
- Students from other groups must turn in their solutions to all homework problems (besides that produced by their group) by **Friday Dec 8th, 5pm**.

### Potential topics

- ggplot2/plotly
- dplyr/tidyr
- text processing, and regular expressions (sed and awk)
- distributed computation & storage: Amazon EC2/S3, map-reduce, hadoop/spark 
- GPU libraries for R
- Shiny and [shinyapps.io](shinyapps.io)
- Building a package with Roxygen docs
- Quarto
- Parallel evaluation in R (foreach, furr, etc.)

Other topics may be proposed.
