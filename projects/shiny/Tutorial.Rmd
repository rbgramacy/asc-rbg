---
title: "Tutorial"
author: "Mingang Kim & Piper Zimmerman"
date: "2023-11-13"
output: pdf_document
header-includes:
   - \usepackage{float}
---

ShinyR is an R package designed for constructing interactive web applications, enabling users to develop interactive dashboards and data visualizations without the need for expertise in web development languages such as HTML, CSS, or JavaScript. Shiny apps can be privately used and shared, or alternatively, users can create a Shiny.io account for web publication. 

This tutorial aims to guide users through the step-by-step process of creating their own basic Shiny app. 


In order to use Shiny, the user needs to install the \texttt{“shiny”} package in \texttt{R}. \texttt{"shinythemes"} is also installed to change the theme of the app.

## Install shiny pacakge in R

```{r, eval = FALSE}
install.packages("shiny")
install.packages("shinythemes")
library(shiny)
library(shinythemes)
```

Now, we are ready to start our journey. 



Building a Shiny app consists of two main components: the user interface, often referred to as the "\textbf{frontend}" and the server, known as the "\textbf{backend}". The user interface (UI) is responsible for the app's layout, appearance, and user interactions, enabling the creation of pages, tabs, and control widgets. On the other hand, the server processes the app's logic, handling tasks such as generating plots and managing user inputs.

Now, we will talk about this Frontend and Backend in detail one by one. 

## UI 

The \texttt{fluidPage} function is the basic building block for the UI in Shiny. It creates a responsive, fluid layout that adjusts to the size of the user's browser window. 

```{r, eval=FALSE}
# Define UI ----
ui <- fluidPage(
  
)
```



### Page Layout
 
A large part of making a Shiny app is determining the layout. The user can create pages using a navigation bar or list. A navigation bar displays the page (or panel) titles across the top of the screen, while a navigation list appears as a list on the left side of the screen. 

One of the most common layouts is using the sidebar panel option. This allows the user to create a title panel, and then split the main part of the page into a side panel and a main panel. This is often used to display widgets on the side and the main plot, text, or output in the main panel. Figure 1 is an example of using a title panel, side panel and a main panel.

```{r, eval=FALSE}
ui <- fluidPage(
                            titlePanel("title panel"),
                            sidebarLayout(
                              sidebarPanel("sidebar panel"),
                              mainPanel("main panel")
                            ))
```

\begin{figure}[H]
  \includegraphics{www/Layout.png}
  \caption{Example of a sidebarPanel layout}
\end{figure}

Another way to customize the layout is to make use of the grid system, which is also what’s happening using the sidebar system. The Shiny app is divided into a grid, using \texttt{fluidRow} and columns. Each row has 12 columns. When manually splitting the page using the \texttt{column} function, the first argument is the desired width of that column. There is not a specified number of rows for the app layout. This is shown in Figure 2.

```{r eval=FALSE}
ui<-fluidPage(
  titlePanel("My Shiny App"),
  fluidRow(
    column(4,
      h3("Column 1"),
      p("This column has a width of four. 
        The other column has a width of 8 
        in order to be a total of 12.")
    ),
    column(8,
      h3("Column 2")
    )
  ),
  fluidRow(
    column(12,
      h3("Second Row"),
    )
  )
)
```

\begin{figure}[H]
  \includegraphics{www/Grid.png}
  \caption{Example of a layout with fluidRow and columns}
\end{figure}

### HTML Styling

To make the layout more sophisticated, we can use Shiny Function which is similar to HTML styling. The table below is a list of functions we can use for making layout look better. 

| Shiny Function | Creates                                    |
|-----------------|--------------------------------------------|
| p               |  A paragraph of text                       |
| h1              |  A first-level header                       |
| h2              | A second-level header                      |
| h3              | A third-level header                       |
| h4              | A fourth-level header                      |
| h5              |  A fifth-level header                       |
| h6              | A sixth-level header                       |
| a               | A hyperlink                               |
| br              | A line break (e.g., a blank line)          |
| div             | A division of text with a uniform style   |
| span            |  An in-line division of text with a uniform style |
| pre             |  Text 'as is' in a fixed-width font         |
| code            |  A formatted block of code                  |
| img             |  An image                                  |
| strong          |  Bold text                                 |
| em              |  Italicized text                           |


Functions that start with h, which are \texttt{h1-h6}, choose the size of headers. \texttt{h1} is the biggest font size and h6 is the smallest font size. We can also choose alignment of the text in the functions with argument \texttt{align}. There are "left", "right" and "center" options for alignment. These are shown in Figure 3.

```{r, eval=FALSE}
ui <- fluidPage(
  titlePanel("title panel"),
  sidebarLayout(
    sidebarPanel("sidebar panel"),
    mainPanel(
      h1("First level title; center", align = "center"),
      h2("Second level title; center", align = "center"),
      h3("Third level title; left", align = "left"),
      h4("Fourth level title; left", align = "left"),
      h5("Fifth level title; right", align = "right"),
      h6("Sixth level title; right", align = "right"),
    )
  )
)
```

\begin{figure}[H]
  \includegraphics{www/Headers.png}
  \caption{Example of headers h1-h6}
\end{figure}

Another examples of using HTML styling functions are like below in Figure 4.

```{r, eval=FALSE}
ui <- fluidPage(
  titlePanel("My Shiny App"),
  sidebarLayout(
    sidebarPanel(),
    mainPanel(
      p("p creates a paragraph of text."),
      p("A new p() command starts a new paragraph. 
        Supply a style attribute to change the format of the entire paragraph.", 
        style = "font-family: 'times'; font-si16pt"),
      strong("strong() makes bold text."),
      em("em() creates italicized (i.e, emphasized) text."),
      br(),
      code("code displays your text similar to computer code"),
      div("div creates segments of text with a similar style. 
          This division of text is all blue because 
          I passed the argument 'style = color:blue' to div",
          style = "color:blue"),
      br(),
      p("span does the same thing as div, but it works with",
        span("groups of words", style = "color:blue"),
        "that appear inside a paragraph.")
    )
  )
)
```

\begin{figure}[H]
  \includegraphics{www/Different_Text.png}
  \caption{Examples of different texts}
\end{figure}

### Image

It is possible to enhance the visualization effect and improve content understanding in a Shiny app by incorporating images. To add an image in the app, we can use \texttt{renderImage} in the server. The generic syntax for output in the server is \texttt{output\$label}. Label is what is used as a reference in the ui so that the app can match the inputs and outputs. This uses \texttt{output\$image\_label} and then a list containing specifications. In our example, we have \texttt{src}, which is the image path, and then the width and height that we want. The argument \texttt{deleteFile=FALSE} makes sure that the image is not deleted after it's rendered. In the ui, the user specifies \texttt{imageOutput("image\_label")}. It is generally good practice to keep all images related to the app in a "www" folder. The example is below in Figure 5.

```{r, eval=FALSE}
ui <- fluidPage(
  titlePanel("title panel"),
  sidebarLayout(
    sidebarPanel("sidebar panel"),
    mainPanel(
      imageOutput("myImage")
    )
  )
)
```

## Server

```{r, eval=FALSE}
# Define server logic ----
server <- function(input, output) {
  output$myImage <- renderImage({
    list(src = "www/my_image.png", width = 300, height = 100)
  }, deleteFile = FALSE)
}
```

\begin{figure}[H]
  \includegraphics{www/Image.png}
  \caption{Example of including an image}
\end{figure}

Below is a simple example of another use of the server. We are using \texttt{renderPlot} to create a scatter plot with the \texttt{mtcars} dataset. In this example, we are assigning our plot to \texttt{"plot\_example"}, and calling it in the ui using the \texttt{plotOutput} function. The app is shown in Figure 6.

```{r, eval=FALSE}
ui <- fluidPage(
  titlePanel("title panel"),
  sidebarLayout(
    sidebarPanel("sidebar panel"),
    mainPanel(
      plotOutput("plot_example")
    )
  )
)
```

```{r, eval=FALSE}
# Define server logic ----
server <- function(input, output) {
  output$plot_example <- renderPlot({
    plot(mtcars$mpg, mtcars$hp,
         xlab="mpg",ylab="hp",
         xlim = input$x_limits)
  })
}
```

\begin{figure}[H]
  \includegraphics{www/plot_example.png}
  \caption{Example of using the server to include a plot}
\end{figure}

## Control Widgets

Another feature that allows the Shiny app to be interactive is the use of control widgets. The types of inputs are shown below. The user can use these widgets to make interactive plots, text, or data tables. For example, you could use the checkbox group to display or hide different groups on a plot or use the date range input to filter for specific dates in a time series. Every widget takes the arguments name and label. The name is what the user will define in the server in order to connect the two. The label is what will appear above the widget on the app. Other arguments are specific to the widget. These could include the initial value, ranges, increments, or choices. 

Here are the input types that can be used. 

| Input Type          | Description                                   |
|----------------------|-----------------------------------------------|
| actionButton         | Action Button                                 |
| checkboxGroupInput   | A group of check boxes                        |
| checkboxInput        | A single check box                            |
| dateInput            | A calendar to aid date selection              |
| dateRangeInput       | A pair of calendars for selecting a date range|
| fileInput            | A file upload control wizard                  |
| helpText             | Help text that can be added to an input form  |
| numericInput         | A field to enter numbers                      |
| radioButtons         | A set of radio buttons                        |
| selectInput          | A box with choices to select from             |
| sliderInput          | A slider bar                                   |
| submitButton         | A submit button                               |
| textInput            | A field to enter text                         |


Below is an example of the previous plot, but it includes a slider input that controls the min and max of the x-axis bounds. The ui includes the slider input, and additional arguments. \texttt{min} and \texttt{max} gave the minimum and the maximum of the slider, \texttt{value} and \texttt{step} give the possible options. In the server, we set the \texttt{xlim} argument in the plot function to be \texttt{input\$x\_limits}. This \texttt{x\_limits} label comes from the first argument of the slider input. This is shown in Figure 7.

```{r, eval=FALSE}
ui <- fluidPage(
  titlePanel("title panel"),
  sidebarLayout(
    sidebarPanel("sidebar panel"),
    mainPanel(
        sliderInput("x_limits", "X-Axis Limits", 
                    min = 0, max = 40, value = c(0, 40), step = 1),
      plotOutput("plot_example")
    )
  )
)
```

```{r, eval=FALSE}
# Define server logic ----
server <- function(input, output) {
  output$plot_example <- renderPlot({
    plot(mtcars$mpg, mtcars$hp,
         xlab="mpg",ylab="hp",
         xlim = input$x_limits)
  })
}
```

\begin{figure}[H]
  \includegraphics{www/slider_input.png}
  \caption{Previous plot example with a slider input to change x-axis limits}
\end{figure}

## Reactive

Reactive outputs in Shiny are elements that automatically update based on changes to reactive inputs, creating dynamic and responsive web applications. This real-time interactivity enhances the user experience by ensuring that displayed content adapts dynamically to user interactions or external data changes.


| Output Function      | Creates    |
|----------------------|------------|
| dataTableOutput      | DataTable |
| htmlOutput           | raw HTML   |
| imageOutput          | image      |
| plotOutput           | plot       |
| tableOutput          | table      |
| textOutput           | text       |
| uiOutput             | raw HTML   |
| verbatimTextOutput   | text       |


| Render Function       | Creates                                           |
|------------------------|---------------------------------------------------|
| renderDataTable        | DataTable                                         |
| renderImage            | images (saved as a link to a source file)         |
| renderPlot             | plots                                             |
| renderPrint            | any printed output                                |
| renderTable            | data frame, matrix, other table-like structures  |
| renderText             | character strings                                 |
| renderUI               | a Shiny tag object or HTML                        |



A helpful argument in creating tabs is the \texttt{id} argument. This allows the user to assign each tab its own id. You can customize the app, such as changing text or plots, based on which tab is selected. The example below shows an app with one page, created with a \texttt{fluidRow} and columns. It includes two tabs, which make use of the id argument. The text on the left changes depending which tab is chosen. We also make use of other Shiny options, such as themes, styles, and alignment. There are two key pieces in the ui: the \texttt{textOutput}, and the \texttt{id} argument. The \texttt{textOutput} contains \texttt{"selected\_text"}, which is what we call our text in the server. The \texttt{id} argument is what allows the server to differentiate between the tabs.

```{r, eval=FALSE}
ui <- navbarPage(selected = "overview",
                 theme = shinytheme("united"),
                 tags$head(),
                 fluidRow(style = "margin: 6px;",
                          h1(strong("Reactive Text"), align = "center"),
                          p("", style = "padding-top:10px;"),
                          # Column on the left for text
                          fluidRow(style = "margin: 8px;",
                                   align = "center",
                                   column(6,align="left",
                                          
                                          # Out reactive text
                                          textOutput("selected_text")
                                          ),
                                   
                                   #Column on the right with tabs
                                   column(6,tabsetPanel(
                                     
                                     # Tab ID
                                     id = "tabs_label",
                                     tabPanel("Tab 1"),
                                     tabPanel("Tab 2")
                                     )
                                     ),
                                   ), 
                          ),
                 ) 
```

In the server, there are three pieces: the tab selection, the text selection, and the text output. \texttt{selected\_tab} is a reactive expression to help Shiny display things based on which tab we select. The input is \texttt{tabs\_label}, which is what we made the tab id in the ui. We also create a reactive expression for the text. We use an \texttt{if} statement to say if tab one is selected, output "the text for tab one". Since we only have two tabs, we can use an else statement for tab two. The third and final piece in the server is a simple \texttt{textOutput} using the \texttt{selected\_text} from the reactive expression in order to send the text to the ui. Figure 8 shows the output of tab 1 while Figure 9 shows the output of tab 2.

```{r, eval=FALSE}
server <- function(input, output){
  # Make the tab selection reactive
  selected_tab <- reactive({
    input$tabs_label
  })
  
  # Write desired text
  selected_text <- reactive({
    selected <- selected_tab()
    if (selected == "Tab 1") {
      return("The text for tab one")
    } 
    else {
      return("The text for tab two")
    }
  })
  
  # Create text output
  output$selected_text <- renderText({
    selected_text()
  })

}
```

\begin{figure}[H]
  \includegraphics{www/tab1.png}
  \caption{Example of reactive text on tab 1}
\end{figure}

\begin{figure}[H]
  \includegraphics{www/tab2.png}
  \caption{Example of reactive text on tab 2}
\end{figure}

These are the basics of making Shiny UI and server. In order to call the Shiny app, the user must call \texttt{shinyApp(ui=ui, server=server)}. 

```{r, eval=FALSE}
# Run the app ----
shinyApp(ui = ui, server = server)
```


To test the app locally, the user can simply hit "Run App" in RStudio. This will build the app and bring it up as another window. If something is wrong with the app (e.g. missing a parenthesis or a comma), it will give an error in the console.
In order to deploy the app publicly, one must create an account with Shiny.io. While of course there are more advanced options, this can be done for free, with a limit of 25 active hours and 5 apps. Once an account is created, the app can be published to the web. To do this, click the blue publish button next to the “Run App” button. It will bring up a window to choose the account to deploy it from, a title, and which files to publish. For example, if parts of the app are in different folders, every folder that has a piece of the app should be published.




