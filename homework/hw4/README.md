Guide to solution files.

- You'll need to build mvnllik_sol.so to include mvnllik_sol.c
- Similarly, bootreg_sol.so needs bootreg.c and randomkit.c
- Then you should be able to render the Rmd files.
- The `*_mkl.Rmd` file is inteded to be run on a machine linked to MKL so that a comparison can be made side-by-side.
