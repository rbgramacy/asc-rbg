#include <R_ext/Utils.h>
#include <R.h>
#include <Rmath.h>
#ifdef _OPENMP
#include <omp.h>
#endif

/* to avoid copying */
#include "mvnllik.c"

/*
 * loglik_R:
 * 
 * R interface to log likelihood function above */

void logliks_R(int *n_in, int *m_in, double *Y_in, double *D_in, double *theta_in, 
	int *tlen_in, int *verb_in, double *llik_out)
{
	int i;
	double **Y, **D;

	/* change a vector representation of Y into a array one */
    Y = (double **) malloc(sizeof(double*) * (*n_in));
    Y[0] = Y_in;
    for(i=1; i<*n_in; i++) Y[i] = Y[i-1] + *m_in;

   	/* change a vector representation of D into a array one */
    D = (double **) malloc(sizeof(double*) * (*m_in));
    D[0] = D_in;
    for(i=1; i<*m_in; i++) D[i] = D[i-1] + *m_in;

    /* call the main function */
    logliks(*n_in, *m_in, Y, D, theta_in, *tlen_in, *verb_in, llik_out);

	/* clean up */
	free(Y);
	free(D);
}


#ifdef _OPENMP

/*
 * loglik_omp:
 *
 * calculates log likelihood for a multivariate normal distribution over 
 * a vector of theta values of length tlen used to define the covariance
 * structure; if D is m x m, then Y should be n x m, OpenMP implemetation
 * for threading over theta
 */

void logliks_omp(int n, int m, double **Y, double **D, double *theta, 
	int tlen, int verb, double *llik)
{
	
	#pragma omp parallel
	{
		double **K, **Ki;
		double *KiY;
		int i, j, t, me, nth;
		double ldet, qf;

		/* get thread information */
       	me = omp_get_thread_num();
        nth = omp_get_num_threads();

		/* create space */
		K = new_matrix(m, m);
		Ki = new_matrix(m, m);
		KiY = (double*) malloc(sizeof(double) *m);

		/* loop over thetas */
		for(t=me; t<tlen; t+=nth) {

			/* build covariance matrix */
			for(i=0; i<m; i++) {
				K[i][i] = 1.0 + SDEPS;
				for(j=i+1; j<m; j++)
					K[i][j] = K[j][i] = exp(0.0-D[i][j]/theta[t]);
			}

			/* calculate inverse and determinant*/
			ldet = invdet(m, K, Ki);

			/* initialize log likelihood calculation */
			llik[t] =  0.0 - n*(m*M_LN_SQRT_2PI + 0.5*ldet);

			/* calculate quadratic form */
			qf = 0.0;
			for(i=0; i<n; i++) {
				dsymv(&upper,&m,&d_one,*Ki,&m,Y[i],&i_one,&d_zero,KiY,&i_one);
				qf += ddot(&m,KiY,&i_one,Y[i],&i_one);
			}

			/* finish log likelihood calculation */
			llik[t] -= 0.5*qf;

			/* progress meter */
			#pragma omp master
			if(verb > 0 && (t+1) % verb == 0) 
				printf("t=%d, ll=%g\n", t+1, llik[t]);
		}

		/* clean up */
		delete_matrix(K);
		delete_matrix(Ki);
		free(KiY);
	}
}


/*
 * loglik_omp_R:
 * 
 * R interface to log likelihood function above, using OpenMP for threading over theta */

void logliks_omp_R(int *n_in, int *m_in, double *Y_in, double *D_in, double *theta_in, 
	int *tlen_in, int *verb_in, double *llik_out)
{
	int i;
	double **Y, **D;

	/* change a vector representation of Y into a array one */
    Y = (double **) malloc(sizeof(double*) * (*n_in));
    Y[0] = Y_in;
    for(i=1; i<*n_in; i++) Y[i] = Y[i-1] + *m_in;

   	/* change a vector representation of D into a array one */
    D = (double **) malloc(sizeof(double*) * (*m_in));
    D[0] = D_in;
    for(i=1; i<*m_in; i++) D[i] = D[i-1] + *m_in;

    /* call the main function */
    logliks_omp(*n_in, *m_in, Y, D, theta_in, *tlen_in, *verb_in, llik_out);

	/* clean up */
	free(Y);
	free(D);
}

#endif