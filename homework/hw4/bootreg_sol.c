#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <R.h>
#include "randomkit.h"
#include <omp.h>


/* include all the stuff from the non-OpenMP version 
   link from src/bootreg.c in lecture */
#include "bootreg.c"


/*
 * bootols_omp:
 *
 * Bootstrap OLS subroutine, for B bootstrap samples of beta_hat
 * OpenMP version -- uses threadsafe RNG
 */

void bootols_omp(double **X, double *Y, int n, int m, int B, int inv, double **beta_hat)
{
  /* run bootstrap in parallal */
  #pragma omp parallel
  { 
    int b, me, i, j, bindex, nth;
    double **Xb, **XtX, **XtXi;
    double *XtY, *Yb;
    unsigned long s;
    rk_state *rng_state;

    /* set up RNG state */
    rng_state = (rk_state*) malloc(sizeof(rk_state));
    #pragma omp critical
    {
      s = round(unif_rand() * 10000);
    }
    
    rk_seed(s, rng_state);

    /* temporary space for ols */
    XtX = new_matrix(m, m); 
    if(inv) {
      XtY = (double*) malloc(sizeof(double) * m);
      XtXi = new_matrix(m, m);
    } else { XtY = NULL; XtXi = NULL; }
    Xb = new_matrix(n, m);
    Yb = (double*) malloc(sizeof(double) * n);  

    /* get thread information */
    me = omp_get_thread_num();
    nth = omp_get_num_threads();

    /* in checking all (i,j) pairs, partition the work 
      according to i; this thread me will handle all 
      i that equal me mod nth */
    for(b=me; b<B; b+=nth) {
      for(i=0; i<n; i++) {
        bindex = floor(((double) n) * ((double) rk_random(rng_state))/RK_MAX);
        Yb[i] = Y[bindex];
        for(j=0; j<m; j++) Xb[i][j] = X[bindex][j];
      }

      /* call ols on Xb Yb */
      ols(Xb, Yb, n, m, XtY, XtX, XtXi, beta_hat[b]);
    }
    
    /* clean up */
    delete_matrix(Xb);
    free(Yb);
    delete_matrix(XtX);
    if(XtXi) delete_matrix(XtXi);
    if(XtY) free(XtY); 
    free(rng_state);
  }
}


/*
 * bootols_omp_R:
 *
 * bootstrap R interface to ols()
 */

void bootols_omp_R(double *X_in, double *Y_in, int *n_in, int *m_in, 
             int *B_in, int *inv_in, double *beta_hat_out)
{
  int i;
  double **X, **beta_hat;

  /* change a vector representation of X into a array one */
  X = (double **) malloc(sizeof(double*) * (*n_in));
  X[0] = X_in;
  for(i=1; i<*n_in; i++) X[i] = X[i-1] + *m_in;

  /* change a vector representation of beta_hat_out into a array one */
  beta_hat = (double **) malloc(sizeof(double*) * (*B_in));
  beta_hat[0] = beta_hat_out;
  for(i=1; i<*B_in; i++) beta_hat[i] = beta_hat[i-1] + *m_in;

  /* for R's RNG */
  GetRNGstate();

  /* call the C-side subroutine */
  bootols_omp(X, Y_in, *n_in, *m_in, *B_in, *inv_in, beta_hat);

  /* for R's RNG */
  PutRNGstate();

  /* clean up */
  free(X);
  free(beta_hat);
}
