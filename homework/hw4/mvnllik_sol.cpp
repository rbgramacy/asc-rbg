#include <Rcpp.h>
using namespace Rcpp;

extern "C" {
#include "mvnllik_sol.c"
}


/*
 * logliks_Cpp:
 *
 * this is an RCpp interface to logliks and logliks_omp, coded in C
 */
  
// [[Rcpp::depends(RcppArmadillo)]]
// [[Rcpp::plugins(openmp)]]
// [[Rcpp::export]]
NumericVector logliksCpp(NumericMatrix Yt, NumericMatrix Din, NumericVector theta, bool omp = false, int verb = 0) 
{
  
  double ** Y;
  double ** D;
  NumericVector ll(theta.length());
  
  /* change a vector representation of Y into a array one */
  Y = (double **) malloc(sizeof(double*) * Yt.ncol());
  Y[0] = &(Yt(0,0));
  for(int i=1; i<Yt.ncol(); i++) Y[i] = Y[i-1] + Yt.nrow();
  
  /* change a vector representation of D into a array one */
  D = (double **) malloc(sizeof(double*) * Din.nrow());
  D[0] = &(Din(0,0));
  for(int i=1; i<Din.nrow(); i++) D[i] = D[i-1] + Din.nrow();
  
  /* call the main function */
  if(omp) 
    logliks_omp(Yt.ncol(), Yt.nrow(), Y, D, &(theta(0)), theta.length(), verb, (double*) &(ll(0)));
  else
    logliks(Yt.ncol(), Yt.nrow(), Y, D, &(theta(0)), theta.length(), verb, (double*) &(ll(0)));

  /* clean up */
  free(Y);
  free(D);

  /* send the log likelihoods back */
  return(ll);
}
