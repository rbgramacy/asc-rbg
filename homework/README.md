# Homeworks

Subdirectories herin contain the source files for homework problems, data and solutions for the **Advanced Statistical Computing** class.

## Breakdown

- **hw0** due 28 Aug 2023: recreating a latex solution document (extra credit)
- **hw1** due 08 Sep 2023: covering local and remote Linux, `git` for version control, and solving a simple optimization problem.
- **hw2** due 22 Sep 2023: covering more unix stuff, R topics like scoping and S3 object oriented capabilities.
- **hw3** due 13 Oct 2023: covering debugging/profiling and parallelization
- **hw4** due 27 Oct 2023: covering C subroutines, OpenMP parallelization and Rcpp
- **hw5** due 17 Nov 2023: covering cluster/ARC parallelization
