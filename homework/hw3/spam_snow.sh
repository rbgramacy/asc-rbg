#!/bin/bash

# get number of cores
nprc=$(nproc)

# set defaults
nth=$nprc
reps=5
folds=5

# check if there is more than three
if [[ $# -gt 3 ]];
then
    echo "Error: too many arguments"
    exit
fi

# if single (instances) argument, validate and update
if [[ $# -ge 1 ]];
then
    nth=$1
    # check if an integer
    if [[ -n ${nth//[0-9]/} ]];
    then
        echo "Error: instances ($nth) argument must be a positive integer"
        exit
    fi
fi

# if second (folds) argument, validate
if [[ $# -ge 2 ]];
then
    folds=$2
    # check if an integer
    if [[ -n ${folds//[0-9]/} ]];
    then
        echo "Error: reps ($folds) argument must be a positive integer"
        exit
    fi
fi

# if second (reps) argument, validate
if [[ $# -ge 3 ]];
then
    reps=$3
    # check if an integer
    if [[ -n ${reps//[0-9]/} ]];
    then
        echo "Error: reps ($reps) argument must be a positive integer"
        exit
    fi
fi

# check if greater than number of cores in machine
if [[ $reps -gt $nprc ]];
then
    echo "Warning: requested more parallel instances ($reps) than cores ($nprc)"
fi

# now call R
nohup R CMD BATCH "--args nth=$nth folds=$folds reps=$reps" spam_snow.R  &

