#!/bin/bash

# get number of cores
nprc=$(nproc)

# set defaults
nth=$nprc
reps=5

# check if there is more than two arguments
if [[ $# -gt 2 ]];
then
    echo "Error: too many arguments"
    exit
fi

# if single (instances) argument, validate and update
if [[ $# -ge 1 ]];
then
    nth=$1
    # check if an integer
    if [[ -n ${nth//[0-9]/} ]];
    then
        echo "Error: instances ($nth) argument must be a positive integer"
        exit
    fi
fi

# if second (reps) argument, validate
if [[ $# -ge 2 ]];
then
    reps=$2
    # check if an integer
    if [[ -n ${reps//[0-9]/} ]];
    then
        echo "Error: reps ($reps) argument must be a positive integer"
        exit
    fi
fi

# check if greater than number of cores in machine
if [[ $reps -gt $nprc ]];
then
    echo "Warning: requested more parallel instances ($reps) than cores ($nprc)"
fi

# now call R scripts
for i in `seq 1 $nth`;
do
    echo "running nohup R CMD BATCH '--args seed=$i reps=$reps' spam_mc.R spam_mc_$i.Rout"
    nohup R CMD BATCH "--args seed=$i reps=$reps" spam_mc.R spam_mc_$i.Rout &
done

## Thanks to Alex Durbin for the initial version of this file
