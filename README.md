# Advanced Statistical Computing

## Details
- **Course Number**: STAT 6554
- **Institution**: Virginia Tech
- **Term, Dates, Location**: Fall Semester 2023, M/W/F 10:10-11:00am Hutcheson Hall 209
- **Instructor**: Robert B. Gramacy (rbg@vt.edu; [bobby.gramacy.com](http://bobby.gramacy.com))
    + **Office Hours & Location**: by appointment in HUTCH 403G 
- **Prerequisites**: A first-year graduate sequence in statistics, an introductory statistical computing course (ideally with `R`), and a willingness and capability to work in a Unix environment (more details under Personal Computing below).
- **Required text**: None
- **Web**: Course materials will be provided on the class [Bitbucket `git` repository](https://bitbucket.org/rbgramacy/asc-rbg/) in raw form (students must compile to ease visuals); [Canvas](http://canvas.vt.edu) will only be used for grades, calendar and delivery of proprietary material
- **Communication and collaboration**: all course communication, i.e., with the professor but also with other students, must occur on the class [Discord](https://discord.com) channels; use the link on Canvas to sign up.  Email and other (non-discord/class channel) communication with classmates will be considered a violation of the honor code (more below)

## About the course

### Content and goals

This is a second course on statistical computing.  Although basics will be revisited, the pace will be swift so we can get to advanced computing and data management topics as quickly as possible.  The main programming language will be `R`, but by the end it will primarily act as the "glue" binding together other languages, databases, computing architectures and interfaces, as appropriate for the task(s) at hand.  We will learn how statisticians can best leverage modern desktop computing (multiple cores), cluster computing (multiple nodes) and distributed computing (hadoop/Amazon EC2) and the coming wave of exascale computing (GPU/TPU/Xeon Phi).  The goal is to make students marketable as postdocs at National Lab and similar research facilities where statisticians are expected to have the same computing skills as other applied scientists.  A high bar of computing experience is required for graduating Ph.D.s to be competitive applicants for those positions, and likewise at investment banks/hedge funds, semiconductor companies, industrial engineering giants (Boeing, GE), etc.  An aspect of that preparation will be "back to basics" with navigating the Unix shell, manipulating data therein, compiling libraries with `make`, version control (e.g., Git), and good habits/best practice with code development and data management.   

### Software/List of topics

`R` will be reviewed and will serve as the main language for the class.  Along the way we will be introduced to the following languages/libraries/platforms.  No previous experience with these will be required.  In most cases the material will be structured so that students will ultimately be versed in *using* these tools appropriately, rather than being required to build bespoke ones from scratch.  (We will not attempt to teach `C` programming for example, but students will learn how to compile a `C` library and access it from within `R`.)  

The expectation is that there would be about a week for each of these, plus one week reviewing `R` basics.

1. Unix: `bash` and common build-ins: `find`, `head`, `grep`, `make`, `nohup`, `screen` 
2. Version control: `git` with [Bitbucket](www.bitbucket.com) and/or [GitHub](www.github.com) and collaboration with [Discord](discord.com)
3. Advanced `R` topics: object oriented programming (S3/S4), pre-allocation and vectorization, modularization, environments, data structures, objects, functional programming
4. `R` code correctness and efficiency: debugging (`debug`, `error=recover`) and profiling (`Rprof`)
5. Data manipulation and data cleaning in Unix and `R`: regular expressions, `sed`, `awk`, merging, pivoting, subsetting, summarizing
6. Data input: scraping (from web), formatted data (JSON/XML), databases (SQL), from other computing environments (`Matlab`, `Excel`, `Stata`, etc.)
7. Visualization: advanced plotting and `R` graphics manipulation, `ggplot`
8. Sharing: `R` packages, `Rmarkdown`, `Shiny` apps
9. Compiled code: `C`/`C++` (and `Rcpp`), `Fortan` for fast `for` loops and linking external libraries, correctness (`gdb`/`valgrind`) and efficiency (`gprof`/OSX Instruments)
10. Custom linear algebra libraries: MKL, Accellerate framework, ATLAS for faster `R` execution and customizing matrix-vector routines in `C`
11. Symmetric multiprocessor computation: `OpenMP`
12. Cluster computation: `parallel` in `R`, MPI (`Rmpi`), scheduler scripts/job queueing (SLURM)
13. Distributed computation and storage: Amazon EC2/S3, map-reduce (`hadoop`)
14. Exascale computing: data-parallel computing on graphics cards (NVidia/`CUDA`) and their competitors (Intel Xeon Phi)


### Personal computing

Students will need a Unix environment for this class.  Examples include OSX, any Linux, BSD variant, etc.  Students with PCs running Windows are encouraged to create a partition and install [Ubuntu Linux](https://www.ubuntu.com/).  The instructor will provide support for this on a one-on-one basis.  Alternatives to installing Ubuntu Linux on a partition included a (persistent) installation on a thumb drive,  on a (free) virtual machine, e.g., through [Virtual Box](https://www.virtualbox.org/), or through a container such as the [Windows Subsystem for Linux](https://docs.microsoft.com/en-us/windows/wsl/install-win10) which the instructor will also support. Students must also engage Linux servers remotely and off-line for full credit on homeworks.  

### Texts

Although there is no required text (because you can generally Google and find quality help more quickly than with a book), the lecture notes are derived from the following texts, which each make nice desk references and are therefore highly recommended.

- [The Art of `R` Programming](https://www.amazon.com/Art-Programming-Statistical-Software-Design/dp/1593273843) by Norman Matloff; [(do not share)](http://rbg.stat.vt.edu/books/ArtofRProgramming.pdf)
- [Advanced `R`](https://www.amazon.com/Advanced-Chapman-Hall-Hadley-Wickham/dp/1466586966) by Hadley Wickham; [(free version)](https://adv-r.hadley.nz/)
- [Parallel Computing for Data Science](https://www.amazon.com/Parallel-Computing-Data-Science-Examples/dp/1466587016) by Norman Matloff; [(free version)](https://citeseerx.ist.psu.edu/document?repid=rep1&type=pdf&doi=6adae247c2c728cc17e1e9d34336bad7b0001847)
- [`R` in a Nutshell](https://www.amazon.com/Nutshell-Desktop-Quick-Reference-OReilly/dp/144931208X) by Joseph Adler; [(do not share)](http://rbg.stat.vt.edu/books/R_in_a_Nutshell.pdf)
- [Scientific Programming and Simulation using `R`](https://www.amazon.com/Introduction-Scientific-Programming-Simulation-Chapman/dp/1466569999/ref=sr_1_1?s=books&ie=UTF8&qid=1478545225&sr=1-1&keywords=scientific+programming+and+simulation+with+r) by Jones, et al.; [(do not share)](http://rbg.stat.vt.edu/books/SPSUR.pdf)

### Grading details

#### Rubric
- 75% Homework
- 25% Final project

The graded work will be code-based and will be marked separately for correctness, efficiency, documentation and style.  Students will be required to set up a private Git repository on [Bitbucket](www.bitbucket.com) in which all class work will be stored, and where homeworks will be submitted for grading.  The maintenance of this repository will also be fair game for evaluation.  All class material will be provided by a separate Git repo on Bitbucket, including material contributed by students for the final project(s).  

#### Exams

There will be no in-class exams.  A final project will be assigned part-way through the semester, which the students must complete on their own or in groups (but without the help of other classmates).  It will be due during finals week along with in-class presentation and material pulled to the class Git repo via pull request.  

#### Homework

- Homework will be assigned and due on a regular basis. Students are welcome to collaborate with one another, but are required to submit their own work as well as be able to reproduce it.  Nhat non-discord (on the class server) electronic communication between students, or anyone else, about class content is a violation of the Honor Code.
- All work must be shown and software must be used when appropriate with attached software output.
- Late homework will be penalized.  Homeworks will be due at 5pm.  No help can be given, by classmates or the professor, after the due date.  You will be given a grace period up until midnight (same night) where you can turn in your work without penalty.  Every day (24 hours) thereafter that you are late will result in a 5 point deduction.  There will be no exceptions to this setup without doctor's notes.  Get started earlly and manage your time.  Some of the homeworks will require use of shared resources (like department servers and ARC).  Oversuscription of those resources, requiring you to wait in queues, etc., are not an excuse for penalty-free extension.
- Homeworks will be submitted via the student's private `git` repository.  Evaluation of the contents and organization of this repo is also fair game.  
- All the homework grades will be kept which means NO homework grade will be dropped.
- You will also be marked on the quality of your presentation.  Solutions without verbal description that shows insight and attention to detail will not recieve full credit.  You must learn to write about code.  Pay attention to formatting too.  Solution files that do not manage the size of figures, or the amount of output printed to the screen, etc., will also not get full credit.  

## Logistics

### Honor code

The [Virginia Tech Honor Code](http://www.honorsystem.vt.edu) will be strictly enforced in this course. All graded assignments must be composed of your own work.  If you are unsure about something, ask.  You can discuss with other students, but you cannot put their work on your homework solutions.  Do not discuss with anyone outside of the class.  Anything that you have not explained will not recieve full credit (and may recieve no credit), and any plajarism (any appropriating of someone else's work as your own) will be referred to the Graduate School, and the recommendation will be $F^{\star}$.  There will be no further warnings or leniency.

### Services for students with disabilities

Any student who feels that he or she may need an accommodation because of a disability (learning disability, attention deficit disorder, psychological, physical, etc.), please make an appointment to see me during office hours.

### Important dates

- Please take note of the important dates and deadlines noted on the [Registrar's web page](http://registrar.vt.edu/dates-deadlines-accordion.html).
