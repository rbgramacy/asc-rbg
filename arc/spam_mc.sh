#!/bin/bash

#SBATCH -N 1
#SBATCH --ntasks-per-node=4
#SBATCH -t 1:00:00       
#SBATCH -p normal_q              
#SBATCH -A ascclass

module reset
module load R/4.1.0-foss-2021a

## export MKL_NUM_THREADS=$SLURM_NTASKS
export OMP_NUM_NUM_THREADS=$SLURM_NTASKS

echo "$( date ): Starting spam_mc"
R CMD BATCH spam_mc.R
echo "$( date ): Finished spam_mc"

