#!/bin/bash

#SBATCH -N 2
#SBATCH --ntasks-per-node=16
#SBATCH -t 1:00:00       
#SBATCH -p normal_q              
#SBATCH -A ascclass

module reset
module load R/4.1.0-foss-2021a

#set number of cores used by each r process
export OMP_NUM_THREADS=2

#number of r processes to run
ncopies=16

echo "$( date ): Starting spam_mc"

for i in $( seq 1 $ncopies ); do 
  R CMD BATCH "--args seed=$i reps=5" spam_mc.R spam_mc_${i}.Rout &
done
wait

echo "$( date ): Finished spam_mc"

