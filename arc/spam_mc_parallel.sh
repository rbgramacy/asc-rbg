#!/bin/bash

#SBATCH -N 2
#SBATCH --ntasks-per-node=16
#SBATCH -t 1:00:00       
#SBATCH -p normal_q              
#SBATCH -A ascclass

module reset
module load parallel
module load R/4.1.0-foss-2021a

#set number of cores used by each r process
export OMP_NUM_THREADS=2

#number of r processes to run
ncopies=32

#processes to run at a time
nparallel=16

echo "$( date ): Starting spam_mc"

scontrol show hostname $SLURM_NODELIST > node.list

# for i in $( seq 1 $ncopies ); do 
#   R CMD BATCH "--args seed=$i reps=5" spam_mc.R spam_mc_${i}.Rout &
# done
# wait
seq 1 $ncopies | parallel -j$nparallel --workdir $PWD --sshloginfile node.list --env OMP_NUM_THREADS "module reset; module load R/4.1.0-foss-2021a; R CMD BATCH \"--args seed={} reps=5\" spam_mc.R spam_mc_{}.Rout"

echo "$( date ): Finished spam_mc"

## collect the results
echo "$( date ): Starting spam_mc_collect"
R CMD BATCH spam_mc_collect.R spam_mc_collect.Rout
echo "$( date ): Finished spam_mc_collect"

