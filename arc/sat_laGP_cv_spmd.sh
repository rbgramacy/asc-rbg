#!/bin/bash
#SBATCH -t 15:00:00
#SBATCH -p normal_q
#SBATCH -A ascclass
#SBATCH -N 1 --ntasks-per-node=5 --cpus-per-task=16

# Add modules
module reset
module load R/4.1.0-foss-2021a

#set num threads
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
export OPENBLAS_NUM_THEADS=1

#timestamp
echo "$( date ): Starting laGP (spmd version)"

## Run R (spmd mode)
SCRIPT=sat_laGP_cv_spmd.R  
mpirun -np $SLURM_NTASKS_PER_NODE Rscript sat_laGP_cv_spmd.R 

#timestamp
echo "$( date ): Finished laGP (spmd version)"
