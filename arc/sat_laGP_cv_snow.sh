#!/bin/bash
#SBATCH -t 1:00:00
#SBATCH -p normal_q 
#SBATCH -A ascclass
#SBATCH -N1 --ntasks-per-node=5 --cpus-per-task=8
## ask for one more task than you need for the master 

### Add modules
module reset
module load R/4.1.0-foss-2021a

# Set num threads
export OMP_NUM_THREADS=8 ##$SLURM_CPUS_PER_TASK
# but not for OpenBLAS
export OPENBLAS_NUM_THREADS=1

# Run R
# --map-by creates a process on each node (not needed if one node)
# --bind-to lets OpenMP run across the allocated cores on the node
## mpirun -np 1 --map-by ppr:1:node --bind-to none Rscript sat_laGP_cv_snow.R
mpirun -np 1 --bind-to none Rscript sat_laGP_cv_snow.R

#wait (not sure if I need this)
#exit;
